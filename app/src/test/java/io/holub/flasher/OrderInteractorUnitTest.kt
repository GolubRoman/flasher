package io.holub.flasher

import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppModule
import io.holub.flasher.common.di.DaggerAppComponent
import io.holub.flasher.store.cart.CartStuff
import io.holub.flasher.store.cart.DisplayedCartStuff
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.lang.Thread.sleep

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class OrderInteractorUnitTest {

    var appComponent: AppComponent? = null

    @Before
    fun init() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(RuntimeEnvironment.application))
            .build()
    }

    @Test
    fun addStuffToCartTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val stuff = createRandomStuff()

        orderinteractor.addStuffToCart(stuff)
            .subscribeOn(Schedulers.io())
            .blockingAwait()

        val cartStuff = orderDao.observeCartStuffForStuffId(stuff.stuffId)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(cartStuff != null)
    }

    @Test
    fun incrementCartStuffCountTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val store = createRandomStore()
        val stuff = createRandomStuffWithStoreId(store.id)
        val cartStuff = createRandomCartStuff(stuff)

        Completable.fromRunnable { orderDao.upsertCartStuff(cartStuff) }
            .subscribeOn(Schedulers.io())
            .blockingAwait()
        orderinteractor.incrementCartStuffCount(DisplayedCartStuff(cartStuff, stuff, store))
        sleep(2000)

        val updatedCartStuff = orderDao.observeCartStuffForId(cartStuff.cartStuffId)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updatedCartStuff.count == ++cartStuff.count)
    }

    @Test
    fun decrementCartStuffCountTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val store = createRandomStore()
        val stuff = createRandomStuffWithStoreId(store.id)
        val cartStuff = createRandomCartStuff(stuff)
        cartStuff.count++

        Completable.fromRunnable { orderDao.upsertCartStuff(cartStuff) }
            .subscribeOn(Schedulers.io())
            .blockingAwait()
        orderinteractor.decrementCartStuffCount(DisplayedCartStuff(cartStuff, stuff, store))
        sleep(2000)

        val updatedCartStuff = orderDao.observeCartStuffForId(cartStuff.cartStuffId)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updatedCartStuff.count == --cartStuff.count)
    }

    @Test
    fun removeCartStuffTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val store = createRandomStore()
        val stuff = createRandomStuffWithStoreId(store.id)
        val cartStuff = createRandomCartStuff(stuff)
        val emptyCartStuff = CartStuff("", "", 0)

        Completable.fromRunnable { orderDao.upsertCartStuff(cartStuff) }
            .subscribeOn(Schedulers.io())
            .blockingAwait()
        orderinteractor.removeCartStuff(DisplayedCartStuff(cartStuff, stuff, store))
        sleep(2000)

        val updatedCartStuff = orderDao.observeCartStuffForId(cartStuff.cartStuffId)
            .defaultIfEmpty(emptyCartStuff)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updatedCartStuff == emptyCartStuff)
    }

    @Test
    fun observeDisplayedCartStuffsForStoreIdTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val storeDao = appComponent!!.storeDao()
        val store = createRandomStore()
        val stuffs = createRandomStuffs(3, store.id)
        val cartStuffs = createRandomCartStuffs(stuffs)

        Completable.fromRunnable {
            storeDao.upsertStore(store)
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        Observable.range(0, stuffs.size)
            .flatMap {
                Observable.fromCallable {
                    storeDao.upsertStuff(stuffs[it])
                    orderDao.upsertCartStuff(cartStuffs[it])
                }
            }.subscribeOn(Schedulers.io())
            .blockingLast()

        val displayedCartStuffs = orderinteractor.observeDisplayedCartStuffsForStoreId(store.id)
            .subscribeOn(Schedulers.io())
            .blockingGet()
        sleep(3000)

        System.out.println(displayedCartStuffs.toString())
        assertTrue(displayedCartStuffs.map { it.cartStuff }.containsAll(cartStuffs))
        assertTrue(displayedCartStuffs.map { it.stuff }.containsAll(stuffs))
    }

    @Test
    fun setOrderTest() {
        val orderinteractor = appComponent!!.orderInteractor()
        val orderDao = appComponent!!.orderDao()
        val storeDao = appComponent!!.storeDao()
        val store = createRandomStore()
        val stuffs = createRandomStuffs(3, store.id)
        val cartStuffs = createRandomCartStuffs(stuffs)

        Completable.fromRunnable {
            storeDao.upsertStore(store)
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        Observable.range(0, stuffs.size)
            .flatMap {
                Observable.fromCallable {
                    storeDao.upsertStuff(stuffs[it])
                    orderDao.upsertCartStuff(cartStuffs[it])
                }
            }.subscribeOn(Schedulers.io())
            .blockingLast()

        orderinteractor.setOrder(
            store.id,
            store.name,
            "+380000000000",
            -1.0,
            -1.0,
            "Address"
        ).subscribeOn(Schedulers.io())
            .blockingAwait()

        val orders = orderDao.getAllOrders()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        val orderStuffIds = orderDao.observeOrderStuffsForOrderId(orders[0].orderId)
            .subscribeOn(Schedulers.io())
            .blockingGet()
            .map { it.stuffId }

        assertTrue(orders.isNotEmpty() && orders[0].storeId == store.id && orders[0].storeName == store.name
            && orderStuffIds.containsAll(stuffs.map { it.stuffId }))
    }

}