package io.holub.flasher

import com.google.firebase.database.FirebaseDatabase
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppModule
import io.holub.flasher.common.di.DaggerAppComponent
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.stuff.db.StuffSubCharacteristic
import io.holub.flasher.store.stuff.db.SubCharacteristicType
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.lang.Thread.sleep

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class StoreInteractorUnitTest {

    var appComponent: AppComponent? = null

    var storeInteractor: StoreInteractor? = null

    var firebaseDatabase: FirebaseDatabase? = null

    @Before
    fun init() {
        firebaseDatabase = mock(FirebaseDatabase::class.java)
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(RuntimeEnvironment.application))
            .build()
        storeInteractor = StoreInteractor(appComponent!!.prefs(), appComponent!!.storeDao(), firebaseDatabase!!, appComponent!!.notificationManager())
    }

    @Test
    fun dropAllFiltersForCategoryIdTest() {
        val storeDao = appComponent!!.storeDao()
        val category = createRandomCategory()
        val characteristic = createRandomCharacteristic(category)
        val subCharacteristics = createRandomSubCharacteristics(3, characteristic)

        Completable.fromRunnable {
            storeDao.upsertCategory(category)
            storeDao.upsertCharacteristic(characteristic)
            subCharacteristics.forEach {
                it.intStart = 5
                it.intEnd = 6
                it.intRangeStart = 0
                it.intRangeEnd = 10
                it.type = SubCharacteristicType.INT
                storeDao.upsertSubCharacteristic(it)
            }
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        storeInteractor!!.dropAllFiltersForCategoryId(category.id)
            .subscribeOn(Schedulers.io())
            .blockingAwait()

        val updatedSubCharacteristics = storeDao.observeSubCharacteristics(characteristic.characteristicId)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updatedSubCharacteristics.none { it.intStart != it.intRangeStart || it.intEnd != it.intRangeEnd })
    }

    @Test
    fun observeCharacteristicsForCategoryIdTest() {
        val storeDao = appComponent!!.storeDao()
        val category = createRandomCategory()
        val characteristic = createRandomCharacteristic(category)

        Completable.fromRunnable {
            storeDao.upsertCategory(category)
            storeDao.upsertCharacteristic(characteristic)
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        val updatedCharacteristic = storeInteractor!!.observeCharacteristicsForCategoryId(category.id)
            .subscribeOn(Schedulers.io())
            .blockingGet()[0]

        assertTrue(updatedCharacteristic == characteristic)
    }

    @Test
    fun upsertSubCharacteristicTest() {
        val storeDao = appComponent!!.storeDao()
        val category = createRandomCategory()
        val characteristic = createRandomCharacteristic(category)
        val subCharacteristic = createRandomSubCharacteristic(characteristic)

        storeInteractor!!.upsertSubCharacteristic(subCharacteristic)
        sleep(3000)

        val updateSubCharacteristic = Maybe.fromCallable {
            storeDao.getSubCharacteristicsForCharacteristicId(characteristic.characteristicId).elementAt(0)
        }.subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updateSubCharacteristic.subCharacteristicId == subCharacteristic.subCharacteristicId)
    }

    @Test
    fun observeCategoryForCategoryIdTest() {
        val storeDao = appComponent!!.storeDao()
        val category = createRandomCategory()

        Completable.fromRunnable {
            storeDao.upsertCategory(category)
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        val updatedCategory = storeInteractor!!.observeCategoryForCategoryId(category.id)
            .subscribeOn(Schedulers.io())
            .blockingGet()


        assertTrue(updatedCategory.id == category.id)
    }

    @Test
    fun observeStoreForStoreIdTest() {
        val storeDao = appComponent!!.storeDao()
        val store = createRandomStore()

        Completable.fromRunnable {
            storeDao.upsertStore(store)
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        val updatedStore = storeInteractor!!.observeStoreForStoreId(store.id)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(updatedStore.id == store.id)
    }

    @Test
    fun filteredStuffsForCategoryTest() {
        val storeDao = appComponent!!.storeDao()
        val store = createRandomStore()
        val category = createRandomCategory()
        val characteristic = createRandomCharacteristic(category)
        val subCharacteristic = createRandomSubCharacteristic(characteristic)
        subCharacteristic.intRangeStart = 0
        subCharacteristic.intRangeEnd = 3
        subCharacteristic.intStart = 2
        subCharacteristic.intEnd = 2
        val stuffs = createRandomStuffs(3, store.id)
        val stuffCategories = stuffs.map { createRandomStuffCategory(it, category) }
        val stuffCharacteristics = stuffs.map { createRandomStuffCharacteristic(it, characteristic) }
        val stuffSubCharacteristics = mutableMapOf<String, StuffSubCharacteristic>()
        stuffs.forEachIndexed { index, stuff ->
            stuffSubCharacteristics.put(stuff.stuffId,
                createRandomStuffSubCharacteristic(stuff, subCharacteristic,
                    stuffCharacteristics.filter { it.stuffId == stuff.stuffId}.first(),
                    index))
        }

        Completable.fromRunnable {
            storeDao.upsertStore(store)
            storeDao.upsertCategory(category)
            storeDao.upsertCharacteristic(characteristic)
            storeDao.upsertSubCharacteristic(subCharacteristic)
            storeDao.upsertStuffs(stuffs)
            stuffCategories.forEach { storeDao.upsertStuffCategory(it) }
            stuffCharacteristics.forEach { storeDao.upsertStuffCharacteristic(it) }
            stuffSubCharacteristics.forEach { storeDao.upsertStuffSubCharacteristic(it.value) }
        }.subscribeOn(Schedulers.io())
            .blockingAwait()

        val filteredStuffs = storeInteractor!!.filteredStuffsForCategory(category.id)
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(filteredStuffs.size == 1)
    }

}