package io.holub.flasher

import io.holub.flasher.store.cart.CartStuff
import io.holub.flasher.store.db.Category
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.db.Stuff
import io.holub.flasher.store.stuff.db.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.*

fun createRandomStuff(): Stuff {
    return Stuff(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        -1,
        -1,
        false,
        10,
        false
    )
}

fun createRandomCartStuff(stuff: Stuff): CartStuff {
    return CartStuff(
        UUID.randomUUID().toString(),
        stuff.stuffId,
        0
    )
}

fun createRandomStuffs(count: Int, storeId: String): List<Stuff> {
    val list = mutableListOf<Stuff>()
    Observable.range(0, count)
        .map { list.add(createRandomStuffWithStoreId(storeId)) }
        .subscribe()
    return list
}

fun createRandomCartStuffs(list: List<Stuff>): List<CartStuff> {
    return list.toMutableList().map { createRandomCartStuff(it) }
}

fun createRandomStore(): Store {
    return Store(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        -1L,
        -1L,
        -1.0,
        -1.0,
        false,
        false
    )
}

fun createRandomStuffWithStoreId(storeId: String): Stuff {
    return Stuff(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        storeId,
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        -1,
        -1,
        false,
        10,
        false
    )
}

fun createRandomCategory(): Category {
    return Category(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        false
    )
}

fun createRandomStuffCategory(stuff: Stuff, category: Category): StuffCategory {
    return StuffCategory(
        UUID.randomUUID().toString(),
        stuff.stuffId,
        category.id
    )
}

fun createRandomCharacteristic(category: Category): Characteristic {
    return Characteristic(
        UUID.randomUUID().toString(),
        category.id,
        UUID.randomUUID().toString()
    )
}

fun createRandomStuffSubCharacteristics(count: Int, stuff: Stuff, subCharacteristic: SubCharacteristic, stuffCharacteristic: StuffCharacteristic, intValue: Int): List<StuffSubCharacteristic> {
    val list = mutableListOf<StuffSubCharacteristic>()
    Observable.range(0, count)
        .flatMap {
            Observable.fromCallable {
                list.add(createRandomStuffSubCharacteristic(stuff, subCharacteristic, stuffCharacteristic, intValue))
            }
        }.subscribeOn(Schedulers.io())
        .blockingLast()
    return list
}

fun createRandomStuffSubCharacteristic(stuff: Stuff, subCharacteristic: SubCharacteristic, stuffCharacteristic: StuffCharacteristic, intValue: Int): StuffSubCharacteristic {
    val stuffSubCharacteristic = StuffSubCharacteristic(
        UUID.randomUUID().toString(),
        stuff.stuffId,
        stuffCharacteristic.id,
        subCharacteristic.subCharacteristicId,
        UUID.randomUUID().toString(),
        SubCharacteristicType.INT
    )
    stuffSubCharacteristic.intValue = intValue
    return stuffSubCharacteristic
}

fun createRandomStuffCharacteristic(stuff: Stuff, characteristic: Characteristic): StuffCharacteristic {
    return StuffCharacteristic(
        UUID.randomUUID().toString(),
        stuff.stuffId,
        stuff.stuffCategoryId,
        characteristic.characteristicId
    )
}

fun createRandomSubCharacteristic(characteristic: Characteristic): SubCharacteristic {
    return SubCharacteristic(
        UUID.randomUUID().toString(),
        SubCharacteristicType.INT,
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        characteristic.characteristicId
    )
}

fun createRandomSubCharacteristics(count: Int, characteristic: Characteristic): List<SubCharacteristic> {
    var list = mutableListOf<SubCharacteristic>()
    Observable.range(0, count)
        .flatMap {
            Observable.fromCallable {
                list.add(createRandomSubCharacteristic(characteristic))
            }
        }.subscribeOn(Schedulers.io())
        .blockingLast()
    return list
}
