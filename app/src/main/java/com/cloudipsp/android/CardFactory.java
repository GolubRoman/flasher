package com.cloudipsp.android;

public class CardFactory {

    public static Card makeCard(String cardNumber,
                                String expireMonth,
                                String expireYear,
                                String cvv) {
        return new Card(
                cardNumber.replaceAll(" ", ""),
                expireMonth,
                expireYear,
                cvv);
    }
}
