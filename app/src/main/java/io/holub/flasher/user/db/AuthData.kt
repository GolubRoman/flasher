package io.holub.flasher.user.db

data class AuthData(
    var userId: String,
    var accessToken: String,
    var refreshToken: String
)