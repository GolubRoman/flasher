package io.holub.flasher.user

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.util.buildDbConfig
import io.holub.flasher.profile.Card
import io.holub.flasher.profile.PaymentDao
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers

class UserInteractor(
    private val prefs: Prefs,
    private val paymentDao: PaymentDao,
    private val auth: FirebaseAuth?
) {

    @SuppressLint("CheckResult")
    fun removeCard(card: Card) {
        Completable.fromCallable {
            paymentDao.removeCard(card)
        }
            .subscribeOn(Schedulers.io())
            .subscribe({}, { ErrorHandler.logError(it) })
    }

    fun addCard(card: Card) =
        Completable.fromRunnable { paymentDao.upsertCard(card) }

    fun observeCards(pageSize: Int): LiveData<PagedList<Card>> {
        return LivePagedListBuilder(paymentDao.observeCardsLiveData(), buildDbConfig(pageSize)).build()
    }

    fun observeCurrentUser(): Maybe<FirebaseUser> {
        return Maybe.create {
            val user = auth?.currentUser
            if (user == null) {
                it.onError(NullPointerException())
            } else {
                it.onSuccess(user)
            }
        }
    }

    fun signInWithCredential(credential: AuthCredential): Maybe<Task<AuthResult>> {
        return Maybe.fromCallable { auth?.signInWithCredential(credential) }
    }

    fun signOut(): Completable {
        return Completable.fromRunnable {
            auth?.signOut()
        }
    }

    fun isUserAlreadyAuthorized(): Maybe<FirebaseUser> {
        return Maybe.create {
            val user = auth?.currentUser
            if (user == null) {
                it.onComplete()
            } else {
                it.onSuccess(user)
            }
        }
    }
}

