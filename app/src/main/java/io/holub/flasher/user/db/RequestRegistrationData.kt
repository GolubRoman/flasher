package io.holub.flasher.user.db

data class RequestRegistrationData(
    var code: String,
    var phone: String,
    var name: String
)