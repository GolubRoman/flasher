package io.holub.flasher.user.db

data class UserData(
    var name: String,
    var phone: String?
)