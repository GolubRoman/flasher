package io.holub.flasher.user.db

data class RequestAuthData(
    var code: String,
    var phone: String
)