package io.holub.flasher.user.db

data class RegistrationState(
    var registered: Boolean
)