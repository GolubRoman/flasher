package io.holub.flasher.map

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.google.android.gms.maps.model.LatLng
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.StoreInteractor
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MapViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    init {
        DaggerMapViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeStores(latLng: LatLng): LiveData<PagedList<Store>> {
        return storeInteractor.observeAllStoresList(latLng, 0)
    }

    fun observeSpecificStore(storeId: String): Maybe<Store> {
        return storeInteractor.observeSpecificStoreForIdMaybe(storeId)
            .observeOn(AndroidSchedulers.mainThread())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(mapViewModel: MapViewModel)
    }
}


