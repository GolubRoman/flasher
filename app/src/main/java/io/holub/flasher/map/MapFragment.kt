package io.holub.flasher.map

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.common.view.TabFragment
import io.holub.flasher.databinding.FragmentMapBinding
import io.holub.flasher.store.StoreActivity
import io.holub.flasher.store.db.Store
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class MapFragment : TabFragment() {

    private var binding: FragmentMapBinding? = null
    private var viewModel: MapViewModel? = null
    private var googleMap: GoogleMap? = null
    private var isInitted = false
    private var isMapInitted = false
    private var lastClickedMarker: Marker? = null

    private val kyivsLatLng = LatLng(50.4350207, 30.5281623)
    private var currentLatLng = kyivsLatLng
    private var fusedLocationClient: FusedLocationProviderClient? = null

    private val TRANSLATE_Y = 4000f
    private val TRANSLATE_ANIMATION_DURATION = 300L
    private var defaultStoreCardPosition = 0f
    private var deltaY = 0f

    private val PERMISSIONS =
        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false)
        viewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)
        return binding?.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun init() {
        if (!isMapInitted) {
            checkLocationPermission()
        }
        if (!isInitted) {
            showProgressBarFrame(binding?.flProgressBar)
            binding?.let {
                defaultStoreCardPosition = it.cvStoreCard.y
                it.cvStoreCard.y = TRANSLATE_Y
                it.storeCard.let { it.root.setOnTouchListener { view, motionEvent -> onTouch(view, motionEvent) } }
            }
            isInitted = true
        }
    }

    @SuppressLint("MissingPermission")
    private fun initLocationProcessing() {
        activity?.let {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as AppCompatActivity)
            fusedLocationClient?.lastLocation?.addOnSuccessListener {
                it?.let {
                    currentLatLng = LatLng(it.latitude, it.longitude)
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun checkLocationPermission() {
        activity?.let {
            PermissionUtils.requestEachPermissions(activity as AppCompatActivity, PERMISSIONS)
                .take(PERMISSIONS.size.toLong()).toList()
                .subscribe({ data ->
                    if (data.none { !it.granted }) {
                        showMapView()
                    } else {
                        showUnapprovedMapView()
                    }
                }, { ErrorHandler.logError(it) })
        }
    }

    private fun showUnapprovedMapView() {
        hideProgressBarFrame(binding?.flProgressBar)
        binding?.let {
            it.llUnapprovedMapContainer.visibility = View.VISIBLE
            it.btnUnapprovedMap.setOnClickListener { checkLocationPermission() }
            it.flMapContainer.visibility = View.INVISIBLE
        }
    }

    private fun showMapView() {
        binding?.let {
            it.flMapContainer.visibility = View.VISIBLE
            it.llUnapprovedMapContainer.visibility = View.INVISIBLE
        }
        initStores()
        isMapInitted = true
    }

    private fun initStores() {
        (childFragmentManager.findFragmentById(R.id.f_map) as SupportMapFragment).getMapAsync { onMapReady(it) }
        initLocationProcessing()
        viewModel?.let {
            activity?.let { activity ->
                it.observeStores(currentLatLng).observe(this, Observer {
                    it?.let { stores -> initMarkers(googleMap, stores) }
                })
            }
        }
    }

    private fun initMarkers(googleMap: GoogleMap?, stores: List<Store>) {
        googleMap?.let {
            it.clear()
            hideStoreCard()
            lastClickedMarker = null
            stores.addMarkers(it)
            it.setOnMarkerClickListener()
            hideProgressBarFrame(binding?.flProgressBar)
        }
    }

    private fun List<Store>.addMarkers(googleMap: GoogleMap?) = forEach { store ->
        val markerLatLng = LatLng(store.latitude, store.longitude)
        googleMap?.addMarker(
            MarkerOptions()
                .snippet(store.id)
                .position(markerLatLng)
                .icon(
                    bitmapDescriptorFromVector(
                        activity as Context,
                        R.drawable.ic_default_marker
                    )
                )
        )
    }

    private fun GoogleMap.setOnMarkerClickListener() = setOnMarkerClickListener {
        hideStoreCard()
        lastClickedMarker = it
        it.setIcon(bitmapDescriptorFromVector(activity as Context, R.drawable.ic_active_marker))
        Handler().postDelayed({ showStoreCard(it.snippet) }, TRANSLATE_ANIMATION_DURATION)
        false
    }

    @SuppressLint("CheckResult")
    private fun showStoreCard(storeid: String) {
        binding?.let { binding ->
            binding.flStoreCardProgressBar.visibility = View.VISIBLE
            storeCardUp()
            viewModel?.let {
                it.observeSpecificStore(storeid)
                    .subscribe({
                        initStoreCard(it)
                        binding.flStoreCardProgressBar.visibility = View.INVISIBLE
                    }, { ErrorHandler.logError(it) })
            }
        }
    }

    private fun hideStoreCard() {
        storeCardDown()
        activity?.let {
            lastClickedMarker?.let {
                try {
                    it.setIcon(bitmapDescriptorFromVector(activity as Context, R.drawable.ic_default_marker))
                } catch (e: IllegalArgumentException) {
                    ErrorHandler.logError(e)
                }
            }
        }
        lastClickedMarker = null
    }

    private fun storeCardUp() {
        val animator = ValueAnimator.ofFloat(TRANSLATE_Y, defaultStoreCardPosition)
        animator.duration = TRANSLATE_ANIMATION_DURATION
        animator.addUpdateListener { animation -> binding?.cvStoreCard?.y = animation.animatedValue as Float }
        animator.start()
    }

    private fun storeCardDown() {
        if (binding?.cvStoreCard?.y == defaultStoreCardPosition) {
            val animator = ValueAnimator.ofFloat(defaultStoreCardPosition, TRANSLATE_Y)
            animator.duration = TRANSLATE_ANIMATION_DURATION
            animator.addUpdateListener { animation -> binding?.cvStoreCard?.y = animation.animatedValue as Float }
            animator.start()
        }
    }

    private fun onTouch(view: View, event: MotionEvent): Boolean {
        val startY = event.rawY
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                deltaY = view.y - event.rawY
            }
            MotionEvent.ACTION_MOVE ->
                if (Math.abs(startY + deltaY) > 300) {
                    hideStoreCard()
                }
            else -> return false
        }
        return Math.abs(startY + deltaY) > 300
    }

    private fun initStoreCard(store: Store) {
        binding?.storeCard?.store = store
        binding?.storeCard?.executePendingBindings()
        binding?.storeCard?.ivPhoto?.let {
            initImageViewViaGlide(it, store.photoUrl)
        }
        initDistanceView(store.latitude, store.longitude, currentLatLng)
        binding?.storeCard?.tvOpenStatus?.let {
            initOpenStatusView(
                binding?.storeCard,
                it,
                store.startWorkingSession,
                store.endWorkingSession,
                store.blocked
            )
        }
        initOnStoreClickListener(store)
    }

    private fun initDistanceView(latitude: Double, longitude: Double, latLng: LatLng) {
        binding?.storeCard?.let {
            val loc1 = Location("")
            loc1.latitude = latLng.latitude
            loc1.longitude = latLng.longitude

            val loc2 = Location("")
            loc2.latitude = latitude
            loc2.longitude = longitude

            it.tvDistance.text =
                getKmDistance(loc1.distanceTo(loc2).toInt()) + " " + binding?.root?.context?.resources?.getString(R.string.km)
        }

    }

    private fun initOnStoreClickListener(store: Store) {
        binding?.storeCard?.let {
            it.root.setOnClickListener {
                Handler().postDelayed({
                    if (binding?.cvStoreCard?.y == defaultStoreCardPosition) {
                        StoreActivity.start(activity as AppCompatActivity, store.id, false)
                    }
                }, TRANSLATE_ANIMATION_DURATION)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        this.googleMap.let {
            it?.isMyLocationEnabled = true
            it?.uiSettings?.isMyLocationButtonEnabled = true
            it?.uiSettings?.isCompassEnabled = true
            val kyiv = LatLng(50.4350207, 30.5281623)
            it?.moveCamera(CameraUpdateFactory.newLatLngZoom(kyiv, 11f))
        }
    }
}