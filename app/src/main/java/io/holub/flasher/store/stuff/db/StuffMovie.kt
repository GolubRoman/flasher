package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class StuffMovie(
    @PrimaryKey
    @NonNull
    val movieId: String,
    val youtubeMovieId: String,
    val youtubeMoviePhotoUrl: String,
    val stuffId: String
)