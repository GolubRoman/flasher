package io.holub.flasher.store.category

import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityCategoryBinding
import io.holub.flasher.store.category.filters.FiltersDialogFragment
import io.holub.flasher.store.stuff.StuffActivity
import io.holub.flasher.store.stuff_list.StuffsAdapter
import io.reactivex.android.schedulers.AndroidSchedulers

class CategoryActivity : AppCompatActivity() {

    private var binding: ActivityCategoryBinding? = null
    private var viewModel: CategoryViewModel? = null
    private var adapter: StuffsAdapter? = null
    private var currentAnimator: AnimatorSet? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category)
        viewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        intent?.getStringExtra(CATEGORY_ID)?.let {
            initStuffsList(it)
            binding?.btnFilters?.setOnClickListener { view ->
                showFiltersDialog(this, it)
            }
        }
    }

    private fun CategoryViewModel.processSpecificCategoryForCategoryId(categoryId: String) =
        observeCategoryForCategoryId(categoryId)
            .observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                binding?.tvCategoryName?.text = it.name
            }, { ErrorHandler.logError(it) })

    private fun StuffsAdapter.processSpecificStuffClicked() = observeOnStuffClicked()
        .subscribe({
            StuffActivity.start(this@CategoryActivity, it.stuffId, false)
        }, { ErrorHandler.logError(it) })

    private fun StuffsAdapter.processSpecificStuffBuyClicked() = observeOnBuyStuffClicked()
        .subscribe({
            viewModel?.addStuffToCart(it)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ currentAnimator = showOnCompletedView(binding?.ivCompleted, currentAnimator) }, { ErrorHandler.logError(it) })
        }, { ErrorHandler.logError(it) })

    private fun CategoryViewModel.processFilteredStuffsForCategoryId(categoryId: String) = observeFilteredStuffsForCategoryId(categoryId)
        .observeOn(AndroidSchedulers.mainThread())
        ?.subscribe({
            adapter?.submitList(it)
            if (it.isEmpty()) {
                showEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
            } else {
                hideEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
            }
            hideProgressBarFrame(binding?.flProgressBar)
        }, { ErrorHandler.logError(it) })

    private fun CategoryViewModel.processFilteresChanged(categoryId: String) = observeOnFiltersChanged()
        .observeOn(AndroidSchedulers.mainThread())
        ?.subscribe({
            viewModel?.observeFilteredStuffsForCategoryId(categoryId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    adapter?.submitList(it)
                    if (it.isEmpty()) {
                        showEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
                    } else {
                        hideEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
                    }
                    hideProgressBarFrame(binding?.flProgressBar)
                }, { ErrorHandler.logError(it) })
        }, { ErrorHandler.logError(it) })

    @SuppressLint("CheckResult")
    private fun initStuffsList(categoryId: String) {
        showProgressBarFrame(binding?.flProgressBar)
        initSwipeToRefreshView(binding?.srlSwipeToRefresh, binding?.flProgressBar)

        viewModel?.processSpecificCategoryForCategoryId(categoryId)

        adapter = StuffsAdapter()
        binding?.rvStuffs?.layoutManager = LinearLayoutManager(this)
        binding?.rvStuffs?.adapter = adapter
        adapter?.processSpecificStuffClicked()
        adapter?.processSpecificStuffBuyClicked()

        viewModel?.processFilteredStuffsForCategoryId(categoryId)
        viewModel?.processFilteresChanged(categoryId)
    }

    private fun showFiltersDialog(context: Context?, categoryId: String) {
        if (context != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            val previousDialog = supportFragmentManager.findFragmentByTag("dialog")
            if (previousDialog != null) {
                fragmentTransaction.remove(previousDialog)
            }
            fragmentTransaction.addToBackStack(null)
            val dialogFragment = FiltersDialogFragment.newInstance(categoryId)
            dialogFragment.show(fragmentTransaction, "dialog")
        }

    }

    companion object {

        val CATEGORY_ID = "CATEGORY_ID"

        fun start(currentActivity: AppCompatActivity, categoryId: String, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }

            val intent = Intent(currentActivity, CategoryActivity::class.java)
            intent.putExtra(CATEGORY_ID, categoryId)
            currentActivity.startActivity(intent)
        }
    }
}
