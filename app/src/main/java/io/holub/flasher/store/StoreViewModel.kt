package io.holub.flasher.store

import android.arch.lifecycle.ViewModel
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import javax.inject.Inject

class StoreViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    init {
        DaggerStoreViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(storeViewModel: StoreViewModel)
    }
}


