package io.holub.flasher.store.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class StoreCategory(
    @PrimaryKey
    @NonNull
    var id: String,
    var storeId: String,
    var parentCategoryId: String
)
