package io.holub.flasher.store.stuff_list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Stuff
import io.reactivex.Completable
import javax.inject.Inject

class StuffsListInCategoryViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerStuffsListInCategoryViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeStuffsForStoreCategoryIdAndStoreId(storeId: String, storeCategoryId: String): LiveData<PagedList<Stuff>> {
        return storeInteractor.observeStuffsForStoreCategoryIdAndStoreId(0, storeId, storeCategoryId)
    }

    fun addStuffToCart(stuff: Stuff): Completable {
        return orderInteractor.addStuffToCart(stuff)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(stuffsListInCategoryViewModel: StuffsListInCategoryViewModel)
    }
}


