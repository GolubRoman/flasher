package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Embedded

data class StuffSubCharacteristicWithSubCharacteristic(
    @Embedded
    val subCharacteristic: SubCharacteristic,
    @Embedded
    val stuffSubCharacteristic: StuffSubCharacteristic
)