package io.holub.flasher.store.store_list

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initImageViewViaGlide
import io.holub.flasher.common.util.initOpenStatusView
import io.holub.flasher.databinding.ItemStoreBigBinding
import io.holub.flasher.store.db.Store
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class StorePagedAdapter : PagedListAdapter<Store, StorePagedAdapter.StoreViewHolder>(DIFF_CALLBACK) {

    private val onStoreClickedPublishSubject = PublishSubject.create<Store>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val binding = DataBindingUtil.inflate<ItemStoreBigBinding>(
            LayoutInflater.from(parent.context), R.layout.item_store_big, parent, false
        )
        return StoreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    fun observeOnStoreClicked(): Observable<Store> {
        return onStoreClickedPublishSubject
    }

    inner class StoreViewHolder(val binding: ItemStoreBigBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(store: Store?) {
            if (store != null) {
                binding.store = store
                binding.executePendingBindings()
                initImageViewViaGlide(binding.ivPhoto, store.photoUrl)
                initOpenStatusView(
                    binding,
                    binding.tvOpenStatus,
                    store.startWorkingSession,
                    store.endWorkingSession,
                    store.blocked
                )
                initOnStoreClickListener()
            }
        }

        private fun initOnStoreClickListener() {
            binding.store?.let { store ->
                binding.root.setOnClickListener { onStoreClickedPublishSubject.onNext(store) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Store> = object : DiffUtil.ItemCallback<Store>() {
            override fun areItemsTheSame(
                item1: Store, item2: Store
            ): Boolean {
                return item1.id == item2.id
            }

            override fun areContentsTheSame(
                item1: Store, item2: Store
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
