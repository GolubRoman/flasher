package io.holub.flasher.store.stuff

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.holub.flasher.R
import io.holub.flasher.databinding.ItemSubCharacteristicBinding
import io.holub.flasher.store.stuff.db.StuffSubCharacteristicWithSubCharacteristicWithCharacteristic
import io.holub.flasher.store.stuff.db.SubCharacteristicType

class SubCharacteristicsAdapter :
    PagedListAdapter<StuffSubCharacteristicWithSubCharacteristicWithCharacteristic, SubCharacteristicsAdapter.SubCharactericViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCharactericViewHolder {
        val binding = DataBindingUtil.inflate<ItemSubCharacteristicBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_sub_characteristic, parent, false
        )
        return SubCharactericViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SubCharactericViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    private fun showTitleIfPreviousIsFromAnotherCharacteristic(previous: Int, current: Int, titleView: TextView) {
        val previousItem = try {
            (getItem(previous) as StuffSubCharacteristicWithSubCharacteristicWithCharacteristic).characteristic
        } catch (e: java.lang.Exception) {
            null
        }
        val currentItem =
            (getItem(current) as StuffSubCharacteristicWithSubCharacteristicWithCharacteristic).characteristic
        if (previousItem == null || currentItem.characteristicId != previousItem.characteristicId) {
            titleView.visibility = View.VISIBLE
            titleView.text = currentItem.name
        } else {
            titleView.visibility = View.GONE
        }
    }

    inner class SubCharactericViewHolder(val binding: ItemSubCharacteristicBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindTo(stuffSubCharacteristicWithSubCharacteristic: StuffSubCharacteristicWithSubCharacteristicWithCharacteristic?) {
            if (stuffSubCharacteristicWithSubCharacteristic != null) {
                binding.stuffSubCharacteristicWithSubCharacteristic = stuffSubCharacteristicWithSubCharacteristic
                binding.executePendingBindings()
                showTitleIfPreviousIsFromAnotherCharacteristic(
                    adapterPosition - 1,
                    adapterPosition,
                    binding.tvCharacteristicName
                )
                val stuffSubCharacteristic = stuffSubCharacteristicWithSubCharacteristic.stuffSubCharacteristic
                binding.tvValue.text = when (stuffSubCharacteristic.type) {
                    SubCharacteristicType.STRING -> stuffSubCharacteristic.stringValue
                    SubCharacteristicType.INT -> stuffSubCharacteristic.intValue.toString()
                    SubCharacteristicType.FLOAT -> stuffSubCharacteristic.floatValue.toString()
                    SubCharacteristicType.BOOLEAN -> stuffSubCharacteristic.booleanValue.toString()
                    SubCharacteristicType.DROP -> stuffSubCharacteristic.stringValue
                    else -> "None"
                }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<StuffSubCharacteristicWithSubCharacteristicWithCharacteristic> =
            object : DiffUtil.ItemCallback<StuffSubCharacteristicWithSubCharacteristicWithCharacteristic>() {
                override fun areItemsTheSame(
                    item1: StuffSubCharacteristicWithSubCharacteristicWithCharacteristic,
                    item2: StuffSubCharacteristicWithSubCharacteristicWithCharacteristic
                ): Boolean {
                    return item1.stuffSubCharacteristic.stuffSubCharacteristicId == item2.stuffSubCharacteristic.stuffSubCharacteristicId
                }

                override fun areContentsTheSame(
                    item1: StuffSubCharacteristicWithSubCharacteristicWithCharacteristic,
                    item2: StuffSubCharacteristicWithSubCharacteristicWithCharacteristic
                ): Boolean {
                    return item1.stuffSubCharacteristic == item2.stuffSubCharacteristic
                }
            }
    }
}
