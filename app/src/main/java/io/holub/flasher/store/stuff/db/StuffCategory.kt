package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class StuffCategory(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "stuffCategoryId")
    var id: String,
    var stuffId: String,
    var categoryId: String
)
