package io.holub.flasher.store

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.io.db.CategoryDto
import io.holub.flasher.common.io.db.DiscountOfferDto
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.io.db.StoreDto
import io.holub.flasher.common.io.location.NotificationManager
import io.holub.flasher.common.util.buildDbConfig
import io.holub.flasher.common.util.distance
import io.holub.flasher.main.DiscountOffer
import io.holub.flasher.store.category.filters.SubCharacteristicWithCharacteristic
import io.holub.flasher.store.db.*
import io.holub.flasher.store.stuff.db.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class StoreInteractor(
    private val prefs: Prefs,
    private val storeDao: StoreDao,
    private val firebaseDatabase: FirebaseDatabase,
    private val notificationManager: NotificationManager
) {

    val onFiltersChangedPublishSubject = PublishSubject.create<Boolean>()

    fun observeAllStoresList(latLng: LatLng, pageSize: Int): LiveData<PagedList<Store>> {
        return LivePagedListBuilder(
            storeDao.getAllStoresDataSourceFactory(latLng.latitude, latLng.longitude),
            buildDbConfig(pageSize)
        ).build()
    }

    fun dropAllFiltersForCategoryId(categoryId: String): Completable {
        return storeDao.observeCharacteristicsForCategoryIdMaybe(categoryId)
            .flatMap {
                Maybe.fromCallable {
                    it.forEach {
                        storeDao.getSubCharacteristicsForCharacteristicId(it.characteristicId)
                            .forEach {
                                when (it.type) {
                                    SubCharacteristicType.STRING -> {
                                        it.stringValue = ""
                                    }
                                    SubCharacteristicType.FLOAT -> {
                                        it.floatEnd = it.floatRangeEnd
                                        it.floatStart = it.floatRangeStart
                                    }
                                    SubCharacteristicType.INT -> {
                                        it.intEnd = it.intRangeEnd
                                        it.intStart = it.intRangeStart
                                    }
                                    SubCharacteristicType.BOOLEAN -> {
                                        it.booleanValue = false
                                    }
                                    SubCharacteristicType.DROP -> {
                                        it.stringValue = ""
                                    }
                                    else -> {
                                        it.stringValue = ""
                                    }
                                }
                                storeDao.upsertSubCharacteristic(it)
                            }
                    }
                    onFiltersChangedPublishSubject.onNext(true)
                }
            }.ignoreElement()
    }

    fun observeSubCharacteristicsForCharacteristicsId(
        characteristicsId: List<String>,
        pageSize: Int
    ): LiveData<PagedList<SubCharacteristicWithCharacteristic>> {
        return LivePagedListBuilder(
            storeDao.observeSubCharacteristicsForCharacteristicIds(characteristicsId),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeCharacteristicsForCategoryId(categoryId: String): Maybe<List<Characteristic>> {
        return storeDao.observeCharacteristicsForCategoryIdMaybe(categoryId)
    }

    @SuppressLint("CheckResult")
    fun upsertSubCharacteristic(subCharacteristic: SubCharacteristic) {
        Completable.fromCallable {
            storeDao.upsertSubCharacteristic(subCharacteristic)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ onFiltersChangedPublishSubject.onNext(true) }, { ErrorHandler.logError(it) })
    }

    fun observeCategoryForCategoryId(categoryId: String): Maybe<Category> {
        return storeDao.observeCategoryForCategoryId(categoryId)
    }

    fun observeSubCharacteristicsForStuffId(
        stuffId: String,
        pageSize: Int
    ): LiveData<PagedList<StuffSubCharacteristicWithSubCharacteristicWithCharacteristic>> {
        return LivePagedListBuilder(
            storeDao.observeSubCharacteristicsForStuffIdLiveData(stuffId),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeStuffPhotos(stuffId: String): LiveData<List<StuffPhoto>> {
        return storeDao.observeStuffPhotos(stuffId)
    }

    fun observeStuffMovies(stuffId: String): LiveData<List<StuffMovie>> {
        return storeDao.observeStuffMovies(stuffId)
    }

    fun observeStuffsForStoreCategoryIdAndStoreId(
        pageSize: Int,
        storeId: String,
        storeCategoryId: String
    ): LiveData<PagedList<Stuff>> {
        return LivePagedListBuilder(
            storeDao.observeStuffsForStoreCategoryIdAndStoreId(storeId, storeCategoryId),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeStoreCategoriesForStoreId(
        pageSize: Int,
        storeId: String
    ): LiveData<PagedList<CategoryWithStoreCategory>> {
        return LivePagedListBuilder(
            storeDao.observeStoreCategoriesForStoreId(storeId),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeStoreForStoreId(storeId: String): Maybe<Store> {
        return storeDao.observeSpecificStoreForId(storeId)
    }

    fun observeAllStoresLiveData(pageSize: Int): LiveData<PagedList<Store>> {
        return LivePagedListBuilder(
            storeDao.observeAllStoresLiveData(),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeStoresForRegexLiveData(regex: String, pageSize: Int): LiveData<PagedList<Store>> {
        return LivePagedListBuilder(
            storeDao.observeStoresRegexLiveData(regex.replace(" ", "%")),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeSpecificStoreForIdMaybe(id: String): Maybe<Store> {
        return storeDao.observeSpecificStoreForId(id)
            .subscribeOn(Schedulers.io())
    }

    fun observeSpecificStuffForStuffIdLiveData(id: String): LiveData<Stuff> {
        return storeDao.observeSpecificStuffForStuffIdLiveData(id)
    }

    fun observeDiscountOffers(): LiveData<List<DiscountOffer>> {
        return storeDao.observeDiscountOffers()
    }

    fun observeCategoriesLiveData(pageSize: Int): LiveData<PagedList<Category>> {
        return LivePagedListBuilder(storeDao.observeCategoriesLiveData(), buildDbConfig(pageSize)).build()
    }

    fun observeCategoriesForRegexLiveData(regex: String, pageSize: Int): LiveData<PagedList<Category>> {
        return LivePagedListBuilder(
            storeDao.observeCategoriesForRegexLiveData(regex.replace(" ", "%")),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeTopSalesLiveData(pageSize: Int): LiveData<PagedList<Stuff>> {
        return LivePagedListBuilder(storeDao.observeTopSalesLiveData(), buildDbConfig(pageSize)).build()
    }


    fun observeOnFiltersChanged(): Observable<Boolean> {
        return onFiltersChangedPublishSubject
    }

    @SuppressLint("CheckResult")
    fun filteredStuffsForCategory(categoryId: String): Maybe<List<Stuff>> {
        val UNIVERSAL_VALUE = "All"
        return observeCharacteristicsForCategoryId(categoryId)
            .flatMap {
                Maybe.fromCallable {
                    val subCharacteristics =
                        storeDao.getSubCharacteristicsForCharacteristicIds(it.map { it.characteristicId })
                    val stuffSubCharacteristicsWithSubCharacteristics =
                        storeDao.getStuffSubCharacteristicsForSubCharacteristicIds(subCharacteristics.map { it.subCharacteristicId })
                    val filteredListOfStuffIds = stuffSubCharacteristicsWithSubCharacteristics.filter {
                        when (it.stuffSubCharacteristic.type) {
                            SubCharacteristicType.STRING -> {
                                it.stuffSubCharacteristic.stringValue == it.subCharacteristic.stringValue || it.subCharacteristic.stringValue.isEmpty()
                            }
                            SubCharacteristicType.INT -> {
                                it.stuffSubCharacteristic.intValue >= it.subCharacteristic.intStart
                                    && it.stuffSubCharacteristic.intValue <= it.subCharacteristic.intEnd
                            }
                            SubCharacteristicType.BOOLEAN -> {
                                it.stuffSubCharacteristic.booleanValue == it.subCharacteristic.booleanValue
                            }
                            SubCharacteristicType.FLOAT -> {
                                it.stuffSubCharacteristic.floatValue >= it.subCharacteristic.floatStart
                                    && it.stuffSubCharacteristic.floatValue <= it.subCharacteristic.floatEnd
                            }
                            SubCharacteristicType.DROP -> {
                                it.stuffSubCharacteristic.stringValue == it.subCharacteristic.stringValue
                                    || it.subCharacteristic.stringValue.isEmpty() || it.subCharacteristic.stringValue == UNIVERSAL_VALUE
                            }
                        }
                    }.groupBy {
                        it.stuffSubCharacteristic.stuffId
                    }
                        .filter { it.value.size == subCharacteristics.size }
                        .map { it.key }

                    storeDao.getStuffsForStuffIds(filteredListOfStuffIds)
                }
            }
    }

    private fun <T> getFirebaseBlockingCollection(collectionPath: String, type: Class<T>): MutableList<T?> {
        val collectionPublishSubject = PublishSubject.create<MutableList<T?>>()
        firebaseDatabase.getReference(collectionPath).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                collectionPublishSubject.onNext(mutableListOf())
            }

            override fun onDataChange(it: DataSnapshot) {
                collectionPublishSubject.onNext(it.children.map { it.getValue(type) }.toMutableList())
            }

        })
        return collectionPublishSubject.blockingFirst()

    }

    private fun getFirebaseBlockingDownsyncDate(): Long {
        val valuePublishSubject = PublishSubject.create<Long>()
        firebaseDatabase.getReference("updateDate").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                valuePublishSubject.onNext(-1)
            }

            override fun onDataChange(it: DataSnapshot) {
                valuePublishSubject.onNext(it.getValue(Long::class.java) ?: -1)
            }

        })
        return valuePublishSubject.blockingFirst()

    }

    fun downsyncAllData(): Completable {
        return Completable.fromRunnable {
            val updateDate = getFirebaseBlockingDownsyncDate()
            val localUpdateDate = prefs.get<Long>(Prefs.Key.DOWNSYNC_DATE).blockingGet(-1)
            if (updateDate != -1L && updateDate > localUpdateDate) {
                val categories = getFirebaseBlockingCollection("categories", CategoryDto::class.java)
                categories.forEach {
                    it?.let {
                        storeDao.upsertCategory(it.toCategory())
                        it.characteristics.forEach {
                            storeDao.upsertCharacteristic(it.toCharacteristic())
                            it.subCharacteristics.forEach {
                                storeDao.upsertSubCharacteristic(it.toSubCharacteristic())
                            }
                        }
                    }
                }

                val stores = getFirebaseBlockingCollection("stores", StoreDto::class.java)
                stores.forEach {
                    it?.let {
                        storeDao.upsertStore(it.toStore())
                        it.stuffs.forEach {
                            storeDao.upsertStuff(it.toStuff())
                            storeDao.upsertStuffCategory(it.stuffCategory.toStuffCategory())
                            it.stuffMovies.forEach {
                                storeDao.upsertStuffMovie(it.toStuffMovie())
                            }
                            it.stuffPhotos.forEach {
                                storeDao.upsertStuffPhoto(it.toStuffPhoto())
                            }
                            it.stuffCategory.stuffCharacteristics.forEach {
                                storeDao.upsertStuffCharacteristic(it.toStuffCharacteristic())
                                it.stuffSubCharacteristics.forEach {
                                    storeDao.upsertStuffSubCharacteristic(it.toStuffSubCharacteristic())
                                }
                            }
                        }
                        it.categories.forEach {
                            storeDao.upsertStoreCategory(it.toStoreCategory())
                        }

                    }
                }

                val discountOffers = getFirebaseBlockingCollection("discountOffers", DiscountOfferDto::class.java)
                discountOffers.forEach {
                    it?.let {
                        storeDao.upsertDiscountOffer(it.toDiscountOffer())
                    }
                }

                prefs.blockingPut(Prefs.Key.DOWNSYNC_DATE, updateDate)
            }
        }.subscribeOn(Schedulers.io())
    }

    @SuppressLint("CheckResult")
    fun userIsThere(latitude: Double, longitude: Double) {
        storeDao.observeAllStores()
            .flatMap {
                Maybe.fromCallable {
                    it.filter { distance(latitude, longitude, it.latitude, it.longitude) <= 1.0 }
                        .map {
                            Pair(it, storeDao.getTopDiscountedStuffsForStoreId(it.id))
                        }
                }
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.forEach {
                    notificationManager.showNotification(it.first, it.second)
                }
            }, { ErrorHandler.logError(it) })
    }
}

