package io.holub.flasher.store.top_sales_list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Stuff
import io.reactivex.Completable
import javax.inject.Inject

class TopSalesListViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerTopSalesListViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeTopSalesListLiveData(): LiveData<PagedList<Stuff>> {
        return storeInteractor.observeTopSalesLiveData(0)
    }

    fun addStuffToCart(stuff: Stuff): Completable {
        return orderInteractor.addStuffToCart(stuff)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(topSalesListViewModel: TopSalesListViewModel)
    }
}


