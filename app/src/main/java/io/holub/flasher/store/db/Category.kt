package io.holub.flasher.store.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
class Category {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "categoryId")
    var id: String
    @ColumnInfo(name = "categoryName")
    var name: String
    var photoUrl: String
    @ColumnInfo(name = "categoryDeleted")
    var deleted: Boolean

    constructor(id: String, name: String, photoUrl: String, deleted: Boolean) {
        this.id = id
        this.name = name
        this.photoUrl = photoUrl
        this.deleted = deleted

    }
}
