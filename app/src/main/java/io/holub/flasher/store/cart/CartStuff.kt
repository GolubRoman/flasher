package io.holub.flasher.store.cart

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class CartStuff(
    @PrimaryKey
    @ColumnInfo(name = "fullStuffId")
    @NonNull
    val cartStuffId: String,
    val parentStuffId: String,
    var count: Int
)