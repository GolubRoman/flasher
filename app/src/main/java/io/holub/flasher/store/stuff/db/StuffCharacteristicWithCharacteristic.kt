package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Embedded

data class StuffCharacteristicWithCharacteristic(
    @Embedded
    var characteristic: Characteristic,
    @Embedded
    var stuffCharacteristic: StuffCharacteristic
)