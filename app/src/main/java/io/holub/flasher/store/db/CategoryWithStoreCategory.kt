package io.holub.flasher.store.db

import android.arch.persistence.room.Embedded

class CategoryWithStoreCategory(
    @Embedded
    var category: Category,
    @Embedded
    var storeCategory: StoreCategory
)