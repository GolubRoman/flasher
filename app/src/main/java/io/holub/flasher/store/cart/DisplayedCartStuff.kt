package io.holub.flasher.store.cart

import android.arch.persistence.room.Embedded
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.db.Stuff

data class DisplayedCartStuff(
    @Embedded
    var cartStuff: CartStuff,
    @Embedded
    var stuff: Stuff,
    @Embedded
    var store: Store
)