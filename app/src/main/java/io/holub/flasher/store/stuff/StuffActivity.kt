package io.holub.flasher.store.stuff

import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import com.github.andreilisun.swipedismissdialog.SwipeDismissDialog
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityStuffBinding
import io.holub.flasher.main.OnPageSelectedListener
import io.holub.flasher.store.stuff.db.StuffMovie
import io.holub.flasher.store.stuff.db.StuffPhoto
import io.reactivex.android.schedulers.AndroidSchedulers

class StuffActivity : AppCompatActivity() {

    private var binding: ActivityStuffBinding? = null
    private var viewModel: StuffViewModel? = null
    private var stuffPhotosPagerAdapter: StuffPhotosPagerAdapter? = null
    private var stuffMoviesPagerAdapter: StuffMoviesPagerAdapter? = null
    private var subCharacteristicsAdapter: SubCharacteristicsAdapter? = null
    private var currentAnimator: AnimatorSet? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_stuff)
        viewModel = ViewModelProviders.of(this).get(StuffViewModel::class.java)

        intent?.getStringExtra(STUFF_ID)?.let {
            init(it)
        }
    }

    @SuppressLint("CheckResult")
    private fun init(stuffId: String) {
        showProgressBarFrame(binding?.flProgressBar)
        viewModel?.observeSpecificStuffForStuffIdLiveData(stuffId)
            ?.observe(this, Observer { stuff ->
                stuff?.let {
                    binding?.stuff = stuff
                    binding?.executePendingBindings()
                    if (binding?.tvPrice?.text != binding?.tvPriceWithDiscount?.text) {
                        strikeThroughText(binding?.tvPrice)
                        binding?.tvPriceWithDiscount?.visibility = View.VISIBLE
                    } else {
                        binding?.tvPriceWithDiscount?.visibility = View.GONE
                    }
                    binding?.btnBuy?.setOnClickListener {
                        viewModel?.addStuffToCart(stuff)
                            ?.observeOn(AndroidSchedulers.mainThread())
                            ?.subscribe({
                                currentAnimator = showOnCompletedView(binding?.ivCompleted, currentAnimator)
                            }, { ErrorHandler.logError(it) })
                    }
                    initPhotosSlider(stuffId)
                    initMoviesList(stuffId)
                    initCharacteristics(stuffId)
                }
            })
    }

    private fun StuffViewModel.processStuffPhotos(stuffId: String) = observeStuffPhotos(stuffId)
        .observe(this@StuffActivity, Observer {
            it?.let {
                stuffPhotosPagerAdapter?.submitList(it)
                binding?.tlTabs?.count = it.size
                binding?.tlTabs?.selection = 0
            }
        })

    private fun StuffPhotosPagerAdapter.processSpecificPhotoClicked() = observeOnClickPhotoObservable()
        .observeOn(AndroidSchedulers.mainThread())
        ?.subscribe({
            showPhotoDialog(this@StuffActivity, it)
        }, { ErrorHandler.logError(it) })

    @SuppressLint("CheckResult")
    private fun initPhotosSlider(stuffId: String) {
        stuffPhotosPagerAdapter = StuffPhotosPagerAdapter(this)
        binding?.vpStuffPhotos?.adapter = stuffPhotosPagerAdapter
        binding?.tlTabs?.setViewPager(binding?.vpStuffPhotos)
        binding?.vpStuffPhotos?.addOnPageChangeListener(object : OnPageSelectedListener {
            override fun onPageSelected(position: Int) {
                binding?.tlTabs?.selection = position
            }
        })
        viewModel?.processStuffPhotos(stuffId)
        stuffPhotosPagerAdapter?.processSpecificPhotoClicked()
    }

    override fun onBackPressed() {
        if (stuffMoviesPagerAdapter?.fragments?.none { it.isFullScreenEnabled() } == true) {
            super.onBackPressed()
        }
    }

    @SuppressLint("CheckResult")
    private fun initMoviesList(stuffId: String) {
        viewModel?.observeStuffMovies(stuffId)
            ?.observe(this, Observer {
                it?.let {
                    val fragments = it.map { StuffMovieFragment() }
                    stuffMoviesPagerAdapter = StuffMoviesPagerAdapter(supportFragmentManager, fragments)
                    binding?.vpMovies?.adapter = stuffMoviesPagerAdapter
                    binding?.tlMoviesTabs?.setViewPager(binding?.vpMovies)
                    binding?.vpMovies?.offscreenPageLimit = it.size
                    fragments.processStuffMovieFragments(it, 0)
                    processOnPageChange(fragments, it)
                }
            })
    }

    private fun processOnPageChange(fragments: List<StuffMovieFragment>, stuffMovies: List<StuffMovie>) {
        binding?.vpMovies?.addOnPageChangeListener(object : OnPageSelectedListener {
            override fun onPageSelected(position: Int) {
                binding?.tlMoviesTabs?.selection = position
                fragments.processStuffMovieFragments(stuffMovies, position)
            }
        })
    }

    private fun List<StuffMovieFragment>.processStuffMovieFragments(stuffMovies: List<StuffMovie>, position: Int) =
        forEachIndexed { index, stuffMovieFragment ->
            if (index != position) {
                stuffMovieFragment.release()
            } else {
                stuffMovieFragment.init(stuffMovies[position])
            }
        }

    private fun initCharacteristics(stuffId: String) {
        subCharacteristicsAdapter = SubCharacteristicsAdapter()
        binding?.rvCharacteristics?.layoutManager = LinearLayoutManager(this)
        binding?.rvCharacteristics?.adapter = subCharacteristicsAdapter
        viewModel?.observeSubCharacteristicsForStuffId(stuffId)
            ?.observe(this, Observer {
                subCharacteristicsAdapter?.submitList(it)
                hideProgressBarFrame(binding?.flProgressBar)
            })
    }

    fun showPhotoDialog(context: Context?, stuffPhoto: StuffPhoto) {
        if (context != null) {
            val dialog = SwipeDismissDialog.Builder(this)
                .setLayoutResId(R.layout.dialog_stuff_photo)
                .build()
            initImageViewViaGlide(dialog.findViewById(R.id.iv_photo) as ImageView, stuffPhoto.photoUrl)
            dialog.show()
        }

    }

    companion object {

        val STUFF_ID = "STUFF_ID"

        fun start(currentActivity: AppCompatActivity, stuffId: String, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            val intent = Intent(currentActivity, StuffActivity::class.java)
            intent.putExtra(STUFF_ID, stuffId)
            currentActivity.startActivity(intent)
        }
    }
}
