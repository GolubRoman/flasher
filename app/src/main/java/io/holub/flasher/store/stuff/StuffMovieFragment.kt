package io.holub.flasher.store.stuff

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import io.holub.flasher.R
import io.holub.flasher.common.util.hideProgressBarFrame
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.util.showProgressBarFrame
import io.holub.flasher.databinding.FragmentStuffMovieBinding
import io.holub.flasher.store.stuff.db.StuffMovie

class StuffMovieFragment : Fragment() {

    private var binding: FragmentStuffMovieBinding? = null
    private var isInitted = false
    private var currentPlayer: YouTubePlayer? = null
    private var isFullScreenEnabled: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stuff_movie, container, false)
        return binding?.root
    }

    fun init(stuffMovie: StuffMovie) {
        binding?.ivPhoto?.let { initCenterCropImageViewViaGlide(it, stuffMovie.youtubeMoviePhotoUrl) }
        binding?.tvShowMovie?.setOnClickListener {
            if (!isInitted) {
                showProgressBarFrame(binding?.flProgressBar)
                val youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance()

                val transaction = childFragmentManager.beginTransaction()
                transaction?.add(R.id.fl_fragment_container, youTubePlayerFragment)
                transaction?.commit()

                youTubePlayerFragment.initialize(
                    resources.getString(R.string.google_api_key),
                    object : OnInitializationSuccessListener {

                        override fun onInitializationSuccess(
                            playerProvider: YouTubePlayer.Provider?,
                            player: YouTubePlayer?,
                            p2: Boolean
                        ) {
                            currentPlayer = player
                            currentPlayer?.loadVideo(stuffMovie.youtubeMovieId)
                            currentPlayer?.setFullscreen(true)
                            currentPlayer?.setOnFullscreenListener {
                                isFullScreenEnabled = it
                                if (!it) {
                                    isFullScreenEnabled = it
                                    collapseVideo()
                                }
                            }
                            Handler().postDelayed({ hideProgressBarFrame(binding?.flProgressBar) }, 1000)
                        }
                    })

                isInitted = true
            }
            hideYoutubeBlock()
        }
    }

    fun isFullScreenEnabled(): Boolean {
        return isFullScreenEnabled
    }

    fun collapseVideo() {
        currentPlayer?.setFullscreen(false)
        release()
    }

    fun release() {
        showYoutubeBlock()
        currentPlayer?.release()
        isInitted = false
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showYoutubeBlock() {
        binding?.cvPhotoContainer?.visibility = View.VISIBLE
        binding?.cvPhotoContainer?.setOnTouchListener { _, _ -> true }
    }

    private fun hideYoutubeBlock() {
        binding?.cvPhotoContainer?.visibility = View.INVISIBLE
    }

}