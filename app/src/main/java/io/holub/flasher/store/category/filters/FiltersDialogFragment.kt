package io.holub.flasher.store.category.filters

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.showSnackbar
import io.holub.flasher.databinding.DialogFiltersBinding
import io.holub.flasher.store.stuff.db.SubCharacteristic
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers

class FiltersDialogFragment : DialogFragment() {

    private var binding: DialogFiltersBinding? = null
    private var viewModel: FiltersViewModel? = null
    private var subCharacteristicsPagedAdapter: SubCharacteristicsPagedAdapter? = null
    private var filtersConfigurations = mutableMapOf<String, SubCharacteristic>()

    companion object {
        private val CATEGORY_ID = "CATEGORY_ID"

        fun newInstance(categoryId: String): FiltersDialogFragment {
            val filtersDialogFragment = FiltersDialogFragment()

            val args = Bundle()
            args.putString(CATEGORY_ID, categoryId)
            filtersDialogFragment.arguments = args

            return filtersDialogFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_filters, container, false)
        viewModel = ViewModelProviders.of(this).get(FiltersViewModel::class.java)


        arguments?.getString(CATEGORY_ID)?.let {
            initViews(it)
        }
        return binding?.root
    }

    private fun initViews(categoryId: String) {
        binding?.btnCancel?.setOnClickListener { dismiss() }
        binding?.btnDrop?.setOnClickListener {
            viewModel?.dropAllFiltersForCategoryId(categoryId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    binding?.root?.let {
                        showSnackbar(it, R.string.filters_dropped)
                    }
                    dismiss()
                }, { ErrorHandler.logError(it) })
        }
        binding?.btnSubmit?.setOnClickListener {
            filtersConfigurations.forEach { viewModel?.changeSubCharacteristic(it.value) }
            dismiss()
        }
        initSubCharacteristicsList(categoryId)
    }

    @SuppressLint("CheckResult")
    private fun initSubCharacteristicsList(categoryId: String) {
        viewModel?.observeCharacteristicsForCategoryId(categoryId)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                subCharacteristicsPagedAdapter = SubCharacteristicsPagedAdapter()
                binding?.rvCharacteristics?.adapter = subCharacteristicsPagedAdapter
                binding?.rvCharacteristics?.layoutManager = LinearLayoutManager(activity)
                viewModel?.observeSubCharacteristicsForCharacteristicsIdsLiveData(it.map { it.characteristicId })
                    ?.observe(this, Observer {
                        it?.let { subCharacteristicsPagedAdapter?.submitList(it) }
                    })
                subCharacteristicsPagedAdapter?.processFiltersConfigurations()
            }, { ErrorHandler.logError(it) })
    }

    private fun SubCharacteristicsPagedAdapter.processFiltersConfigurations() = observeFiltersConfigurations()
        .flatMapMaybe {
            Maybe.fromCallable {
                filtersConfigurations.put(it.subCharacteristicId, it)
            }
        }.subscribe({}, {ErrorHandler.logError(it)})

    override fun onStart() {
        super.onStart()
        dialog.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
    }
}
