package io.holub.flasher.store.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import java.text.DecimalFormat

@Entity
data class Stuff(
    @PrimaryKey
    @NonNull
    val stuffId: String,
    val storeCategoryId: String,
    val stuffCategoryId: String,
    @SerializedName(value = "storeId")
    val parentStoreId: String,
    @ColumnInfo(name = "stuffName")
    val name: String,
    @ColumnInfo(name = "stuffPhotoUrl")
    val photoUrl: String,
    @ColumnInfo(name = "stuffDescription")
    val description: String,
    @ColumnInfo(name = "stuffPrice")
    val price: Int,
    var salesCount: Int,
    @ColumnInfo(name = "stuffBlocked")
    var blocked: Boolean,
    var discountPercentage: Int,
    @ColumnInfo(name = "stuffDeleted")
    val deleted: Boolean
) {
    fun formatPrice() = DecimalFormat("##.##").format((price * 0.01).toInt())

    fun formatPriceWithDiscount() =
        DecimalFormat("##.##").format((price * (1 - discountPercentage * 0.01) * 0.01).toInt())
}