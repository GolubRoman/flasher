package io.holub.flasher.store.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.util.*

@Entity
class Store {

    @PrimaryKey
    @NonNull
    var id: String
    var merchantId: String
    var name: String
    var photoUrl: String
    var address: String
    var phone: String
    var description: String
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    @Ignore
    var categories: List<StoreCategory>? = null
    var startWorkingSession: Long = 0
    var endWorkingSession: Long = 0
    var deleted: Boolean = false
    var blocked: Boolean = false

    constructor(
        id: String, merchantId: String, name: String, photoUrl: String,
        address: String, phone: String, description: String,
        categories: List<StoreCategory>, startWorkingSession: Long,
        endWorkingSession: Long, longitude: Double, latitude: Double, deleted: Boolean, blocked: Boolean
    ) {
        this.id = id
        this.merchantId = merchantId
        this.name = name
        this.photoUrl = photoUrl
        this.address = address
        this.phone = phone
        this.description = description
        this.categories = categories
        this.startWorkingSession = startWorkingSession
        this.endWorkingSession = endWorkingSession
        this.longitude = longitude
        this.latitude = latitude
        this.deleted = deleted
        this.blocked = blocked
    }

    constructor(
        id: String, merchantId: String, name: String, photoUrl: String,
        address: String, phone: String, description: String,
        startWorkingSession: Long, endWorkingSession: Long, longitude: Double,
        latitude: Double, deleted: Boolean, blocked: Boolean
    ) {
        this.id = id
        this.merchantId = merchantId
        this.name = name
        this.photoUrl = photoUrl
        this.address = address
        this.phone = phone
        this.description = description
        this.categories = Arrays.asList()
        this.startWorkingSession = startWorkingSession
        this.endWorkingSession = endWorkingSession
        this.longitude = longitude
        this.latitude = latitude
        this.deleted = deleted
        this.blocked = blocked
    }

    companion object {

        val STUB_EMPTY_STORE = Store(
            "", "", "",
            "", "", "", "", listOf(), 0, 0, 0.0, 0.0, deleted = false, blocked = false
        )

        fun stubEmptyStore(): Store {
            return STUB_EMPTY_STORE
        }
    }
}