package io.holub.flasher.store.stuff_list

import android.annotation.SuppressLint
import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.util.strikeThroughText
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemStuffBinding
import io.holub.flasher.store.db.Stuff
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class StuffsPagedAdapter : PagedListAdapter<Stuff, StuffsPagedAdapter.StuffViewHolder>(DIFF_CALLBACK) {

    private val onStuffClickedPublishSubject = PublishSubject.create<Stuff>()
    private val onStuffBuyClickedPublishSubject = PublishSubject.create<Stuff>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StuffViewHolder {
        val binding = DataBindingUtil.inflate<ItemStuffBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_stuff,
            parent,
            false
        )
        return StuffViewHolder(binding)
    }

    fun observeOnStuffClicked(): Observable<Stuff> {
        return onStuffClickedPublishSubject
    }

    fun observeOnBuyStuffClicked(): Observable<Stuff> {
        return onStuffBuyClickedPublishSubject
    }

    override fun onBindViewHolder(holder: StuffViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    inner class StuffViewHolder(val binding: ItemStuffBinding) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("CheckResult")
        fun bindTo(stuff: Stuff?) {
            if (stuff != null) {
                binding.stuff = stuff
                binding.executePendingBindings()
                if (binding.tvPrice.text != binding.tvPriceWithDiscount.text) {
                    strikeThroughText(binding.tvPrice)
                    binding.tvPriceWithDiscount.visibility = View.VISIBLE
                } else {
                    binding.tvPriceWithDiscount.visibility = View.GONE
                }
                initCenterCropImageViewViaGlide(binding.ivPhoto, stuff.photoUrl)
                onClick(stuff)
            }
        }

        fun onClick(stuff: Stuff) {
            binding.clRoot.setOnClickListener { onStuffClickedPublishSubject.onNext(stuff) }
            binding.btnBuy.setOnClickListener { onStuffBuyClickedPublishSubject.onNext(stuff) }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Stuff> = object : DiffUtil.ItemCallback<Stuff>() {
            override fun areItemsTheSame(
                item1: Stuff, item2: Stuff
            ): Boolean {
                return item1.stuffId == item2.stuffId
            }

            override fun areContentsTheSame(
                item1: Stuff, item2: Stuff
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
