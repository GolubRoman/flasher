package io.holub.flasher.store.category.filters

import android.text.TextWatcher

interface AfterTextChangedListener: TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}