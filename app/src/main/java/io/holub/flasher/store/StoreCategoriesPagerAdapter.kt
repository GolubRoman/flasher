package io.holub.flasher.store

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import io.holub.flasher.store.store_tab.StoreTabFragment

internal class StoreCategoriesPagerAdapter(
    fm: FragmentManager,
    val fragments: List<StoreTabFragment>,
    val titles: List<String>
) : FragmentPagerAdapter(fm) {

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}