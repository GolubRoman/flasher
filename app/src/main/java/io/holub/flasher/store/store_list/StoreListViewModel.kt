package io.holub.flasher.store.store_list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Store
import javax.inject.Inject

class StoreListViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    var searchRegex = ""

    init {
        DaggerStoreListViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeStoresLiveData(): LiveData<PagedList<Store>> {
        return storeInteractor.observeStoresForRegexLiveData(searchRegex, 0)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(storeListViewModel: StoreListViewModel)
    }
}


