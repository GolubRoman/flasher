package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class Characteristic(
    @PrimaryKey
    @NonNull
    var characteristicId: String,
    var categoryId: String,
    @ColumnInfo(name = "characteristicName")
    var name: String
)
