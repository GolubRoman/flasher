package io.holub.flasher.store.stuff_list

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.hideEmptyListView
import io.holub.flasher.common.util.hideProgressBarFrame
import io.holub.flasher.common.util.showEmptyListView
import io.holub.flasher.common.util.showProgressBarFrame
import io.holub.flasher.databinding.FragmentStuffsListInCategoryBinding
import io.holub.flasher.store.store_tab.StoreTabFragment
import io.holub.flasher.store.stuff.StuffActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

class StuffsListInCategoryFragment : StoreTabFragment() {

    private var binding: FragmentStuffsListInCategoryBinding? = null
    private var viewModel: StuffsListInCategoryViewModel? = null
    private var adapter: StuffsPagedAdapter? = null
    private var onAddStuffToCartEmitter = PublishSubject.create<Boolean>()
    private var isInitted = false

    companion object {
        private val STORE_ID_KEY = "STORE_ID_KEY"
        private val STORE_CATEGORY_ID_KEY = "STORE_CATEGORY_ID_KEY"

        fun newInstance(storeId: String, categoryId: String): StuffsListInCategoryFragment {
            val stuffsListInCategoryFragment = StuffsListInCategoryFragment()

            val args = Bundle()
            args.putString(STORE_CATEGORY_ID_KEY, categoryId)
            args.putString(STORE_ID_KEY, storeId)
            stuffsListInCategoryFragment.arguments = args

            return stuffsListInCategoryFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stuffs_list_in_category, container, false)
        viewModel = ViewModelProviders.of(this).get(StuffsListInCategoryViewModel::class.java)
        return binding?.root
    }

    fun observeOnAddStuffToCartEmitter(): Observable<Boolean> {
        return onAddStuffToCartEmitter
    }

    override fun init() {
        if (!isInitted) {
            showProgressBarFrame(binding?.flProgressBar)
            isInitted = true
            arguments?.let {
                if (it.getString(STORE_ID_KEY) != null && it.getString(STORE_CATEGORY_ID_KEY) != null) {
                    initStuffsList(it.getString(STORE_ID_KEY), it.getString(STORE_CATEGORY_ID_KEY))
                }
            }

        }
    }

    @SuppressLint("CheckResult")
    private fun initStuffsList(storeId: String, storeCategoryId: String) {
        adapter = StuffsPagedAdapter()
        binding?.rvStuffs?.layoutManager = LinearLayoutManager(activity)
        binding?.rvStuffs?.adapter = adapter
        binding?.rvStuffs?.itemAnimator = null
        viewModel?.processStuffsForStoreCategoryIdAndStoreId(storeId, storeCategoryId)
        adapter?.processSpecificStuffClicked()
        adapter?.processSpecificStuffBuyClicked()
    }

    private fun StuffsListInCategoryViewModel.processStuffsForStoreCategoryIdAndStoreId(
        storeId: String,
        storeCategoryId: String
    ) = observeStuffsForStoreCategoryIdAndStoreId(storeId, storeCategoryId)
        .observe(this@StuffsListInCategoryFragment, Observer {
            it?.let {
                adapter?.submitList(it)
                if (it.size == 0) {
                    showEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
                } else {
                    hideEmptyListView(binding?.rvStuffs, binding?.flEmptyList)
                }
            }
            hideProgressBarFrame(binding?.flProgressBar)
        })

    private fun StuffsPagedAdapter.processSpecificStuffClicked() = observeOnStuffClicked()
        .subscribe({
            StuffActivity.start(activity as AppCompatActivity, it.stuffId, false)
        }, { ErrorHandler.logError(it) })

    private fun StuffsPagedAdapter.processSpecificStuffBuyClicked() = observeOnBuyStuffClicked()
        .subscribe({
            viewModel?.addStuffToCart(it)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ onAddStuffToCartEmitter.onNext(true) }, { ErrorHandler.logError(it) })
        }, { ErrorHandler.logError(it) })
}
