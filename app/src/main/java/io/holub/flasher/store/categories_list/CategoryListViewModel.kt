package io.holub.flasher.store.categories_list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Category
import javax.inject.Inject

class CategoryListViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    var searchRegex = ""

    init {
        DaggerCategoryListViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeCategoriesLiveData(): LiveData<PagedList<Category>> {
        return storeInteractor.observeCategoriesForRegexLiveData(searchRegex, 0)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(categoryListViewModel: CategoryListViewModel)
    }
}


