package io.holub.flasher.store

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.holub.flasher.R
import io.holub.flasher.common.util.initTransparentStatusBar
import io.holub.flasher.databinding.ActivityStoreBinding
import io.holub.flasher.store.store_container.StoreContainerFragment

class StoreActivity : AppCompatActivity() {

    private var binding: ActivityStoreBinding? = null
    private var viewModel: StoreViewModel? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_store)
        viewModel = ViewModelProviders.of(this).get(StoreViewModel::class.java)


        if (intent != null && intent?.getStringExtra(STORE_ID) != null) {
            intent?.getStringExtra(STORE_ID)?.let {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.cl_fragment_container, StoreContainerFragment.newInstance(it))
                ft.commit()
            }
        }
    }

    companion object {

        val STORE_ID = "STORE_ID"

        fun start(currentActivity: AppCompatActivity, storeId: String, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            val intent = Intent(currentActivity, StoreActivity::class.java)
            intent.putExtra(STORE_ID, storeId)
            currentActivity.startActivity(intent)
        }
    }
}

