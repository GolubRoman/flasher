package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SubCharacteristicConverters {

    @TypeConverter
    fun stringToType(value: String): SubCharacteristicType? {
        return if (value == null) null else SubCharacteristicType.valueOf(value)
    }

    @TypeConverter
    fun typeToString(type: SubCharacteristicType?): String? {
        return type?.name
    }

    @TypeConverter
    fun stringToList(listOfString: String): List<String> {
        return Gson().fromJson(listOfString, object : TypeToken<List<String>>() {}.type)
    }

    @TypeConverter
    fun listToString(listOfString: List<String>): String {
        return Gson().toJson(listOfString)
    }

}