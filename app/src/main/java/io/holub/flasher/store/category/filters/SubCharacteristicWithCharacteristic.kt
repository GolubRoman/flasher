package io.holub.flasher.store.category.filters

import android.arch.persistence.room.Embedded
import io.holub.flasher.store.stuff.db.Characteristic
import io.holub.flasher.store.stuff.db.SubCharacteristic

class SubCharacteristicWithCharacteristic(
    @Embedded
    var subCharacteristic: SubCharacteristic,
    @Embedded
    var characteristic: Characteristic
)