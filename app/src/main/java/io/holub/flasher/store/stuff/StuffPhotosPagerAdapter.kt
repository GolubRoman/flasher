package io.holub.flasher.store.stuff

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initImageViewViaGlide
import io.holub.flasher.databinding.ItemStuffPhotoBinding
import io.holub.flasher.store.stuff.db.StuffPhoto
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class StuffPhotosPagerAdapter(val context: Context) : PagerAdapter() {

    private var inflater = LayoutInflater.from(context)
    private var stuffPhotos = mutableListOf<StuffPhoto>()
    private var onClickPhotoPublishSubject = PublishSubject.create<StuffPhoto>()

    fun submitList(stuffPhotos: List<StuffPhoto>) {
        this.stuffPhotos.clear()
        this.stuffPhotos.addAll(stuffPhotos)
        notifyDataSetChanged()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return stuffPhotos.size
    }

    fun observeOnClickPhotoObservable(): Observable<StuffPhoto> {
        return onClickPhotoPublishSubject
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val binding = DataBindingUtil.inflate<ItemStuffPhotoBinding>(inflater, R.layout.item_stuff_photo, view, false)

        initImageViewViaGlide(binding.ivPhoto, stuffPhotos[position].photoUrl)
        view.addView(binding.root)
        binding.ivPhoto.setOnClickListener { onClickPhotoPublishSubject.onNext(stuffPhotos[position]) }

        return binding.root
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}