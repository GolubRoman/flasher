package io.holub.flasher.store.categories_list

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityCategoryListBinding
import io.holub.flasher.store.category.CategoryActivity
import io.holub.flasher.store.db.Category

class CategoryListActivity : AppCompatActivity() {

    private var binding: ActivityCategoryListBinding? = null
    private var viewModel: CategoryListViewModel? = null
    private var adapter: CategoriesPagedAdapter? = null
    private var categoriesLiveData: LiveData<PagedList<Category>>? = null
    private var categoriesObserver: Observer<PagedList<Category>>? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category_list)
        viewModel = ViewModelProviders.of(this).get(CategoryListViewModel::class.java)
        initCategoryList()
    }

    @SuppressLint("CheckResult")
    private fun initCategoryList() {
        showProgressBarFrame(binding?.flProgressBar)
        initSwipeToRefreshView(binding?.srlSwipeToRefresh, binding?.flProgressBar)

        adapter = CategoriesPagedAdapter()
        binding?.rvCategories?.layoutManager = GridLayoutManager(this, 2)
        binding?.rvCategories?.adapter = adapter

        adapter?.processCategoryClicked()
        viewModel?.processCategories()
        initSearchInput()
    }

    private fun CategoriesPagedAdapter.processCategoryClicked() = observeOnCategoryClicked().subscribe({
        CategoryActivity.start(this@CategoryListActivity, it.id, false)
    }, { ErrorHandler.logError(it) })

    private fun CategoryListViewModel.processCategories() = observeCategoriesLiveData()
        .observe(this@CategoryListActivity, Observer {
            it?.let {
                adapter?.submitList(it)
                hideProgressBarFrame(binding?.flProgressBar)
            }
        })

    @SuppressLint("CheckResult")
    private fun initSearchInput() {
        binding?.etSearchView?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                if (text?.length ?: 0 > 2) {
                    viewModel?.searchRegex = text.toString()
                    subscribeToStoresResult()
                } else {
                    if (viewModel?.searchRegex != "") {
                        viewModel?.searchRegex = ""
                        subscribeToStoresResult()
                    }
                }

                if (text?.length ?: 0 > 0) {
                    binding?.ivClear?.visibility = View.VISIBLE
                    binding?.ivClear?.setOnClickListener {
                        binding?.etSearchView?.setText("")
                        binding?.ivClear?.visibility = View.INVISIBLE
                    }
                } else {
                    binding?.ivClear?.visibility = View.INVISIBLE
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun subscribeToStoresResult() {
        if (categoriesLiveData != null && categoriesObserver != null) {
            categoriesLiveData!!.removeObserver(categoriesObserver!!)
            categoriesLiveData = null
            categoriesObserver = null
        }
        categoriesObserver = Observer {
            adapter?.submitList(it)
            if (it?.size ?: 0 == 0) {
                showEmptyListView(binding?.rvCategories, binding?.flEmptyList)
            } else {
                hideEmptyListView(binding?.rvCategories, binding?.flEmptyList)
            }
            Handler().postDelayed({ hideProgressBarFrame(binding?.flProgressBar) }, 1000)
        }
        categoriesLiveData = viewModel?.observeCategoriesLiveData()

        categoriesLiveData?.let { nonNullCategoriesLiveData ->
            categoriesObserver?.let { nonNullCategoriesObserver ->
                nonNullCategoriesLiveData.observe(this, nonNullCategoriesObserver)
            }
        }

    }

    companion object {

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, CategoryListActivity::class.java))
        }
    }
}

