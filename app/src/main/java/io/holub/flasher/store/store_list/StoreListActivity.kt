package io.holub.flasher.store.store_list

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.view.View
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityStoreListBinding
import io.holub.flasher.store.StoreActivity
import io.holub.flasher.store.category.filters.AfterTextChangedListener
import io.holub.flasher.store.db.Store

class StoreListActivity : AppCompatActivity() {

    private var binding: ActivityStoreListBinding? = null
    private var viewModel: StoreListViewModel? = null
    private var adapter: StorePagedAdapter? = null
    private var storesLiveData: LiveData<PagedList<Store>>? = null
    private var storesObserver: Observer<PagedList<Store>>? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_store_list)
        viewModel = ViewModelProviders.of(this).get(StoreListViewModel::class.java)
        initStoreList()
    }

    @SuppressLint("CheckResult")
    private fun initStoreList() {
        showProgressBarFrame(binding?.flProgressBar)
        initSwipeToRefreshView(binding?.srlSwipeToRefresh, binding?.flProgressBar)

        adapter = StorePagedAdapter()
        binding?.rvStores?.layoutManager = LinearLayoutManager(this)
        binding?.rvStores?.adapter = adapter
        adapter?.processStoreClicked()
        viewModel?.processStores()

        initSearchInput()
    }

    private fun StorePagedAdapter.processStoreClicked() = observeOnStoreClicked().subscribe({
        StoreActivity.start(this@StoreListActivity, it.id, false)
    }, { ErrorHandler.logError(it) })

    private fun StoreListViewModel.processStores() = observeStoresLiveData().observe(this@StoreListActivity, Observer {
        it?.let {
            adapter?.submitList(it)
            hideProgressBarFrame(binding?.flProgressBar)
        }
    })

    @SuppressLint("CheckResult")
    private fun initSearchInput() {
        binding?.etSearchView?.addTextChangedListener(object : AfterTextChangedListener {
            override fun afterTextChanged(text: Editable?) {
                if (text?.length ?: 0 > 2) {
                    viewModel?.searchRegex = text.toString()
                    subscribeToStoresResult()
                } else {
                    if (viewModel?.searchRegex != "") {
                        viewModel?.searchRegex = ""
                        subscribeToStoresResult()
                    }
                }

                if (text?.length ?: 0 > 0) {
                    binding?.ivClear?.visibility = View.VISIBLE
                    binding?.ivClear?.setOnClickListener {
                        binding?.etSearchView?.setText("")
                        binding?.ivClear?.visibility = View.INVISIBLE
                    }
                } else {
                    binding?.ivClear?.visibility = View.INVISIBLE
                }
            }
        })
    }

    private fun subscribeToStoresResult() {
        if (storesLiveData != null && storesObserver != null) {
            storesLiveData!!.removeObserver(storesObserver!!)
            storesLiveData = null
            storesObserver = null
        }
        storesObserver = Observer {
            adapter?.submitList(it)
            if (it?.size ?: 0 == 0) {
                showEmptyListView(binding?.rvStores, binding?.flEmptyList)
            } else {
                hideEmptyListView(binding?.rvStores, binding?.flEmptyList)
            }
            Handler().postDelayed({ hideProgressBarFrame(binding?.flProgressBar) }, 1000)
        }
        storesLiveData = viewModel?.observeStoresLiveData()
        if (storesLiveData != null && storesObserver != null) {
            storesLiveData!!.observe(this, storesObserver!!)
        }

    }

    companion object {

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, StoreListActivity::class.java))
        }
    }
}

