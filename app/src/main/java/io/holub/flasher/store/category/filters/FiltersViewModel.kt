package io.holub.flasher.store.category.filters

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.stuff.db.Characteristic
import io.holub.flasher.store.stuff.db.SubCharacteristic
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FiltersViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    init {
        DaggerFiltersViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeSubCharacteristicsForCharacteristicsIdsLiveData(characteristicsIds: List<String>): LiveData<PagedList<SubCharacteristicWithCharacteristic>> {
        return storeInteractor.observeSubCharacteristicsForCharacteristicsId(characteristicsIds, 0)
    }

    fun observeCharacteristicsForCategoryId(categoryId: String): Maybe<List<Characteristic>> {
        return storeInteractor.observeCharacteristicsForCategoryId(categoryId)
            .subscribeOn(Schedulers.io())
    }

    fun changeSubCharacteristic(subCharacteristic: SubCharacteristic) {
        storeInteractor.upsertSubCharacteristic(subCharacteristic)
    }

    fun dropAllFiltersForCategoryId(categoryId: String): Completable {
        return storeInteractor.dropAllFiltersForCategoryId(categoryId)
            .subscribeOn(Schedulers.io())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(filtersViewModel: FiltersViewModel)
    }
}


