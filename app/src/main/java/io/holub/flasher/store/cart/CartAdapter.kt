package io.holub.flasher.store.cart

import android.annotation.SuppressLint
import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.holub.flasher.R
import io.holub.flasher.common.util.formatDoubleSumPrice
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.util.strikeThroughText
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemCartStuffBinding
import io.holub.flasher.tab.TabViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class CartAdapter(val viewModel: TabViewModel) :
    PagedListAdapter<DisplayedCartStuff, CartAdapter.CartViewHolder>(DIFF_CALLBACK) {

    private val onConfirmOrderClickedPublishSubject = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = DataBindingUtil.inflate<ItemCartStuffBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_cart_stuff,
            parent,
            false
        )
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    fun observeOnConfirmOrderClickedObservable(): Observable<String> {
        return onConfirmOrderClickedPublishSubject
    }

    @SuppressLint("SetTextI18n")
    private fun showTitleAndConfirmButtonIfPreviousIsFromAnotherStore(
        previous: Int,
        current: Int,
        titleView: TextView,
        btnView: Button
    ) {
        val previousItem = try {
            (getItem(previous) as DisplayedCartStuff).store
        } catch (e: java.lang.Exception) {
            null
        }
        val currentItem = (getItem(current) as DisplayedCartStuff).store
        if (previousItem == null || currentItem.id != previousItem.id) {
            titleView.visibility = View.VISIBLE
            btnView.visibility = View.VISIBLE
            titleView.text = currentItem.name
            btnView.text = formatDoubleSumPrice(
                currentList?.snapshot()
                    ?.map { it.stuff.formatPriceWithDiscount().toDouble() * it.cartStuff.count }
                    ?.sum() ?: 0.0) + "₴ " + titleView.resources.getString(R.string.confirm)

            btnView.setOnClickListener {
                onConfirmOrderClickedPublishSubject.onNext(currentItem.id)
            }
        } else {
            titleView.visibility = View.GONE
            btnView.visibility = View.GONE
        }
    }

    inner class CartViewHolder(val binding: ItemCartStuffBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(displayedCartStuff: DisplayedCartStuff?) {
            if (displayedCartStuff != null) {
                binding.displayedCartStuff = displayedCartStuff
                binding.executePendingBindings()
                if (binding.tvPrice.text != binding.tvPriceWithDiscount.text) {
                    strikeThroughText(binding.tvPrice)
                    binding.tvPriceWithDiscount.visibility = View.VISIBLE
                } else {
                    binding.tvPriceWithDiscount.visibility = View.GONE
                }
                showTitleAndConfirmButtonIfPreviousIsFromAnotherStore(
                    adapterPosition - 1,
                    adapterPosition,
                    binding.tvStoreName,
                    binding.btnConfirm
                )
                initButtons(displayedCartStuff)
                initCenterCropImageViewViaGlide(binding.ivPhoto, displayedCartStuff.stuff.photoUrl)
            }
        }

        private fun initButtons(displayedCartStuff: DisplayedCartStuff?) {
            binding.ivPlus.setOnClickListener { viewModel.incrementCartStuffCount(displayedCartStuff) }
            binding.ivMinus.setOnClickListener {
                if (displayedCartStuff?.cartStuff?.count ?: 0 > 1) {
                    viewModel.decrementCartStuffCount(displayedCartStuff)
                } else {
                    viewModel.removeCartStuff(displayedCartStuff)
                }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<DisplayedCartStuff> =
            object : DiffUtil.ItemCallback<DisplayedCartStuff>() {
                override fun areItemsTheSame(
                    item1: DisplayedCartStuff, item2: DisplayedCartStuff
                ): Boolean {
                    return item1.cartStuff.cartStuffId == item2.cartStuff.cartStuffId
                }

                override fun areContentsTheSame(
                    item1: DisplayedCartStuff, item2: DisplayedCartStuff
                ): Boolean {
                    return item1 == item2
                }
            }
    }
}
