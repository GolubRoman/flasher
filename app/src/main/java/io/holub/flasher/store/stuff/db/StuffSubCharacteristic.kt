package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
class StuffSubCharacteristic {
    @PrimaryKey
    @NonNull
    var stuffSubCharacteristicId: String = ""
    var stuffId: String = ""
    var stuffCharacteristicId = ""
    var parentSubCharacteristicId: String = ""
    var name: String = ""
    var type: SubCharacteristicType = SubCharacteristicType.STRING
    var floatValue: Float = 0.0f
    var intValue: Int = 0
    var stringValue: String = ""
    var booleanValue: Boolean = false

    constructor(
        stuffSubCharacteristicId: String, stuffId: String, stuffCharacteristicId: String,
        parentSubCharacteristicId: String, name: String, type: SubCharacteristicType
    ) {
        this.stuffSubCharacteristicId = stuffSubCharacteristicId
        this.stuffId = stuffId
        this.stuffCharacteristicId = stuffCharacteristicId
        this.parentSubCharacteristicId = parentSubCharacteristicId
        this.name = name
        this.type = type
        this.stringValue = stringValue
    }
}