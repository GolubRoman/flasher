package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
class SubCharacteristic {
    @PrimaryKey
    @NonNull
    var subCharacteristicId: String = ""
    @ColumnInfo(name = "subCharacteristicName")
    var name: String = ""
    @ColumnInfo(name = "subCharacteristicType")
    var type: SubCharacteristicType = SubCharacteristicType.STRING
    var floatStart: Float = 0.0f
    var floatEnd: Float = 0.0f
    var floatRangeStart: Float = 0.0f
    var floatRangeEnd: Float = 0.0f
    var intStart: Int = 0
    var intEnd: Int = 0
    var intRangeStart: Int = 0
    var intRangeEnd: Int = 0
    @ColumnInfo(name = "subCharacteristicStringValue")
    var stringValue: String = ""
    @ColumnInfo(name = "subCharacteristicBooleanValue")
    var booleanValue: Boolean = false
    var parentCharacteristicId: String = ""
    var dropList: List<String> = listOf()

    constructor(
        subCharacteristicId: String,
        type: SubCharacteristicType,
        name: String,
        stringValue: String,
        parentCharacteristicId: String
    ) {
        this.subCharacteristicId = subCharacteristicId
        this.type = type
        this.name = name
        this.stringValue = stringValue
        this.parentCharacteristicId = parentCharacteristicId
    }
}

enum class SubCharacteristicType {
    INT, FLOAT, STRING, BOOLEAN, DROP
}