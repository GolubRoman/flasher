package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class StuffPhoto(
    @PrimaryKey
    @NonNull
    val photoId: String,
    val photoUrl: String,
    val stuffId: String
)