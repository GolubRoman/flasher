package io.holub.flasher.store.store_container

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.CategoryWithStoreCategory
import io.holub.flasher.store.db.Store
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StoreContainerViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    init {
        DaggerStoreContainerViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeStoreCategoriesForStoreId(storeId: String): LiveData<PagedList<CategoryWithStoreCategory>> {
        return storeInteractor.observeStoreCategoriesForStoreId(0, storeId)
    }

    fun observeStoreForStoreId(storeId: String): Maybe<Store> {
        return storeInteractor.observeStoreForStoreId(storeId)
            .subscribeOn(Schedulers.io())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(storeContainerViewModel: StoreContainerViewModel)
    }
}


