package io.holub.flasher.store.top_sales_list

import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityTopSalesListBinding
import io.holub.flasher.store.stuff.StuffActivity
import io.reactivex.android.schedulers.AndroidSchedulers

class TopSalesListActivity : AppCompatActivity() {

    private var binding: ActivityTopSalesListBinding? = null
    private var viewModel: TopSalesListViewModel? = null
    private var adapter: TopSalesPagedAdapter? = null
    private var currentAnimator: AnimatorSet? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_top_sales_list)
        viewModel = ViewModelProviders.of(this).get(TopSalesListViewModel::class.java)
        initStoreList()
    }

    @SuppressLint("CheckResult")
    private fun initStoreList() {
        showProgressBarFrame(binding?.flProgressBar)
        initSwipeToRefreshView(binding?.srlSwipeToRefresh, binding?.flProgressBar)

        adapter = TopSalesPagedAdapter()
        binding?.rvTopSales?.layoutManager = GridLayoutManager(this, 2)
        binding?.rvTopSales?.adapter = adapter
        viewModel?.processTopSales()

        adapter?.processStuffClicked()
        adapter?.processStuffBuyClicked()
    }

    private fun TopSalesListViewModel.processTopSales() = observeTopSalesListLiveData()
        .observe(this@TopSalesListActivity, Observer {
            it?.let {
                adapter?.submitList(it)
                hideProgressBarFrame(binding?.flProgressBar)
            }
        })

    private fun TopSalesPagedAdapter.processStuffClicked() = observeOnStuffClicked()
        .subscribe({
            StuffActivity.start(this@TopSalesListActivity, it.stuffId, false)
        }, { ErrorHandler.logError(it) })

    private fun TopSalesPagedAdapter.processStuffBuyClicked() = observeOnBuyStuffClicked()
        .subscribe({
            viewModel?.addStuffToCart(it)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                    { currentAnimator = showOnCompletedView(binding?.ivCompleted, currentAnimator) },
                    { ErrorHandler.logError(it) })
        }, { ErrorHandler.logError(it) })

    companion object {

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, TopSalesListActivity::class.java))
        }
    }
}

