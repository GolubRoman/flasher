package io.holub.flasher.store.stuff

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Stuff
import io.holub.flasher.store.stuff.db.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StuffViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerStuffViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeSpecificStuffForStuffIdLiveData(stuffId: String): LiveData<Stuff> {
        return storeInteractor.observeSpecificStuffForStuffIdLiveData(stuffId)
    }

    fun observeStuffPhotos(stuffId: String): LiveData<List<StuffPhoto>> {
        return storeInteractor.observeStuffPhotos(stuffId)
    }

    fun addStuffToCart(stuff: Stuff): Completable {
        return orderInteractor.addStuffToCart(stuff)
    }

    fun observeStuffMovies(stuffId: String): LiveData<List<StuffMovie>> {
        return storeInteractor.observeStuffMovies(stuffId)
    }

    fun observeSubCharacteristicsForStuffId(stuffId: String): LiveData<PagedList<StuffSubCharacteristicWithSubCharacteristicWithCharacteristic>> {
        return storeInteractor.observeSubCharacteristicsForStuffId(stuffId, 0)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(stuffViewModel: StuffViewModel)
    }
}


