package io.holub.flasher.store.category.filters

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import io.holub.flasher.R
import io.holub.flasher.common.util.showSnackbar
import io.holub.flasher.databinding.*
import io.holub.flasher.store.stuff.db.SubCharacteristic
import io.holub.flasher.store.stuff.db.SubCharacteristicType
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.florescu.android.rangeseekbar.RangeSeekBar

class SubCharacteristicsPagedAdapter() :
    PagedListAdapter<SubCharacteristicWithCharacteristic, SubCharacteristicsPagedAdapter.SubCharacteristicViewHolder>(
        DIFF_CALLBACK
    ) {

    private val filtersConfigurationsEmitter = PublishSubject.create<SubCharacteristic>()

    fun observeFiltersConfigurations(): Observable<SubCharacteristic> =
        filtersConfigurationsEmitter.subscribeOn(Schedulers.io())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCharacteristicViewHolder {
        return when (viewType) {
            SUB_CHARACTERISTIC_STRING_TYPE -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicStringFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_string_filters, parent, false
                )
                SubCharactericStringViewHolder(binding)
            }
            SUB_CHARACTERISTIC_INT_TYPE -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicIntFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_int_filters, parent, false
                )
                SubCharactericIntViewHolder(binding)
            }
            SUB_CHARACTERISTIC_BOOLEAN_TYPE -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicBooleanFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_boolean_filters, parent, false
                )
                SubCharactericBooleanViewHolder(binding)
            }
            SUB_CHARACTERISTIC_FLOAT_TYPE -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicFloatFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_float_filters, parent, false
                )
                SubCharactericFloatViewHolder(binding)
            }
            SUB_CHARACTERISTIC_DROP_TYPE -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicDropFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_drop_filters, parent, false
                )
                SubCharactericDropViewHolder(binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<ItemSubCharacteristicStringFiltersBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_sub_characteristic_string_filters, parent, false
                )
                SubCharactericStringViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: SubCharacteristicViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition), holder.adapterPosition)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)?.subCharacteristic?.type) {
            SubCharacteristicType.STRING -> SUB_CHARACTERISTIC_STRING_TYPE
            SubCharacteristicType.INT -> SUB_CHARACTERISTIC_INT_TYPE
            SubCharacteristicType.BOOLEAN -> SUB_CHARACTERISTIC_BOOLEAN_TYPE
            SubCharacteristicType.FLOAT -> SUB_CHARACTERISTIC_FLOAT_TYPE
            SubCharacteristicType.DROP -> SUB_CHARACTERISTIC_DROP_TYPE
            else -> SUB_CHARACTERISTIC_STRING_TYPE
        }
    }

    abstract class SubCharacteristicViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        abstract fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int)
    }

    private fun showTitleIfPreviousIsFromAnotherCharacteristic(previous: Int, current: Int, titleView: View) {
        val previousItem = try {
            (getItem(previous) as SubCharacteristicWithCharacteristic).subCharacteristic
        } catch (e: java.lang.Exception) {
            null
        }
        val currentItem = (getItem(current) as SubCharacteristicWithCharacteristic).subCharacteristic
        if (previousItem == null || currentItem.parentCharacteristicId != previousItem.parentCharacteristicId) {
            titleView.visibility = View.VISIBLE
        } else {
            titleView.visibility = View.GONE
        }
    }

    private fun initSubCharacteristicRangeViewWithData(
        startValue: Number, endValue: Number, rangeStartValue: Number, rangeEndValue: Number,
        data: SubCharacteristicWithCharacteristic, position: Int,
        tvCharacteristicName: TextView, rsbValue: RangeSeekBar<Number>, etStartValue: EditText, etEndValue: EditText
    ) {
        showTitleIfPreviousIsFromAnotherCharacteristic(position - 1, position, tvCharacteristicName)
        tvCharacteristicName.text = data.characteristic.name
        rsbValue.setRangeValues(rangeStartValue, rangeEndValue)
        rsbValue.selectedMaxValue = endValue
        rsbValue.selectedMinValue = startValue
        etStartValue.setText(startValue.toString())
        etEndValue.setText(endValue.toString())
        rsbValue.setOnRangeSeekBarChangeListener { bar, minValue, maxValue ->
            if (startValue is Int) {
                data.subCharacteristic.intStart = minValue.toInt()
                data.subCharacteristic.intEnd = maxValue.toInt()
                etStartValue.setText(data.subCharacteristic.intStart.toString())
                etEndValue.setText(data.subCharacteristic.intEnd.toString())
            } else if (startValue is Float) {
                data.subCharacteristic.floatStart = minValue.toFloat()
                data.subCharacteristic.floatEnd = maxValue.toFloat()
                etStartValue.setText(data.subCharacteristic.floatStart.toString())
                etEndValue.setText(data.subCharacteristic.floatEnd.toString())
            }
            filtersConfigurationsEmitter.onNext(data.subCharacteristic)
        }
    }

    inner class SubCharactericIntViewHolder(val binding: ItemSubCharacteristicIntFiltersBinding) :
        SubCharacteristicViewHolder(binding) {

        override fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int) {
            if (subCharacteristicWithCharacteristic != null) {
                binding.subCharacteristic = subCharacteristicWithCharacteristic.subCharacteristic
                binding.executePendingBindings()
                initSubCharacteristicRangeViewWithData(
                    subCharacteristicWithCharacteristic.subCharacteristic.intStart,
                    subCharacteristicWithCharacteristic.subCharacteristic.intEnd,
                    subCharacteristicWithCharacteristic.subCharacteristic.intRangeStart,
                    subCharacteristicWithCharacteristic.subCharacteristic.intRangeEnd,
                    subCharacteristicWithCharacteristic,
                    position,
                    binding.tvCharacteristicName,
                    binding.rsbValue,
                    binding.etStartValue,
                    binding.etEndValue
                )
                binding.etStartValue.addTextChangedListener(object : AfterTextChangedListener {
                    override fun afterTextChanged(s: Editable?) {
                        if (binding.etStartValue.text.isNotEmpty()) {
                            try {
                                var newValue = binding.etStartValue.text.toString().toInt()
                                if (newValue > subCharacteristicWithCharacteristic.subCharacteristic.intRangeEnd) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.intRangeEnd
                                } else if (newValue < subCharacteristicWithCharacteristic.subCharacteristic.intRangeStart) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.intRangeStart
                                }
                                subCharacteristicWithCharacteristic.subCharacteristic.intStart = newValue
                                filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)

                                binding.rsbValue.selectedMinValue =
                                    subCharacteristicWithCharacteristic.subCharacteristic.intStart
                            } catch (e: Exception) {
                                showSnackbar(binding.root, R.string.convert_error)
                            }
                        }
                    }
                })
                binding.etEndValue.addTextChangedListener(object : AfterTextChangedListener {
                    override fun afterTextChanged(s: Editable?) {
                        if (binding.etStartValue.text.isNotEmpty()) {
                            try {
                                var newValue = binding.etEndValue.text.toString().toInt()
                                if (newValue > subCharacteristicWithCharacteristic.subCharacteristic.intRangeEnd) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.intRangeEnd
                                } else if (newValue < subCharacteristicWithCharacteristic.subCharacteristic.intRangeStart) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.intRangeStart
                                }
                                subCharacteristicWithCharacteristic.subCharacteristic.intEnd = newValue
                                filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                                binding.rsbValue.selectedMaxValue =
                                    subCharacteristicWithCharacteristic.subCharacteristic.intEnd
                            } catch (e: Exception) {
                                showSnackbar(binding.root, R.string.convert_error)
                            }
                        }
                    }
                })
            }
        }
    }

    inner class SubCharactericFloatViewHolder(val binding: ItemSubCharacteristicFloatFiltersBinding) :
        SubCharacteristicViewHolder(binding) {

        override fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int) {
            if (subCharacteristicWithCharacteristic != null) {
                binding.subCharacteristic = subCharacteristicWithCharacteristic.subCharacteristic
                binding.executePendingBindings()
                initSubCharacteristicRangeViewWithData(
                    subCharacteristicWithCharacteristic.subCharacteristic.floatStart,
                    subCharacteristicWithCharacteristic.subCharacteristic.floatEnd,
                    subCharacteristicWithCharacteristic.subCharacteristic.floatRangeStart,
                    subCharacteristicWithCharacteristic.subCharacteristic.floatRangeEnd,
                    subCharacteristicWithCharacteristic,
                    position,
                    binding.tvCharacteristicName,
                    binding.rsbValue,
                    binding.etStartValue,
                    binding.etEndValue
                )
                binding.etStartValue.addTextChangedListener(object : AfterTextChangedListener {
                    override fun afterTextChanged(s: Editable?) {
                        if (binding.etStartValue.text.isNotEmpty()) {
                            try {
                                var newValue = binding.etStartValue.text.toString().toFloat()
                                if (newValue > subCharacteristicWithCharacteristic.subCharacteristic.floatRangeEnd) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.floatRangeEnd
                                } else if (newValue < subCharacteristicWithCharacteristic.subCharacteristic.floatRangeStart) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.floatRangeStart
                                }
                                subCharacteristicWithCharacteristic.subCharacteristic.floatStart = newValue
                                filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                                binding.rsbValue.selectedMinValue =
                                    subCharacteristicWithCharacteristic.subCharacteristic.floatStart
                            } catch (e: Exception) {
                                showSnackbar(binding.root, R.string.convert_error)
                            }
                        }
                    }
                })
                binding.etEndValue.addTextChangedListener(object : AfterTextChangedListener {
                    override fun afterTextChanged(s: Editable?) {
                        if (binding.etStartValue.text.isNotEmpty()) {
                            try {
                                var newValue = binding.etEndValue.text.toString().toFloat()
                                if (newValue > subCharacteristicWithCharacteristic.subCharacteristic.floatRangeEnd) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.floatRangeEnd
                                } else if (newValue < subCharacteristicWithCharacteristic.subCharacteristic.floatRangeStart) {
                                    newValue = subCharacteristicWithCharacteristic.subCharacteristic.floatRangeStart
                                }
                                subCharacteristicWithCharacteristic.subCharacteristic.floatEnd = newValue
                                filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                                binding.rsbValue.selectedMaxValue =
                                    subCharacteristicWithCharacteristic.subCharacteristic.floatEnd
                            } catch (e: Exception) {
                                showSnackbar(binding.root, R.string.convert_error)
                            }
                        }
                    }
                })
            }
        }
    }

    inner class SubCharactericStringViewHolder(val binding: ItemSubCharacteristicStringFiltersBinding) :
        SubCharacteristicViewHolder(binding) {

        override fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int) {
            if (subCharacteristicWithCharacteristic != null) {
                binding.subCharacteristic = subCharacteristicWithCharacteristic.subCharacteristic
                binding.executePendingBindings()
                showTitleIfPreviousIsFromAnotherCharacteristic(position - 1, position, binding.tvCharacteristicName)
                binding.tvCharacteristicName.text = subCharacteristicWithCharacteristic.characteristic.name
                binding.etValue.setText(subCharacteristicWithCharacteristic.subCharacteristic.stringValue)
                binding.etValue.addTextChangedListener(object : AfterTextChangedListener {
                    override fun afterTextChanged(s: Editable?) {
                        subCharacteristicWithCharacteristic.subCharacteristic.stringValue =
                            binding.etValue.text.toString()
                        filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                    }
                })
            }
        }
    }

    inner class SubCharactericBooleanViewHolder(val binding: ItemSubCharacteristicBooleanFiltersBinding) :
        SubCharacteristicViewHolder(binding) {

        override fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int) {
            if (subCharacteristicWithCharacteristic != null) {
                binding.subCharacteristic = subCharacteristicWithCharacteristic.subCharacteristic
                binding.executePendingBindings()
                showTitleIfPreviousIsFromAnotherCharacteristic(position - 1, position, binding.tvCharacteristicName)
                binding.tvCharacteristicName.text = subCharacteristicWithCharacteristic.characteristic.name
                binding.sValue.isChecked = subCharacteristicWithCharacteristic.subCharacteristic.booleanValue
                binding.sValue.setOnCheckedChangeListener { buttonView, isChecked ->
                    subCharacteristicWithCharacteristic.subCharacteristic.booleanValue = isChecked
                    filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                }
            }
        }
    }

    inner class SubCharactericDropViewHolder(val binding: ItemSubCharacteristicDropFiltersBinding) :
        SubCharacteristicViewHolder(binding) {

        override fun bindTo(subCharacteristicWithCharacteristic: SubCharacteristicWithCharacteristic?, position: Int) {
            if (subCharacteristicWithCharacteristic != null) {
                binding.subCharacteristic = subCharacteristicWithCharacteristic.subCharacteristic
                binding.executePendingBindings()
                showTitleIfPreviousIsFromAnotherCharacteristic(position - 1, position, binding.tvCharacteristicName)
                binding.tvCharacteristicName.text = subCharacteristicWithCharacteristic.characteristic.name
                val list = subCharacteristicWithCharacteristic.subCharacteristic.dropList
                binding.sDropdown.setItems(list)
                var selectedIndex = list.indexOf(subCharacteristicWithCharacteristic.subCharacteristic.stringValue)
                if (selectedIndex == -1) {
                    selectedIndex = 0
                    subCharacteristicWithCharacteristic.subCharacteristic.stringValue = list[selectedIndex]
                    filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                }
                binding.sDropdown.selectedIndex = selectedIndex
                binding.sDropdown.setOnItemSelectedListener { view, position, id, item ->
                    subCharacteristicWithCharacteristic.subCharacteristic.stringValue = list[position]
                    filtersConfigurationsEmitter.onNext(subCharacteristicWithCharacteristic.subCharacteristic)
                }
            }
        }
    }

    companion object {

        val SUB_CHARACTERISTIC_STRING_TYPE = 999
        val SUB_CHARACTERISTIC_INT_TYPE = 1000
        val SUB_CHARACTERISTIC_FLOAT_TYPE = 1001
        val SUB_CHARACTERISTIC_BOOLEAN_TYPE = 1002
        val SUB_CHARACTERISTIC_DROP_TYPE = 1003

        val DIFF_CALLBACK: DiffUtil.ItemCallback<SubCharacteristicWithCharacteristic> =
            object : DiffUtil.ItemCallback<SubCharacteristicWithCharacteristic>() {
                override fun areItemsTheSame(
                    item1: SubCharacteristicWithCharacteristic, item2: SubCharacteristicWithCharacteristic
                ): Boolean {
                    return item1.subCharacteristic.subCharacteristicId == item2.subCharacteristic.subCharacteristicId
                }

                override fun areContentsTheSame(
                    item1: SubCharacteristicWithCharacteristic, item2: SubCharacteristicWithCharacteristic
                ): Boolean {
                    return true
                }
            }
    }
}
