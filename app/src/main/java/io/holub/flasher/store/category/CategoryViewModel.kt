package io.holub.flasher.store.category

import android.arch.lifecycle.ViewModel
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Category
import io.holub.flasher.store.db.Stuff
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoryViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerCategoryViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeCategoryForCategoryId(categoryId: String): Maybe<Category> {
        return storeInteractor.observeCategoryForCategoryId(categoryId)
            .subscribeOn(Schedulers.io())
    }

    fun addStuffToCart(stuff: Stuff): Completable {
        return orderInteractor.addStuffToCart(stuff)
    }

    fun observeFilteredStuffsForCategoryId(categoryId: String): Maybe<List<Stuff>> {
        return storeInteractor.filteredStuffsForCategory(categoryId)
            .subscribeOn(Schedulers.io())
    }

    fun observeOnFiltersChanged(): Observable<Boolean> {
        return storeInteractor.observeOnFiltersChanged()
            .subscribeOn(Schedulers.io())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(categoryViewModel: CategoryViewModel)
    }
}


