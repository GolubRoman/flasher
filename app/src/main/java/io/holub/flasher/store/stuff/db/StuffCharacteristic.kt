package io.holub.flasher.store.stuff.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class StuffCharacteristic(
    @PrimaryKey
    @NonNull
    var id: String,
    var stuffId: String,
    var stuffCategoryId: String,
    var parentCharacteristicId: String
)
