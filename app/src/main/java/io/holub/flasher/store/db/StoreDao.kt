package io.holub.flasher.store.db

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import io.holub.flasher.main.DiscountOffer
import io.holub.flasher.store.category.filters.SubCharacteristicWithCharacteristic
import io.holub.flasher.store.stuff.db.*
import io.reactivex.Maybe

@Dao
interface StoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStore(store: Store): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertDiscountOffer(discountOffer: DiscountOffer): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffPhoto(stuffPhoto: StuffPhoto): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffCategory(stuffCategory: StuffCategory): Long

    @Delete
    fun removeStuffCategory(stuffCategory: StuffCategory)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffMovie(stuffMovie: StuffMovie): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStoreCategory(storeCategory: StoreCategory): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCharacteristic(characteristic: Characteristic): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertSubCharacteristic(subCharacteristic: SubCharacteristic): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffCharacteristic(characteristic: StuffCharacteristic): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffSubCharacteristic(stuffSubCharacteristic: StuffSubCharacteristic): Long

    @Query("SELECT * FROM Store")
    fun observeAllStores(): Maybe<List<Store>>

    @Query("SELECT * FROM Stuff WHERE Stuff.parentStoreId = :storeId ORDER BY Stuff.discountPercentage ASC LIMIT 5")
    fun getTopDiscountedStuffsForStoreId(storeId: String): List<Stuff>

    @Delete
    fun removeStuffSubCharacteristic(stuffSubCharacteristic: StuffSubCharacteristic)

    @Query("SELECT * FROM SubCharacteristic WHERE SubCharacteristic.parentCharacteristicId = :characteristicId ORDER BY SubCharacteristic.subCharacteristicName")
    fun observeSubCharacteristics(characteristicId: String): Maybe<List<SubCharacteristic>>

    @Query("SELECT * FROM StuffSubCharacteristic INNER JOIN SubCharacteristic ON StuffSubCharacteristic.parentSubCharacteristicId = SubCharacteristic.subCharacteristicId INNER JOIN Characteristic ON SubCharacteristic.parentCharacteristicId = Characteristic.characteristicId WHERE StuffSubCharacteristic.stuffId = :stuffId ORDER BY StuffSubCharacteristic.stuffSubCharacteristicId, Characteristic.characteristicId")
    fun observeSubCharacteristicsForStuffIdLiveData(stuffId: String): DataSource.Factory<Int, StuffSubCharacteristicWithSubCharacteristicWithCharacteristic>

    @Delete
    fun removeStore(store: Store)

    @Delete
    fun removeCharacteristic(characteristic: Characteristic)

    @Delete
    fun removeStuffMovie(stuffMovie: StuffMovie)

    @Delete
    fun removeStuffPhoto(stuffPhoto: StuffMovie)

    @Query("SELECT * FROM StuffCharacteristic INNER JOIN Characteristic ON StuffCharacteristic.parentCharacteristicId = Characteristic.characteristicId WHERE StuffCharacteristic.stuffId = :stuffId")
    fun observeStuffCharacteristicsLiveData(stuffId: String): DataSource.Factory<Int, StuffCharacteristicWithCharacteristic>

    @Query("SELECT * FROM Characteristic WHERE Characteristic.categoryId = :categoryId ORDER BY Characteristic.characteristicName")
    fun observeCharacteristicsLiveData(categoryId: String): DataSource.Factory<Int, Characteristic>

    @Query("SELECT * FROM Characteristic WHERE Characteristic.categoryId = :categoryId ORDER BY Characteristic.characteristicName")
    fun observeCharacteristicsForCategoryIdMaybe(categoryId: String): Maybe<List<Characteristic>>

    @Query("SELECT * FROM SubCharacteristic WHERE SubCharacteristic.parentCharacteristicId = :characteristicId ORDER BY SubCharacteristic.subCharacteristicName")
    fun getSubCharacteristicsForCharacteristicId(characteristicId: String): List<SubCharacteristic>

    @Query("SELECT * FROM SubCharacteristic INNER JOIN Characteristic ON SubCharacteristic.parentCharacteristicId = Characteristic.characteristicId WHERE SubCharacteristic.parentCharacteristicId IN (:characteristicIds) ORDER BY SubCharacteristic.parentCharacteristicId, SubCharacteristic.subCharacteristicId")
    fun observeSubCharacteristicsForCharacteristicIds(characteristicIds: List<String>): DataSource.Factory<Int, SubCharacteristicWithCharacteristic>

    @Query("SELECT * FROM SubCharacteristic INNER JOIN Characteristic ON SubCharacteristic.parentCharacteristicId = Characteristic.characteristicId WHERE SubCharacteristic.parentCharacteristicId IN (:characteristicIds) ORDER BY SubCharacteristic.parentCharacteristicId, SubCharacteristic.subCharacteristicId")
    fun getSubCharacteristicsForCharacteristicIds(characteristicIds: List<String>): List<SubCharacteristic>

    @Query("SELECT * FROM Stuff WHERE Stuff.stuffId IN (:stuffIds)")
    fun getStuffsForStuffIds(stuffIds: List<String>): List<Stuff>

    @Query("SELECT * FROM StuffSubCharacteristic INNER JOIN SubCharacteristic ON StuffSubCharacteristic.parentSubCharacteristicId = SubCharacteristic.subCharacteristicId WHERE StuffSubCharacteristic.parentSubCharacteristicId IN (:subCharacteristicIds)")
    fun getStuffSubCharacteristicsForSubCharacteristicIds(subCharacteristicIds: List<String>): List<StuffSubCharacteristicWithSubCharacteristic>

    @Query("SELECT * FROM Stuff WHERE Stuff.storeCategoryId = :storeCategoryId")
    fun observeStuffsForStoreCategoryIdLiveData(storeCategoryId: String): DataSource.Factory<Int, Stuff>

    @Query("SELECT * FROM StoreCategory WHERE StoreCategory.parentCategoryId = :categoryId")
    fun observeStoreCategoryForCategoryId(categoryId: String): Maybe<StoreCategory>

    @Query("SELECT * FROM Category WHERE Category.categoryId = :categoryId")
    fun observeCategoryForCategoryId(categoryId: String): Maybe<Category>

    @Query("SELECT * FROM StuffPhoto WHERE stuffId = :id")
    fun observeStuffPhotos(id: String): LiveData<List<StuffPhoto>>

    @Query("SELECT * FROM StuffMovie WHERE stuffId = :id")
    fun observeStuffMovies(id: String): LiveData<List<StuffMovie>>

    @Delete
    fun removeStoreCategory(storeCategory: StoreCategory)

    @Query("SELECT * FROM StoreCategory INNER JOIN Category ON StoreCategory.parentCategoryId = Category.categoryId WHERE StoreCategory.storeId = :id")
    fun observeStoreCategoriesForStoreId(id: String): DataSource.Factory<Int, CategoryWithStoreCategory>

    @Query("SELECT * FROM Stuff WHERE Stuff.parentStoreId = :storeId AND Stuff.storeCategoryId = :storeCategoryId ORDER BY Stuff.stuffId")
    fun observeStuffsForStoreCategoryIdAndStoreId(
        storeId: String,
        storeCategoryId: String
    ): DataSource.Factory<Int, Stuff>

    @Delete
    fun removeDiscountOffer(discountOffer: DiscountOffer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCategory(category: Category): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCategories(categories: List<Category>): LongArray

    @Query("DELETE FROM Category WHERE categoryId = :categoryId")
    fun removeCategoryForId(categoryId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuff(stuff: Stuff): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertStuffs(stuffs: List<Stuff>): LongArray

    @Query("SELECT * FROM Store WHERE id = :id")
    fun observeSpecificStoreForId(id: String): Maybe<Store>

    @Query("SELECT * FROM Stuff WHERE stuffId = :id")
    fun observeSpecificStuffForStuffIdMaybe(id: String): Maybe<Stuff>

    @Query("SELECT * FROM Stuff WHERE stuffId = :id")
    fun observeSpecificStuffForStuffIdLiveData(id: String): LiveData<Stuff>

    @Query("SELECT * FROM Category WHERE categoryDeleted = 0 ORDER BY Category.categoryName")
    fun observeCategoriesLiveData(): DataSource.Factory<Int, Category>

    @Query("SELECT * FROM Category WHERE categoryDeleted = 0 AND categoryName LIKE '%' || :regex || '%' ")
    fun observeCategoriesForRegexLiveData(regex: String): DataSource.Factory<Int, Category>

    @Query("SELECT * FROM Stuff WHERE stuffDeleted = 0 ORDER BY Stuff.stuffId DESC LIMIT 5")
    fun observeTopSalesLiveData(): DataSource.Factory<Int, Stuff>

    @Query("SELECT * FROM Store WHERE deleted = 0 ORDER BY (latitude - :latitude)*(latitude - :latitude) + (longitude - :longitude)*(longitude - :longitude) ASC")
    fun getAllStoresDataSourceFactory(latitude: Double, longitude: Double): DataSource.Factory<Int, Store>

    @Query("SELECT * FROM Store WHERE deleted = 0 AND name LIKE '%' || :regex || '%' ORDER BY id ASC")
    fun observeStoresRegexLiveData(regex: String): DataSource.Factory<Int, Store>

    @Query("SELECT * FROM Store WHERE deleted = 0 ORDER BY Store.name")
    fun observeAllStoresLiveData(): DataSource.Factory<Int, Store>

    @Query("SELECT * FROM Store WHERE deleted = 0 AND name LIKE '%' || :regex || '%' ORDER BY (latitude - :latitude)*(latitude - :latitude) + (longitude - :longitude)*(longitude - :longitude) ASC")
    fun getAllStoresWithRegexDataSourceFactory(
        latitude: Double,
        longitude: Double,
        regex: String
    ): DataSource.Factory<Int, Store>

    @Query("SELECT * FROM DiscountOffer ORDER BY id")
    fun observeDiscountOffers(): LiveData<List<DiscountOffer>>

}
