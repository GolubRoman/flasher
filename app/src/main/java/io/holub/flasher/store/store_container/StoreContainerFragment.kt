package io.holub.flasher.store.store_container

import android.animation.AnimatorSet
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.FragmentStoreContainerBinding
import io.holub.flasher.main.OnPageSelectedListener
import io.holub.flasher.store.StoreCategoriesPagerAdapter
import io.holub.flasher.store.store_tab.StoreTabFragment
import io.holub.flasher.store.stuff_list.StuffsListInCategoryFragment
import io.reactivex.android.schedulers.AndroidSchedulers

class StoreContainerFragment : Fragment() {

    private var binding: FragmentStoreContainerBinding? = null
    private var viewModel: StoreContainerViewModel? = null
    private var currentAnimator: AnimatorSet? = null

    companion object {
        private val STORE_ID_KEY = "STORE_ID_KEY"

        fun newInstance(storeId: String): StoreContainerFragment {
            val storeContainerFragment = StoreContainerFragment()

            val args = Bundle()
            args.putString(STORE_ID_KEY, storeId)
            storeContainerFragment.arguments = args

            return storeContainerFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_container, container, false)
        viewModel = ViewModelProviders.of(this).get(StoreContainerViewModel::class.java)
        arguments?.let { arguments ->
            if (arguments.getString(STORE_ID_KEY) != null) {
                viewModel?.processSpecificStoreForStoreId(arguments.getString(STORE_ID_KEY))
            }
        }

        return binding?.root
    }

    private fun StoreContainerViewModel.processSpecificStoreForStoreId(storeId: String) =
        observeStoreForStoreId(storeId)
            .observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ store ->
                binding?.let { binding ->

                    binding.store = store
                    binding.executePendingBindings()
                    initOpenStatusView(
                        binding,
                        binding.tvOpenStatus,
                        store.startWorkingSession,
                        store.endWorkingSession,
                        store.blocked
                    )
                    initViewPager(storeId)
                    initImageViewViaGlide(binding.ivPhoto, store.photoUrl)

                    binding.ivInfo.setOnClickListener {
                        activity?.showStoreInfoDialog(store)
                    }
                }
            }, { ErrorHandler.logError(it) })

    private fun initViewPager(storeId: String) {
        showProgressBarFrame(binding?.flProgressBar)
        viewModel?.observeStoreCategoriesForStoreId(storeId)?.observe(this, Observer {
            it?.let {
                val fragments = it.snapshot()
                    .map { StuffsListInCategoryFragment.newInstance(it.storeCategory.storeId, it.storeCategory.id) }
                val titles = it.snapshot().map { it.category.name }
                val adapter = StoreCategoriesPagerAdapter(childFragmentManager, fragments, titles)
                binding?.vpPages?.adapter = adapter
                Handler().postDelayed({ (adapter.getItem(0) as StoreTabFragment).init() }, 300)
                binding?.stlTabs?.setViewPager(binding?.vpPages)
                binding?.vpPages?.offscreenPageLimit = fragments.size - 1
                binding?.vpPages?.addOnPageChangeListener(object : OnPageSelectedListener {
                    override fun onPageSelected(position: Int) {
                        Handler().postDelayed({ (adapter.getItem(position) as StoreTabFragment).init() }, 300)
                    }
                })
                fragments.processAddStuffToCartEmissions()
            }
            hideProgressBarFrame(binding?.flProgressBar)
        })
    }

    private fun List<StuffsListInCategoryFragment>.processAddStuffToCartEmissions() = forEach {
        it.observeOnAddStuffToCartEmitter()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                currentAnimator = showOnCompletedView(binding?.ivCompleted, currentAnimator)
            }, { ErrorHandler.logError(it) })
    }

}