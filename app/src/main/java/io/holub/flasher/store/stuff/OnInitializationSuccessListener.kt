package io.holub.flasher.store.stuff

import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer

interface OnInitializationSuccessListener: YouTubePlayer.OnInitializedListener {

    override fun onInitializationFailure(arg0: YouTubePlayer.Provider, arg1: YouTubeInitializationResult) {}
}