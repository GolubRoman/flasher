package io.holub.flasher.store.top_sales_list

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initImageViewViaGlide
import io.holub.flasher.common.util.strikeThroughText
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemTopSalesBigBinding
import io.holub.flasher.store.db.Stuff
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class TopSalesPagedAdapter : PagedListAdapter<Stuff, TopSalesPagedAdapter.StuffViewHolder>(DIFF_CALLBACK) {

    private val onStuffClickedPublishSubject = PublishSubject.create<Stuff>()
    private val onStuffBuyClickedPublishSubject = PublishSubject.create<Stuff>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StuffViewHolder {
        val binding = DataBindingUtil.inflate<ItemTopSalesBigBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_top_sales_big,
            parent,
            false
        )
        return StuffViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StuffViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    fun observeOnStuffClicked(): Observable<Stuff> {
        return onStuffClickedPublishSubject
    }

    fun observeOnBuyStuffClicked(): Observable<Stuff> {
        return onStuffBuyClickedPublishSubject
    }

    inner class StuffViewHolder(val binding: ItemTopSalesBigBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(stuff: Stuff?) {
            if (stuff != null) {
                binding.stuff = stuff
                binding.executePendingBindings()
                if (binding.tvPrice.text != binding.tvPriceWithDiscount.text) {
                    strikeThroughText(binding.tvPrice)
                    binding.tvPriceWithDiscount.visibility = View.VISIBLE
                } else {
                    binding.tvPriceWithDiscount.visibility = View.GONE
                }
                initImageViewViaGlide(binding.ivPhoto, stuff.photoUrl)
                initOnStuffClickListener()
            }
        }

        private fun initOnStuffClickListener() {
            binding.stuff?.let { stuff ->
                binding.root.setOnClickListener { onStuffClickedPublishSubject.onNext(stuff) }
                binding.btnBuy.setOnClickListener { onStuffBuyClickedPublishSubject.onNext(stuff) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Stuff> = object : DiffUtil.ItemCallback<Stuff>() {
            override fun areItemsTheSame(
                item1: Stuff, item2: Stuff
            ): Boolean {
                return item1.stuffId == item2.stuffId
            }

            override fun areContentsTheSame(
                item1: Stuff, item2: Stuff
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
