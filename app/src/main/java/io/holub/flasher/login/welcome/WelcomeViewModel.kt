package io.holub.flasher.login.welcome

import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.user.UserInteractor
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WelcomeViewModel : ViewModel() {

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerWelcomeViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun isUserAlreadyAuthorized(): Maybe<FirebaseUser> {
        return userInteractor.isUserAlreadyAuthorized()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(welcomeViewModel: WelcomeViewModel)
    }
}
