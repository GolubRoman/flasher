package io.holub.flasher.login.credentials

import android.arch.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.user.UserInteractor
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CredentialsViewModel : ViewModel() {

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerCredentialsViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeCurrentUser(): Maybe<FirebaseUser> {
        return userInteractor.observeCurrentUser()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun signInWithCredential(credential: AuthCredential): Maybe<Task<AuthResult>> {
        return userInteractor.signInWithCredential(credential)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun signOut(): Completable {
        return userInteractor.signOut()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun isUserAlreadyAuthorized(): Maybe<FirebaseUser> {
        return userInteractor.isUserAlreadyAuthorized()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(credentialsViewModel: CredentialsViewModel)
    }
}


