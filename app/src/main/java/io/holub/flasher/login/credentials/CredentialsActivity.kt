package io.holub.flasher.login.credentials

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.setStatusBarTransparent
import io.holub.flasher.common.util.showSnackbar
import io.holub.flasher.databinding.ActivityCredentialsBinding
import io.holub.flasher.tab.TabActivity

class CredentialsActivity : AppCompatActivity() {

    private var binding: ActivityCredentialsBinding? = null
    private var viewModel: CredentialsViewModel? = null
    private var googleSignInClient: GoogleSignInClient? = null
    private var callbackManager: CallbackManager? = null


    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarTransparent(window)
        navigateToTabsOrInitActivity()
    }

    @SuppressLint("CheckResult")
    private fun navigateToTabsOrInitActivity() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_credentials)
        viewModel = ViewModelProviders.of(this).get(CredentialsViewModel::class.java)
        processUserAuthorization()
    }

    private fun processUserAuthorization() = viewModel?.isUserAlreadyAuthorized()
        ?.subscribe({
            TabActivity.start(this, true)
        },
            {
                ErrorHandler.logError(it)
            },
            {
                initGoogleAuth()
                initFacebookAuth()
                hideProgressBarAndEnableButton()
            })

    public override fun onStart() {
        super.onStart()
        navigateToTabsOrInitActivity()
    }

    @SuppressLint("CheckResult")
    private fun processCurrentUser() {
        viewModel?.observeCurrentUser()
            ?.subscribe({
                hideProgressBarAndEnableButton()
                TabActivity.start(this, true)
            }, {
                ErrorHandler.logError(it)
                showAuthenticationFailed()
            })
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                showAuthenticationFailed()
            }
        }

        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun showAuthenticationFailed() {
        binding?.view?.let {
            showSnackbar(it, R.string.authentication_failed)
        }
    }

    private fun showLogoutSuccessful() {
        binding?.view?.let { showSnackbar(it, R.string.logout_successful) }
    }

    @SuppressLint("CheckResult")
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        showProgressBarAndDisableButton()
        viewModel?.signInWithCredential(GoogleAuthProvider.getCredential(acct.idToken, null))
            ?.subscribe({
                it.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        processCurrentUser()
                    } else {
                        showAuthenticationFailed()
                    }
                    hideProgressBarAndEnableButton()
                }
            }, { ErrorHandler.logError(it) })
    }

    private fun googleSignIn() {
        val signInIntent = googleSignInClient?.signInIntent
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
    }

    @SuppressLint("CheckResult")
    private fun googleSignOut() {
        viewModel?.signOut()
            ?.subscribe({}, { ErrorHandler.logError(it) })
        googleSignInClient?.signOut()?.addOnCompleteListener(this) { showLogoutSuccessful() }
    }

    private fun initFacebookAuth() {
        callbackManager = CallbackManager.Factory.create()
        binding?.btnFacebookSignIn?.setReadPermissions("email", "public_profile")
        binding?.btnFacebookSignIn?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                signInWithFacebook(loginResult.accessToken)
            }

            override fun onCancel() {
                showAuthenticationFailed()
            }

            override fun onError(error: FacebookException) {
                showAuthenticationFailed()
            }
        })
    }

    private fun initGoogleAuth() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    @SuppressLint("CheckResult")
    private fun signInWithFacebook(token: AccessToken) {
        showProgressBarAndDisableButton()

        viewModel?.signInWithCredential(FacebookAuthProvider.getCredential(token.token))
            ?.subscribe({
                it.addOnCompleteListener(this) {
                    if (it.isSuccessful) {
                        processCurrentUser()
                    } else {
                        showAuthenticationFailed()
                    }
                    hideProgressBarAndEnableButton()
                }
            }, { ErrorHandler.logError(it) })
    }


    private fun showProgressBarAndDisableButton() {
        binding?.let { binding ->
            binding.flProgressBar.visibility = View.VISIBLE
            binding.flProgressBar.setOnClickListener {}
            binding.btnGoogleSignIn.isEnabled = false
            binding.btnGoogleSignIn.setOnClickListener {}
            binding.btnFacebookSignIn.isEnabled = false
        }
    }

    private fun hideProgressBarAndEnableButton() {
        binding?.let { binding ->
            binding.flProgressBar.visibility = View.INVISIBLE
            binding.btnGoogleSignIn.isEnabled = true
            binding.btnGoogleSignIn.setOnClickListener { googleSignIn() }
            binding.btnFacebookSignIn.isEnabled = true
        }
    }

    companion object {

        private const val RC_GOOGLE_SIGN_IN = 9001

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, CredentialsActivity::class.java))
        }
    }
}