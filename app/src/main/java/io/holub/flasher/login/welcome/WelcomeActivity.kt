package io.holub.flasher.login.welcome

import agency.tango.materialintroscreen.MaterialIntroActivity
import agency.tango.materialintroscreen.SlideFragmentBuilder
import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.login.credentials.CredentialsActivity
import io.holub.flasher.tab.TabActivity

class WelcomeActivity : MaterialIntroActivity() {

    private var viewModel: WelcomeViewModel? = null
    private var batteryDialog: Dialog? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(WelcomeViewModel::class.java)
        viewModel?.isUserAlreadyAuthorized()
            ?.subscribe({
                TabActivity.start(this, true)
            },
                { ErrorHandler.logError(it) },
                {
                    welcomeSlides()
                    prepareBatteryOptimizationDialog(this)
                    showBatteryDialog()
                })
    }

    private fun showBatteryDialog() {
        if (isBatteryOptimized() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            batteryDialog?.show()
        }
    }

    private fun isBatteryOptimized(): Boolean {
        val pwrm = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
        val name = applicationContext.packageName
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return !pwrm.isIgnoringBatteryOptimizations(name)
        }
        return false
    }

    private fun welcomeSlides() {
        addSlide(SlideFragmentBuilder()
            .backgroundColor(R.color.best_stuff_offers_background)
            .buttonsColor(R.color.next_button_background)
            .image(R.drawable.ic_tennis_racket)
            .title(resources.getString(R.string.activity_welcome_best_stuff_offers_title))
            .description(resources.getString(R.string.activity_welcome_best_stuff_offers_description))
            .build())

        addSlide(SlideFragmentBuilder()
            .backgroundColor(R.color.location_based_offers_background)
            .buttonsColor(R.color.next_button_background)
            .image(R.drawable.ic_location_based)
            .title(resources.getString(R.string.activity_welcome_location_based_offers_title))
            .description(resources.getString(R.string.activity_welcome_location_based_offers_description))
            .build())

        addSlide(SlideFragmentBuilder()
            .backgroundColor(R.color.shop_aggregation_background)
            .buttonsColor(R.color.next_button_background)
            .image(R.drawable.ic_shop_aggregation)
            .title(resources.getString(R.string.activity_welcome_shop_aggregation_title))
            .description(resources.getString(R.string.activity_welcome_shop_aggregation_description))
            .build())

        addSlide(SlideFragmentBuilder()
            .backgroundColor(R.color.filters_system_background)
            .buttonsColor(R.color.next_button_background)
            .image(R.drawable.ic_filters)
            .title(resources.getString(R.string.activity_welcome_filters_system_title))
            .description(resources.getString(R.string.activity_welcome_filters_system_description))
            .build())

    }

    override fun onFinish() {
        super.onFinish()
        CredentialsActivity.start(this, true)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (batteryDialog?.isShowing == true) {
            batteryDialog?.dismiss()
        }
    }

    private fun prepareBatteryOptimizationDialog(context: Context) {
        val dialogBuilder = MaterialDialog.Builder(context)

        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_battery_optimization, null, false)
        dialogBuilder.customView(dialogView, false)

        val btnClose = (dialogView.findViewById(R.id.btn_close) as TextView)
        val btnDisableOptimization = (dialogView.findViewById(R.id.btn_battery_optimization) as TextView)

        batteryDialog = dialogBuilder.build()
        batteryDialog?.setCancelable(false)

        btnClose.setOnClickListener {
            batteryDialog?.dismiss()
        }
        btnDisableOptimization.setOnClickListener {
            val intent = Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
            startActivity(intent)
            batteryDialog?.dismiss()
        }

    }

    companion object {

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, WelcomeActivity::class.java))
        }
    }
}