package io.holub.flasher.common.io.location

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import androidx.annotation.RequiresApi
import io.holub.flasher.R
import io.holub.flasher.store.StoreActivity
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.db.Stuff
import java.lang.System.currentTimeMillis

class NotificationManager(val context: Context) : ContextWrapper(context) {

    private var manager: NotificationManager? = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val GROUP_KEY_STORE_NOTIFICATIONS = "GROUP_KEY_STORE_NOTIFICATIONS"

    fun showNotification(store: Store, topDiscountedStuffsInStore: List<Stuff>) {
        manager?.notify(
            store.id.hashCode(),
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                buildNotification(store, topDiscountedStuffsInStore)
            } else {
                buildNotificationLowerOreo(store, topDiscountedStuffsInStore)
            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun buildNotification(store: Store, topDiscountedStuffsInStore: List<Stuff>): Notification {
        val notificationChannelId = "STORE_NOTIFICATION_CHANNEL_ID"
        val notificationChannelName = "FLASHER"
        val notificationBuilder = NotificationCompat.Builder(context, notificationChannelId)
            .setContentTitle(store.name)
            .setContentText(buildTextForNotification(store, topDiscountedStuffsInStore))
            .setSmallIcon(R.drawable.ic_cart)
            .setPriority(Notification.PRIORITY_MAX)
            .setDefaults(Notification.DEFAULT_ALL)
            .setContentIntent(buildClickIntentOnNotification(store))
            .setAutoCancel(false)
            .setGroup(GROUP_KEY_STORE_NOTIFICATIONS)
            .setStyle(NotificationCompat.BigTextStyle())
            .setChannelId(notificationChannelId)
        manager?.createNotificationChannel(
            NotificationChannel(
                notificationChannelId,
                notificationChannelName,
                NotificationManager.IMPORTANCE_HIGH
            )
        )
        return notificationBuilder.build()
    }

    private fun buildNotificationLowerOreo(store: Store, topDiscountedStuffsInStore: List<Stuff>): Notification {
        val notificationBuilder = Notification.Builder(context)
            .setContentTitle(store.name)
            .setContentText(buildTextForNotification(store, topDiscountedStuffsInStore))
            .setSmallIcon(R.drawable.ic_cart)
            .setContentIntent(buildClickIntentOnNotification(store))
            .setPriority(Notification.PRIORITY_MAX)
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(false)

        return Notification.BigTextStyle(notificationBuilder)
            .bigText(buildTextForNotification(store, topDiscountedStuffsInStore))
            .build()
    }

    private fun buildClickIntentOnNotification(store: Store): PendingIntent {
        val notificationIntent = Intent(context, StoreActivity::class.java)
            .putExtra(StoreActivity.STORE_ID, store.id)
            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
            .setAction(currentTimeMillis().toString())

        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        return PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun buildTextForNotification(store: Store, topDiscountedStuffsInStore: List<Stuff>): String {
        val SPACE = " "
        val DOT = "."
        return getString(R.string.you_are_close_to) + SPACE + store.name + SPACE + getString(R.string.on) + SPACE + store.address + DOT +
            getString(R.string.welcome_there_are) + SPACE + topDiscountedStuffsInStore.size.toString() + SPACE + getString(
            R.string.hottest_offers
        ) + DOT +
            getString(R.string.waiting_for_you) + SPACE + store.name + DOT
    }
}