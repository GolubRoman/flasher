package io.holub.flasher.common.util

import android.app.Activity

import com.tbruyelle.rxpermissions2.Permission
import com.tbruyelle.rxpermissions2.RxPermissions

import io.reactivex.Observable

object PermissionUtils {

    fun requestPermissions(activity: Activity, vararg permissions: String): Observable<Boolean> {
        return RxPermissions(activity).request(*permissions)
    }

    fun requestEachPermissions(activity: Activity, permissions: Array<String>): Observable<Permission> {
        return RxPermissions(activity).requestEach(*permissions)
    }

    fun <T> ensurePermissions(activity: Activity,
                              beforeGoingObservable: Observable<T>, vararg permissions: String): Observable<Boolean> {
        return beforeGoingObservable.compose(RxPermissions(activity).ensure(*permissions))
    }

    fun <T> ensureEachPermissions(activity: Activity,
                                  beforeGoingObservable: Observable<T>, vararg permissions: String): Observable<Permission> {
        return beforeGoingObservable.compose(RxPermissions(activity).ensureEach(*permissions))
    }

}
