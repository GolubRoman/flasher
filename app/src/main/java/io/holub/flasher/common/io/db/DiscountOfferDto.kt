package io.holub.flasher.common.io.db

import io.holub.flasher.main.DiscountOffer

data class DiscountOfferDto(
    val id: String,
    val photoUrl: String,
    val stuffId: String
) {
    constructor() : this("", "", "")

    fun toDiscountOffer(): DiscountOffer {
        return DiscountOffer(id, photoUrl, stuffId)
    }
}