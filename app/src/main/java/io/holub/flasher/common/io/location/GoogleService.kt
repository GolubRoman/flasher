package io.holub.flasher.common.io.location

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.location.LocationManager.NETWORK_PROVIDER
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import java.util.*

class GoogleService : Service(), LocationListener {

    private var gpsEnabled = false
    private var networkEnabled = false
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var locationManager: LocationManager? = null
    private var location: Location? = null
    private val handler = Handler()
    private var timer: Timer? = null
    private var notifyInterval: Long = 1000 * 20
    private var intent: Intent? = null

    override fun onCreate() {
        super.onCreate()
        timer = Timer()
        timer?.schedule(TimerTaskToGetLocation(), 5, notifyInterval)
        intent = Intent(LOCATION_BROADCAST_RECEIVER_ACTION)
        getLocation()
    }

    private fun update(location: Location?) {
        intent?.putExtra(LATITUDE, location?.latitude?.toString() ?: EMPTY + "")
        intent?.putExtra(LONGITUDE, location?.longitude?.toString() ?: EMPTY + "")
        sendBroadcast(intent)
    }

    private fun getLocation() {
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsEnabled = locationManager?.isProviderEnabled(GPS_PROVIDER) ?: false
        networkEnabled = locationManager?.isProviderEnabled(NETWORK_PROVIDER) ?: false
        if (gpsEnabled || networkEnabled) {
            processCoordinateWithLocationManager(if (networkEnabled) { NETWORK_PROVIDER } else { GPS_PROVIDER })
        }
    }

    private fun processCoordinateWithLocationManager(networkProvider: String) {
        try {
            location = null
            locationManager?.requestLocationUpdates(networkProvider, 1000, 0f, this)
            if (locationManager != null) {
                location = locationManager?.getLastKnownLocation(networkProvider)
                if (location != null) {
                    logCoordinates(location)
                    latitude = location?.latitude ?: -1.0
                    longitude = location?.longitude ?: -1.0
                    update(location)
                }
            }
        } catch (e: SecurityException) {}
    }

    private fun logCoordinates(location: Location?) {
        Log.e(LATITUDE, location?.latitude?.toString() ?: EMPTY + "")
        Log.e(LONGITUDE, location?.longitude?.toString() ?: EMPTY + "")
    }

    private inner class TimerTaskToGetLocation : TimerTask() {
        override fun run() {
            handler.post { getLocation() }
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onLocationChanged(location: Location) {}
    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String?) {}

    companion object {
        val LOCATION_BROADCAST_RECEIVER_ACTION = "location.broadcast.receiver.action"
        val LATITUDE = "LATITUDE"
        val LONGITUDE = "LONGITUDE"
        val EMPTY = "EMPTY"
    }

}