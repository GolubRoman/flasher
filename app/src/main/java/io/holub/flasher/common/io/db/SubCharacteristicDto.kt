package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.SubCharacteristic
import io.holub.flasher.store.stuff.db.SubCharacteristicType


data class SubCharacteristicDto(
    var subCharacteristicId: String,
    var name: String,
    var type: String,
    var floatStart: Float,
    var floatEnd: Float,
    var floatRangeStart: Float,
    var floatRangeEnd: Float,
    var intStart: Int,
    var intEnd: Int,
    var intRangeStart: Int,
    var intRangeEnd: Int,
    var stringValue: String,
    var booleanValue: Boolean,
    var parentCharacteristicId: String,
    var dropList: List<StringValueDto>
) {

    constructor() : this("", "", "", 0.0f, 0.0f,
        0.0f, 0.0f, 0, 0, 0, 0, "", false, "", listOf())

    fun toSubCharacteristic(): SubCharacteristic {
        val subCharacteristic = SubCharacteristic(subCharacteristicId, SubCharacteristicType.valueOf(type), name, stringValue, parentCharacteristicId)
        subCharacteristic.floatStart = floatStart
        subCharacteristic.floatEnd = floatEnd
        subCharacteristic.floatRangeStart = floatRangeStart
        subCharacteristic.floatRangeEnd = floatRangeEnd
        subCharacteristic.intStart = intStart
        subCharacteristic.intEnd = intEnd
        subCharacteristic.intRangeStart = intRangeStart
        subCharacteristic.intRangeEnd = intRangeEnd
        subCharacteristic.booleanValue = booleanValue
        subCharacteristic.dropList = dropList.map { it.valueToString() }
        return subCharacteristic
    }
}