package io.holub.flasher.common.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

class NetworkStateMonitor(context: Context) : ConnectivityManager.NetworkCallback() {

    private val networkRequest: NetworkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()
    private val networkStateObservable = PublishSubject.create<Boolean>().toSerialized()
    private val connectivityManager: ConnectivityManager = context
        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val currentNetworkState: Boolean
        get() =
            connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnectedOrConnecting

    init {
        enable()
    }

    private fun enable() {
        connectivityManager.registerNetworkCallback(networkRequest, this)
    }

    fun observeNetworkState(): Flowable<Boolean> {
        return networkStateObservable.startWith(currentNetworkState).toFlowable(BackpressureStrategy.DROP)
    }

    fun emitCurrentState() {
        networkStateObservable.onNext(currentNetworkState)
    }

    override fun onAvailable(network: Network) {
        networkStateObservable.onNext(true)
    }

    override fun onLost(network: Network) {
        networkStateObservable.onNext(false)
    }
}