package io.holub.flasher.common.io.db

import io.holub.flasher.store.db.StoreCategory

data class StoreCategoryDto(
    var id: String,
    var storeId: String,
    var parentCategoryId: String
) {

    constructor() : this("", "", "")

    fun toStoreCategory(): StoreCategory {
        return StoreCategory(id, storeId, parentCategoryId)
    }

}