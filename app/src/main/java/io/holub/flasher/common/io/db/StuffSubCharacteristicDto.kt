package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.StuffSubCharacteristic
import io.holub.flasher.store.stuff.db.SubCharacteristicType

data class StuffSubCharacteristicDto (
    var stuffSubCharacteristicId: String,
    var stuffId: String,
    var stuffCharacteristicId: String,
    var parentSubCharacteristicId: String,
    var name: String,
    var type: String,
    var floatValue: Float,
    var intValue: Int,
    var stringValue: String,
    var booleanValue: Boolean
) {
    constructor() : this("", "", "",
        "", "", "", 0.0f, 0, "", false)

    fun toStuffSubCharacteristic(): StuffSubCharacteristic {
        val stuffSubCharacteristic = StuffSubCharacteristic(stuffSubCharacteristicId, stuffId, stuffCharacteristicId, parentSubCharacteristicId, name,
        SubCharacteristicType.valueOf(type))
        stuffSubCharacteristic.floatValue = floatValue
        stuffSubCharacteristic.intValue = intValue
        stuffSubCharacteristic.stringValue = stringValue
        stuffSubCharacteristic.booleanValue = booleanValue

        return stuffSubCharacteristic
    }
}