package io.holub.flasher.common.view

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class FlasherAppGlideModule : AppGlideModule()
