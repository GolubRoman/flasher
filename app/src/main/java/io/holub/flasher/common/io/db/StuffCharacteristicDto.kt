package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.StuffCharacteristic

data class StuffCharacteristicDto(
    var id: String,
    var stuffId: String,
    var stuffCategoryId: String,
    var parentCharacteristicId: String,
    var stuffSubCharacteristics: List<StuffSubCharacteristicDto>
)  {

    constructor() : this("", "", "",  "", listOf())

    fun toStuffCharacteristic(): StuffCharacteristic {
        return StuffCharacteristic(id, stuffId, stuffCategoryId, parentCharacteristicId)
    }
}