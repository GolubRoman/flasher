package io.holub.flasher.common.error

import android.content.Context
import android.util.Log
import com.crashlytics.android.Crashlytics

import io.holub.flasher.common.App
import io.fabric.sdk.android.Fabric

class ErrorHandler(context: Context) {
    private val shareCrashlyticsReports: Boolean

    init {
        initCrashlytics(context)
        this.shareCrashlyticsReports = true
    }

    fun logErrorViaInstance(throwable: Throwable) {
        Log.e(ERROR_HANDLER_LOG_TAG, throwable.message, throwable)
        if (shareCrashlyticsReports) {
            Crashlytics.logException(throwable)
        }
    }

    fun logErrorViaInstance(message: String) {
        Log.e(ERROR_HANDLER_LOG_TAG, message)
        if (shareCrashlyticsReports) {
            Crashlytics.log(message)
        }
    }

    fun logCrashlyticsViaInstance(log: String) {
        Log.v(ERROR_HANDLER_LOG_TAG, log)
        if (shareCrashlyticsReports) {
            Crashlytics.log(log)
        }
    }

    private fun initCrashlytics(context: Context) {
        if (shareCrashlyticsReports) {
            Fabric.with(context, Crashlytics())
        }
    }

    companion object {

        private val ERROR_HANDLER_LOG_TAG = "ERROR_HANDLER"

        fun logError(throwable: Throwable) =
            try {
                App.instance.appComponent().errorHandler().logErrorViaInstance(throwable)
            } catch (e: Throwable) {
                Log.e(ERROR_HANDLER_LOG_TAG, "Error during logging error: " + e.message, e)
            }

        fun logError(message: String) =
            try {
                App.instance.appComponent().errorHandler().logErrorViaInstance(message)
            } catch (e: Throwable) {
                Log.e(ERROR_HANDLER_LOG_TAG, "Error during logging error: " + e.message, e)
            }

        fun logCrashlytics(log: String) =
            App.instance.appComponent().errorHandler().logCrashlyticsViaInstance(log)
    }
}
