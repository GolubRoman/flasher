package io.holub.flasher.common.util

import android.arch.paging.PagedList
import kotlin.math.min

private val DEFAULT_PAGE_SIZE = 20
private val UPPER_PAGE_SIZE_BOUND = 100

fun buildDbConfig(pageSize: Int): PagedList.Config {
    val finalPageSize = min(if (pageSize < 1) DEFAULT_PAGE_SIZE else pageSize, UPPER_PAGE_SIZE_BOUND)
    return PagedList.Config.Builder()
        .setPageSize(finalPageSize)
        .setEnablePlaceholders(true)
        .setInitialLoadSizeHint(finalPageSize * 2)
        .setPrefetchDistance(finalPageSize / 2)
        .build()
}