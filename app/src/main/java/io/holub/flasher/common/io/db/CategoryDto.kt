package io.holub.flasher.common.io.db

import io.holub.flasher.store.db.Category

data class CategoryDto (
    var id: String,
    var name: String,
    var photoUrl: String,
    var deleted: Boolean,
    var characteristics: List<CharacteristicDto>) {

    constructor() : this("", "", "",  false, listOf())

    fun toCategory(): Category {
        return Category(id, name, photoUrl, deleted)
    }
}