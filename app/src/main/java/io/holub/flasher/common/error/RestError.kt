package io.holub.flasher.common.error

import io.holub.flasher.common.util.UNKNOWN_ERROR_CODE
import io.holub.flasher.common.util.UNKNOWN_ERROR_MESSAGE

data class RestError(
    var readableCode: String = UNKNOWN_ERROR_CODE,
    var debugMessage: String = UNKNOWN_ERROR_MESSAGE)