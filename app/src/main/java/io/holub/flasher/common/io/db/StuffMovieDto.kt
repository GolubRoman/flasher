package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.StuffMovie

data class StuffMovieDto(
    val movieId: String,
    val youtubeMovieId: String,
    val youtubeMoviePhotoUrl: String,
    val stuffId: String
) {
    constructor() : this("", "", "",  "")

    fun toStuffMovie(): StuffMovie {
        return StuffMovie(movieId, youtubeMovieId, youtubeMoviePhotoUrl, stuffId)
    }
}