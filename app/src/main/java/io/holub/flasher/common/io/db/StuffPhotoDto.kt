package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.StuffPhoto

data class StuffPhotoDto(
    val photoId: String,
    val photoUrl: String,
    val stuffId: String
) {
    constructor() : this("", "", "")

    fun toStuffPhoto(): StuffPhoto {
        return StuffPhoto(photoId, photoUrl, stuffId)
    }
}