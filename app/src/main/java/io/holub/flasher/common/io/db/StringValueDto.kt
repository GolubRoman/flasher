package io.holub.flasher.common.io.db

data class StringValueDto(
    var value: String
) {

    constructor() : this("")

    fun valueToString(): String {
        return value
    }
}