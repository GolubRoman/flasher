package io.holub.flasher.common.io.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.TypeConverters
import io.holub.flasher.order.db.Order
import io.holub.flasher.order.db.OrderDao
import io.holub.flasher.order.db.OrderStuff
import io.holub.flasher.BuildConfig
import io.holub.flasher.main.DiscountOffer
import io.holub.flasher.profile.Card
import io.holub.flasher.profile.PaymentDao
import io.holub.flasher.store.cart.CartStuff
import io.holub.flasher.store.db.*
import io.holub.flasher.store.stuff.db.*

@Database(
    entities = [Store::class, Stuff::class, Category::class, StoreCategory::class, CartStuff::class,
        DiscountOffer::class, OrderStuff::class, Order::class, Card::class, StuffMovie::class, StuffPhoto::class,
        SubCharacteristic::class, Characteristic::class, StuffCategory::class,
        StuffCharacteristic::class, StuffSubCharacteristic::class],
    version = BuildConfig.VERSION_CODE, exportSchema = false
)
@TypeConverters(SubCharacteristicConverters::class)
abstract class RoomDatabase : android.arch.persistence.room.RoomDatabase() {

    abstract fun getStoreDao(): StoreDao

    abstract fun getOrderDao(): OrderDao

    abstract fun getPaymentDao(): PaymentDao
}
