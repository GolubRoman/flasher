package io.holub.flasher.common.di

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.Component
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.io.db.RoomDatabase
import io.holub.flasher.common.io.location.NotificationManager
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.order.db.OrderDao
import io.holub.flasher.profile.PaymentDao
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.StoreDao
import io.holub.flasher.user.UserInteractor
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun userInteractor(): UserInteractor

    fun storeInteractor(): StoreInteractor

    fun firebaseDatabase(): FirebaseDatabase

    fun orderInteractor(): OrderInteractor

    fun orderDao(): OrderDao

    fun storeDao(): StoreDao

    fun paymentDao(): PaymentDao

    fun firebaseAuth(): FirebaseAuth

    fun roomDatabase(): RoomDatabase

    fun prefs(): Prefs

    fun errorHandler(): ErrorHandler

    fun context(): Context

    fun notificationManager(): NotificationManager

}
