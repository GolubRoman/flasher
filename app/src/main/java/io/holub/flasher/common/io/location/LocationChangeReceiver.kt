package io.holub.flasher.common.io.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.holub.flasher.common.App
import io.holub.flasher.common.io.location.GoogleService.Companion.EMPTY
import io.holub.flasher.common.io.location.GoogleService.Companion.LATITUDE
import io.holub.flasher.common.io.location.GoogleService.Companion.LONGITUDE

class LocationChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.getStringExtra(LATITUDE) ?: EMPTY != EMPTY
            && intent.getStringExtra(LONGITUDE) ?: EMPTY != EMPTY
        ) {
            App.instance
                .appComponent()
                .storeInteractor()
                .userIsThere(intent.getStringExtra(LATITUDE).toDouble(), intent.getStringExtra(LONGITUDE).toDouble())
        }
    }

}