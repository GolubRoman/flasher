package io.holub.flasher.common.util

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.databinding.ViewDataBinding
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.GradientDrawable
import android.location.Location
import android.os.Build
import android.os.Handler
import android.support.design.widget.AppBarLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import br.com.sapereaude.maskedEditText.MaskedEditText
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.wajahatkarim3.easyflipviewpager.CardFlipPageTransformer
import io.holub.flasher.R
import io.holub.flasher.common.App
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.main.AnimatorStartEndListener
import io.holub.flasher.profile.Card
import io.holub.flasher.store.db.Store
import io.reactivex.Maybe
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.DecimalFormat

fun showToast(context: Context?, messageResId: Int) {
    if (context != null) {
        Toast.makeText(
            context, context.resources.getString(messageResId),
            Toast.LENGTH_SHORT
        ).show()
    }
}

fun showToast(context: Context?, throwable: Throwable) {
    App.instance.appComponent().errorHandler().logErrorViaInstance(throwable)
    showToast(context, throwable.message)
}

fun showToast(context: Context?, message: String?) {
    if (context != null) {
        val displayMessage = message ?: context.getString(R.string.unknown_error)
        Toast.makeText(context, displayMessage, Toast.LENGTH_SHORT).show()
    }
}

fun strikeThroughText(price: TextView?) {
    price?.let {
        price.paintFlags = price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }
}

fun removeStrikeThroughText(price: TextView?) {
    price?.let {
        price.paintFlags = price.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
}

fun showAlertDialog(context: Context?, message: String, title: String) {
    if (context != null) {
        val dialogBuilder = MaterialDialog.Builder(context)

        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_alert, null, false)
        dialogBuilder.customView(dialogView, false)

        val tvTitle = (dialogView.findViewById(R.id.tv_title) as TextView)
        val tvMessage = (dialogView.findViewById(R.id.tv_message) as TextView)
        val btnClose = (dialogView.findViewById(R.id.btn_close) as TextView)

        val dialogAlert = dialogBuilder.build()
        dialogAlert.setCancelable(true)
        tvTitle.text = title
        tvMessage.text = message
        dialogAlert.show()

        btnClose.setOnClickListener { dialogAlert.dismiss() }
    }

}

fun Activity.showStoreInfoDialog(store: Store) {
        val dialogBuilder = MaterialDialog.Builder(this)

        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_store_info, null, false)
        dialogBuilder.customView(dialogView, false)

        val ivPhoto = (dialogView.findViewById(R.id.iv_photo) as ImageView)
        val tvDescription = (dialogView.findViewById(R.id.tv_description) as TextView)
        val btnOk = (dialogView.findViewById(R.id.btn_ok) as TextView)

        val dialogStoreInfo = dialogBuilder.build()
        dialogStoreInfo.setCancelable(true)
        tvDescription.text = store.description
        initImageViewViaGlide(ivPhoto, store.photoUrl)
        btnOk.setOnClickListener { dialogStoreInfo.dismiss() }

        dialogStoreInfo.show()
}

fun convertDpToPixel(dp: Float): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = dp * (metrics.densityDpi / 160f)
    return Math.round(px)
}

fun initTransparentStatusBar(window: Window) {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )
}

fun collapseSpecificViewsIfDataIsEmpty(data: String?, flFrame: ViewGroup, tvText: TextView, appBarLayout: AppBarLayout) {
    if (data == null || data.isEmpty()) {
        flFrame.visibility = View.GONE
        tvText.visibility = View.GONE
        Handler().postDelayed({
            appBarLayout.layoutParams = appBarLayout.layoutParams.let {
                it.height -= convertDpToPixel(100f)
                it
            }
            appBarLayout.invalidate()
        }, 200)
    }
}

@SuppressLint("ClickableViewAccessibility")
fun showProgressBarFrame(flProgressBar: ViewGroup?) {
    flProgressBar?.visibility = View.VISIBLE
    flProgressBar?.setOnTouchListener { _, _ -> true }
}

fun hideProgressBarFrame(flProgressBar: ViewGroup?) {
    flProgressBar?.visibility = View.INVISIBLE
}

fun setCardFlipViewPagerTransformer(viewPager: ViewPager?) {
    val cardFlipPageTransformer = CardFlipPageTransformer()
    cardFlipPageTransformer.isScalable = false
    cardFlipPageTransformer.flipOrientation = CardFlipPageTransformer.VERTICAL
    viewPager?.setPageTransformer(true, cardFlipPageTransformer)
}

fun initImageViewViaGlide(imageView: ImageView, photoUrl: String) {
    GlideApp.with(imageView.context)
        .load(photoUrl)
        .into(imageView)
}

fun initCenterCropImageViewViaGlide(imageView: ImageView, photoUrl: String) {
    GlideApp.with(imageView.context)
        .load(photoUrl)
        .centerCrop()
        .into(imageView)
}
fun initWhiteStatusBar(window: Window, context: Context) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(context, R.color.activity_credentials_status_bar_color)
    }
}

fun setEditTextFrameSelectable(editText: EditText?, flEditTextFrame: ViewGroup?) {
    val resources = editText?.resources
    editText?.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
        val drawable = flEditTextFrame?.background as GradientDrawable
        val enemyDrawable = flEditTextFrame?.background as GradientDrawable
        if (hasFocus) {
            drawable.setStroke(4, resources?.getColor(R.color.colorPrimary)?: 0)
            enemyDrawable.setStroke(
                1,
                resources?.getColor(R.color.activity_credentials_phone_container_unfocused_border)?: 0
            )
        } else {
            enemyDrawable.setStroke(4, resources?.getColor(R.color.colorPrimary)?: 0)
            drawable.setStroke(1, resources?.getColor(R.color.activity_credentials_phone_container_unfocused_border)?: 0)
        }
    }
}

fun initCenterCropImageViewViaGlide(imageView: ImageView, photoRes: Int) {
    GlideApp.with(imageView.context)
        .load(imageView.context.resources.getDrawable(photoRes))
        .centerCrop()
        .into(imageView)
}

fun initSwipeToRefreshView(srlSwipeToRefreshView: SwipeRefreshLayout?, flProgressBar: FrameLayout?) {
    srlSwipeToRefreshView?.setColorSchemeResources(R.color.colorPrimary,
        R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary)
    srlSwipeToRefreshView?.setOnRefreshListener {
        showProgressBarFrame(flProgressBar)
        Handler().postDelayed({
            hideProgressBarFrame(flProgressBar)
            srlSwipeToRefreshView?.isRefreshing = false
        }, 1000)
    }
}

fun showEmptyListView(shouldBeHiddenView: View?, flEmptyList: ViewGroup?) {
    shouldBeHiddenView?.visibility = View.INVISIBLE
    flEmptyList?.visibility = View.VISIBLE
}

fun hideEmptyListView(shouldBeShownView: View?, flEmptyList: ViewGroup?) {
    flEmptyList?.visibility = View.INVISIBLE
    shouldBeShownView?.visibility = View.VISIBLE
}

fun showSnackbar(parentView: View, messageResource: Int) = Snackbar.make(
    parentView, parentView.context.resources.getString(messageResource),
    Snackbar.LENGTH_SHORT
).show()

fun showSnackbar(parentView: View, message: String) = Snackbar.make(
    parentView, message, Snackbar.LENGTH_SHORT
).show()

fun setStatusBarTransparent(window: Window) = window.setFlags(
    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
)

fun formatDoubleSumPrice(price: Double) = DecimalFormat("##.##").format(price.toInt())

fun showOnCompletedView(ivCompleted: View?, currentAnimator: AnimatorSet?): AnimatorSet {
    val view = ivCompleted

    currentAnimator?.cancel()

    val scaleX = ObjectAnimator.ofFloat(view, "scaleX", 0f, 1f)
    val scaleY = ObjectAnimator.ofFloat(view, "scaleY", 0f, 1f)
    val alpha = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f)
    scaleX.duration = 600
    scaleY.duration = 600
    alpha.duration = 1000

    val scale = AnimatorSet()
    scale.play(scaleX).with(scaleY).with(alpha)
    scale.addListener(object : AnimatorStartEndListener {
        override fun onAnimationStart(animation: Animator?) {
            view?.visibility = View.VISIBLE
        }

        override fun onAnimationEnd(animation: Animator?) {
            view?.visibility = View.INVISIBLE
            view?.alpha = 1f
            view?.invalidate()
        }
    })

    scale.start()
    return scale
}

fun showAlertDialog(context: Context?, message: Int, title: Int) {
    if (context != null) {
        val titleText = context.resources.getString(title)
        val messageText = context.resources.getString(message)
        showAlertDialog(context, messageText, titleText)
    }
}

fun getKmDistance(distance: Int): String {
    return DecimalFormat("##.#").format(distance * 0.001)
}

fun getKmDistanceDouble(distance: Int): Double {
    return DecimalFormat("##.#").format(distance * 0.001).replace(",", ".").toDouble()
}

fun distance(latitude: Double, longitude: Double, storeLatitude: Double, storeLongitude: Double): Double {
    val loc1 = Location("")
    loc1.latitude = latitude
    loc1.longitude = longitude

    val loc2 = Location("")
    loc2.latitude = storeLatitude
    loc2.longitude = storeLongitude

    return getKmDistanceDouble(loc1.distanceTo(loc2).toInt())
}

fun initOpenStatusView(optionalBinding: ViewDataBinding?, tvOpenStatus: TextView, startWorkingSession: Long, endWorkingSession: Long, blocked: Boolean) {
    optionalBinding?.let { binding ->
        val resources = binding.root.context.resources
        if ((System.currentTimeMillis() < startWorkingSession + DateTime(System.currentTimeMillis()).withTimeAtStartOfDay().toDate().time
                || System.currentTimeMillis() > endWorkingSession + DateTime(System.currentTimeMillis()).withTimeAtStartOfDay().toDate().time)
            || blocked
        ) {
            tvOpenStatus.setTextColor(Color.RED)
            tvOpenStatus.text =
                resources.getString(
                    R.string.working_hours,
                    formatHhMmTime(startWorkingSession),
                    formatHhMmTime(endWorkingSession),
                    resources.getString(R.string.closed)
                )
        } else {
            tvOpenStatus.setTextColor(resources.getColor(R.color.well_green))
            tvOpenStatus.text =
                resources.getString(
                    R.string.working_hours,
                    formatHhMmTime(startWorkingSession),
                    formatHhMmTime(endWorkingSession),
                    resources.getString(R.string.open)
                )
        }
    }
}

fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
    val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
    vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
    val bitmap =
        Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

@SuppressLint("SetTextI18n")
fun formatHhMmTime(time: Long): String {
    val dateTimeFormat = DateTimeFormat.forPattern("HH:mm")
    val startOfDayTime = DateTime(System.currentTimeMillis()).withTimeAtStartOfDay().toDate().time
    return dateTimeFormat.print(DateTime(startOfDayTime + time))
}

fun Activity.setupFabScrollReaction(fab: FloatingActionButton, rvCards: RecyclerView) {
    rvCards.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0 || dy < 0 && fab.isShown) {
                fab.hide()
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                fab.show()
            }

            super.onScrollStateChanged(recyclerView, newState)
        }
    })
}

fun isPhoneNumberCorrect(phone: String): Boolean {
    return formatPhoneNumber(phone).matches(Regex("[0-9]{9}")) &&
        formatPhoneNumber(phone).length == 9
}

fun formatPhoneNumber(rawPhone: String): String {
    return rawPhone.replace("+", "")
        .replace(" ", "")
        .replace("(", "")
        .replace(")", "")
}

fun Activity.showAddCardDialog(): Maybe<Card> {
    val dialogBuilder = MaterialDialog.Builder(this)

    val dialogView = layoutInflater.inflate(R.layout.dialog_add_card, null, false)
    dialogBuilder.customView(dialogView, false)

    val addCardDialog = dialogBuilder.build()
    addCardDialog.setCancelable(false)
    addCardDialog.show()

    val etCardNumber = (dialogView.findViewById(R.id.et_card_number) as MaskedEditText)
    val etDateExpiring = (dialogView.findViewById(R.id.et_date_expiration) as MaskedEditText)
    val etCvc = (dialogView.findViewById(R.id.et_cvc) as MaskedEditText)
    val etName = (dialogView.findViewById(R.id.et_name) as EditText)

    val flCardNumber = (dialogView.findViewById(R.id.fl_card_number) as FrameLayout)
    val flDateExpiring = (dialogView.findViewById(R.id.fl_date_expiration) as FrameLayout)
    val flCvc = (dialogView.findViewById(R.id.fl_cvc) as FrameLayout)
    val flName = (dialogView.findViewById(R.id.fl_name) as FrameLayout)

    initInputStrokeEffect(etCardNumber, flCardNumber, resources)
    initInputStrokeEffect(etDateExpiring, flDateExpiring, resources)
    initInputStrokeEffect(etCvc, flCvc, resources)
    initInputStrokeEffect(etName, flName, resources)

    (flCardNumber.background as GradientDrawable).setStroke(
        1,
        resources.getColor(R.color.activity_credentials_phone_container_unfocused_border)
    )
    (flDateExpiring.background as GradientDrawable).setStroke(
        1,
        resources.getColor(R.color.activity_credentials_phone_container_unfocused_border)
    )
    (flCvc.background as GradientDrawable).setStroke(
        1,
        resources.getColor(R.color.activity_credentials_phone_container_unfocused_border)
    )
    (flName.background as GradientDrawable).setStroke(
        1,
        resources.getColor(R.color.activity_credentials_phone_container_unfocused_border)
    )


    etCardNumber.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            text?.let {
                if (it.length == 4 || it.length == 9 || it.length == 14) {
                    etCardNumber.setText("$it ")
                    etCardNumber.setSelection(it.length)
                }
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })

    return Maybe.create { emitter ->
        dialogView.findViewById<TextView>(R.id.btn_add).setOnClickListener {
            when {
                areFieldsEmpty(etCardNumber, etDateExpiring, etCvc, etName) -> showToast(
                    this,
                    R.string.dialog_add_card_empty_fields
                )
                isInternetConnectionUnavailable() -> showToast(this, R.string.no_internet_connection)
                else -> {
                    addCardDialog.dismiss()
                    emitter.onSuccess(
                        Card(
                            etCardNumber.rawText,
                            etCvc.rawText,
                            etDateExpiring.rawText.substring(0, 2),
                            etDateExpiring.rawText.substring(2, 4),
                            etName.text.toString()
                        )
                    )
                }
            }
        }
        dialogView.findViewById<TextView>(R.id.btn_cancel).setOnClickListener {
            addCardDialog.dismiss()
            emitter.onComplete()
        }
    }
}

private fun areFieldsEmpty(
    etCardNumber: MaskedEditText,
    etDateExpiring: MaskedEditText,
    etCvc: MaskedEditText,
    etName: EditText
): Boolean {
    return etCardNumber.rawText.length != 16 || etDateExpiring.rawText.length != 4
        || etCvc.rawText.length != 3 || etName.text.isEmpty()
}

private fun initInputStrokeEffect(editText: EditText, frameLayout: FrameLayout, resources: Resources) {

    editText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
        val drawable = frameLayout.background as GradientDrawable
        if (hasFocus) {
            drawable.setStroke(4, resources.getColor(R.color.colorPrimary))
        } else {
            drawable.setStroke(1, resources.getColor(R.color.activity_credentials_phone_container_unfocused_border))
        }
    }
}

