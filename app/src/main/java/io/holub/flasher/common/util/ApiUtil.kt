package io.holub.flasher.common.util

import android.content.Context
import android.net.ConnectivityManager
import io.holub.flasher.common.App

fun isInternetConnectionUnavailable() = (App.instance
    .appComponent()
    .context()
    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
    .activeNetworkInfo == null
