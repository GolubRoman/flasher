package io.holub.flasher.common.util

import com.google.gson.Gson
import io.holub.flasher.R
import io.holub.flasher.common.App
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.error.ReadableRestError
import io.holub.flasher.common.error.RestError
import io.reactivex.*
import retrofit2.HttpException

val UNKNOWN_ERROR_CODE = "UNKNOWN_ERROR_CODE"
val UNKNOWN_ERROR_TITLE = App.instance.appComponent().context().resources.getString(R.string.unknown_error_title)
val UNKNOWN_ERROR_MESSAGE = App.instance.appComponent().context().resources.getString(R.string.unknown_error)

fun makeReadableRestError(readableCode: String, readableTitle: String, readableMessage: String) =
    if (readableMessage == UNKNOWN_ERROR_MESSAGE) {
        ReadableRestError(UNKNOWN_ERROR_CODE, UNKNOWN_ERROR_TITLE, UNKNOWN_ERROR_MESSAGE)
    } else {
        ReadableRestError(readableCode, readableTitle, readableMessage)
    }

fun exampleCallMethodOnErrorOccured(readableCode: String) = makeReadableRestError(readableCode, when (readableCode) {
    UNKNOWN_ERROR_CODE -> UNKNOWN_ERROR_TITLE
    else -> UNKNOWN_ERROR_TITLE
}, when (readableCode) {
    UNKNOWN_ERROR_CODE -> UNKNOWN_ERROR_MESSAGE
    else -> UNKNOWN_ERROR_MESSAGE
})

fun mapThrowableToReadableError(throwable: Throwable,
                                errorMessage: (String) -> ReadableRestError): Throwable =
    if (throwable is HttpException) {
        val restError = try {
            Gson().fromJson(throwable.response().errorBody().toString(), RestError::class.java)
                ?: RestError()
        } catch (e: Exception) {
            RestError()
        }
        ErrorHandler.logError(restError.debugMessage)
        errorMessage(restError.readableCode)
    } else {
        throwable
    }

fun <T> Single<T>.convertErrorToReadable(errorMessage: (String) -> ReadableRestError): Single<T> =
    onErrorResumeNext { throwable: Throwable ->
        Single.error<T>(mapThrowableToReadableError(throwable, errorMessage))
    }

fun <T> Maybe<T>.convertErrorToReadable(errorMessage: (String) -> ReadableRestError): Maybe<T> =
    onErrorResumeNext { throwable: Throwable ->
        Maybe.error<T>(mapThrowableToReadableError(throwable, errorMessage))
    }

fun <T> Observable<T>.convertErrorToReadable(errorMessage: (String) -> ReadableRestError): Observable<T> =
    onErrorResumeNext { throwable: Throwable ->
        Observable.error<T>(mapThrowableToReadableError(throwable, errorMessage))
    }

fun <T> Flowable<T>.convertErrorToReadable(errorMessage: (String) -> ReadableRestError): Flowable<T> =
    onErrorResumeNext { throwable: Throwable ->
        Flowable.error<T>(mapThrowableToReadableError(throwable, errorMessage))
    }

fun Completable.convertErrorToReadable(errorMessage: (String) -> ReadableRestError): Completable =
    onErrorResumeNext { throwable: Throwable ->
        Completable.error(mapThrowableToReadableError(throwable, errorMessage))
    }
