package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.Characteristic

data class CharacteristicDto(
    var characteristicId: String,
    var categoryId: String,
    var name: String,
    var subCharacteristics: List<SubCharacteristicDto>
) {

    constructor() : this("", "", "", listOf())

    fun toCharacteristic(): Characteristic {
        return Characteristic(characteristicId, categoryId, name)
    }
}