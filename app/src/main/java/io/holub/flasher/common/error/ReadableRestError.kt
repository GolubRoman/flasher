package io.holub.flasher.common.error

data class ReadableRestError(
    var readableCode: String,
    var readableTitle: String,
    var readableMessage: String
) : Throwable(readableMessage)