package io.holub.flasher.common.di

import android.arch.persistence.room.Room
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.io.db.RoomDatabase
import io.holub.flasher.common.io.location.NotificationManager
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.order.db.OrderDao
import io.holub.flasher.profile.PaymentDao
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.StoreDao
import io.holub.flasher.user.UserInteractor
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideUserInteractor(
        prefs: Prefs,
        paymentDao: PaymentDao,
        firebaseAuth: FirebaseAuth
    ): UserInteractor {
        return UserInteractor(prefs, paymentDao, firebaseAuth)
    }

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }

    @Provides
    @Singleton
    fun provideStoreInteractor(
        prefs: Prefs, storeDao: StoreDao, firebaseDatabase: FirebaseDatabase,
        notificationManager: NotificationManager
    ): StoreInteractor {
        return StoreInteractor(prefs, storeDao, firebaseDatabase, notificationManager)
    }

    @Provides
    @Singleton
    fun provideStoreDao(roomDatabase: RoomDatabase): StoreDao {
        return roomDatabase.getStoreDao()
    }

    @Provides
    @Singleton
    fun provideOrderInteractor(prefs: Prefs, orderDao: OrderDao, storeDao: StoreDao): OrderInteractor {
        return OrderInteractor(prefs, orderDao, storeDao)
    }

    @Provides
    @Singleton
    fun provideOrderDao(roomDatabase: RoomDatabase): OrderDao {
        return roomDatabase.getOrderDao()
    }

    @Provides
    @Singleton
    fun providePaymentDao(roomDatabase: RoomDatabase): PaymentDao {
        return roomDatabase.getPaymentDao()
    }

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @Singleton
    fun provideNotificationManager(context: Context): NotificationManager {
        return NotificationManager(context)
    }

    @Provides
    @Singleton
    fun provideErrorHandler(context: Context): ErrorHandler {
        return ErrorHandler(context)
    }

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): RoomDatabase {
        return Room.databaseBuilder(context, RoomDatabase::class.java, RoomDatabase::class.java.simpleName)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun providePrefs(): Prefs {
        return Prefs(context)
    }

}
