package io.holub.flasher.common.io.db

import io.holub.flasher.store.db.Store

class StoreDto (
    var id: String,
    var merchantId: String,
    var name: String,
    var photoUrl: String,
    var address: String,
    var phone: String,
    var description: String,
    var longitude: Double,
    var latitude: Double,
    var categories: List<StoreCategoryDto>,
    var stuffs: List<StuffDto>,
    var startWorkingSession: Long,
    var endWorkingSession: Long,
    var deleted: Boolean,
    var blocked: Boolean
) {

    constructor() : this("", "", "", "", "", "", "",
        0.0, 0.0, listOf<StoreCategoryDto>(), listOf<StuffDto>(), 0, 0, false, false)

    fun toStore(): Store {
        return Store(id, merchantId, name, photoUrl, address, phone, description,
            startWorkingSession, endWorkingSession, longitude, latitude, deleted, blocked)
    }

}