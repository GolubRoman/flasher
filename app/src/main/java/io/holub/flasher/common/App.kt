package io.holub.flasher.common

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.content.IntentFilter
import android.provider.Settings
import com.annimon.stream.Optional
import com.crashlytics.android.Crashlytics
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.messaging.FirebaseMessaging
import com.tspoon.traceur.Traceur
import io.fabric.sdk.android.Fabric
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppModule
import io.holub.flasher.common.di.DaggerAppComponent
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.io.location.GoogleService
import io.holub.flasher.common.io.location.LocationChangeReceiver
import org.joda.time.chrono.ISOChronology

class App : Application() {

    private lateinit var appComponent: AppComponent

    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()
        Traceur.enableLogging()
        Fabric.with(this, Crashlytics())
        instance = this

        warmUpCache()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .build()
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        generateDeviceId(appComponent.prefs())

        initLocationProcessing()
    }

    private fun initLocationProcessing() {
        registerReceiver(LocationChangeReceiver(), IntentFilter(GoogleService.LOCATION_BROADCAST_RECEIVER_ACTION))
        startService(Intent(this, GoogleService::class.java))
    }

    private fun generateDeviceId(prefs: Prefs) {
        val deviceIdOpt = prefs.get<String>(Prefs.Key.DEVICE_ID)
            .map { Optional.ofNullable(it) }
            .toSingle(Optional.empty())
            .blockingGet()
        if (!deviceIdOpt.isPresent) {
            prefs.blockingPut(
                Prefs.Key.DEVICE_ID,
                Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
            )
        }
    }

    private fun warmUpCache() {
        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(this)
        ISOChronology.getInstance()
    }

    fun appComponent(): AppComponent {
        return appComponent
    }

    companion object {

        lateinit var instance: App
            private set
    }
}
