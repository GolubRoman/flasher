package io.holub.flasher.common.io.db

import io.holub.flasher.store.db.Stuff

data class StuffDto(
    val stuffId: String,
    val storeCategoryId: String,
    val stuffCategoryId: String,
    val parentStoreId: String,
    val name: String,
    val photoUrl: String,
    val description: String,
    val price: Int,
    var salesCount: Int,
    var blocked: Boolean,
    var discountPercentage: Int,
    val deleted: Boolean,
    val stuffCategory: StuffCategoryDto,
    val stuffMovies: List<StuffMovieDto>,
    val stuffPhotos: List<StuffPhotoDto>
) {

    constructor() : this("", "", "", "", "", "",
        "", 0, 0, false, 0,false, StuffCategoryDto(), listOf(), listOf())

    fun toStuff(): Stuff {
        return Stuff(stuffId, storeCategoryId, stuffCategoryId, parentStoreId, name, photoUrl, description, price, salesCount, blocked, discountPercentage, deleted)
    }

}