package io.holub.flasher.common.io.db

import io.holub.flasher.store.stuff.db.StuffCategory

data class StuffCategoryDto(
    var id: String,
    var stuffId: String,
    var categoryId: String,
    var stuffCharacteristics: List<StuffCharacteristicDto>
) {

    constructor() : this("", "", "", listOf())

    fun toStuffCategory(): StuffCategory {
        return StuffCategory(id, stuffId, categoryId)
    }
}