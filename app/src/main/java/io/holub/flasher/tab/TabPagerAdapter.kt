package io.holub.flasher.tab

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import io.holub.flasher.R
import io.holub.flasher.main.MainFragment
import io.holub.flasher.map.MapFragment
import io.holub.flasher.order.orders_list.OrdersFragment
import io.holub.flasher.profile.ProfileFragment
import java.util.Arrays.asList

internal class TabPagerAdapter(fm: FragmentManager, context: Context) : FragmentPagerAdapter(fm) {

    private val fragments = asList(MainFragment(), MapFragment(),
        OrdersFragment(), ProfileFragment())
    private val titles: List<String> = asList(context.resources.getString(R.string.activity_tab_main),
        context.resources.getString(R.string.activity_tab_map), context.resources.getString(R.string.activity_tab_history), context.resources.getString(R.string.activity_tab_profile))

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}