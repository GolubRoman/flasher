package io.holub.flasher.tab

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.common.view.TabFragment
import io.holub.flasher.databinding.ActivityTabBinding
import io.holub.flasher.login.welcome.WelcomeActivity
import io.holub.flasher.main.MainViewModel
import io.holub.flasher.main.OnPageSelectedListener
import io.holub.flasher.order.ConfirmOrderActivity
import io.holub.flasher.store.cart.CartAdapter
import io.reactivex.android.schedulers.AndroidSchedulers

class TabActivity : AppCompatActivity() {

    private var binding: ActivityTabBinding? = null
    private var viewModel: TabViewModel? = null
    private var noInternetSnackbar: Snackbar? = null
    private var networkStateMonitor: NetworkStateMonitor? = null
    private var pagerAdapter: TabPagerAdapter? = null
    private var cartAdapter: CartAdapter? = null
    private var gestureDetector: GestureDetector? = null
    private var _xDelta = 0
    private var _yDelta = 0

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTransparentStatusBar(window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tab)
        viewModel = ViewModelProviders.of(this).get(TabViewModel::class.java)
        viewModel?.processUserAuthorization()
    }

    private fun TabViewModel.processUserAuthorization() = isUserAlreadyAuthorized()
        .subscribe({
            gestureDetector = GestureDetector(this@TabActivity, SingleTapConfirm())
            initCartButton()
            initViewPager()
            observeInternetConnectionLiveData()
            observeDisplayedCartStuffsLiveData()

            showProgressBarFrame(binding?.flProgressBar)
            viewModel?.downsyncAllData()
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    hideProgressBarFrame(binding?.flProgressBar)
                }, {
                    hideProgressBarFrame(binding?.flProgressBar)
                    ErrorHandler.logError(it)
                })
        },
            { ErrorHandler.logError(it) },
            {
                WelcomeActivity.start(this@TabActivity, true)
            })

    override fun onResume() {
        super.onResume()
        networkStateMonitor?.emitCurrentState()
    }

    @SuppressLint("SetTextI18n")
    private fun observeDisplayedCartStuffsLiveData() {
        viewModel?.let { viewModel ->
            binding?.clCartContainer?.let { cartBinding ->
                cartAdapter = CartAdapter(viewModel)
                cartBinding.rvStuffs.layoutManager = LinearLayoutManager(this)
                cartBinding.rvStuffs.itemAnimator = null
                cartBinding.rvStuffs.adapter = cartAdapter
                cartAdapter?.observeOnConfirmOrderClickedObservable()
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe({
                        ConfirmOrderActivity.start(this, it, false)
                    }, { ErrorHandler.logError(it) })
                viewModel.observeDisplayedCartStuffs().observe(this, Observer {
                    cartAdapter?.submitList(it)
                    if (it?.size ?: 0 == 0) {
                        showEmptyListView(cartBinding.rvStuffs, cartBinding.flEmptyList)
                        binding?.tvCartCount?.visibility = View.INVISIBLE
                    } else {
                        hideEmptyListView(cartBinding.rvStuffs, cartBinding.flEmptyList)
                        binding?.tvCartCount?.visibility = View.VISIBLE
                    }
                    it?.let {
                        cartBinding.tvPrice.text =
                            formatDoubleSumPrice(it.snapshot().map { it.stuff.formatPrice().toDouble() * it.cartStuff.count }.sum()) + "₴"
                        cartBinding.tvPriceWithDiscount.text =
                            formatDoubleSumPrice(
                                it.snapshot().map { it.stuff.formatPriceWithDiscount().toDouble() * it.cartStuff.count }.sum()
                            ) + "₴"
                        if (cartBinding.tvPrice.text != cartBinding.tvPriceWithDiscount.text) {
                            strikeThroughText(cartBinding.tvPrice)
                            cartBinding.llPriceWithDiscountContainer.visibility = View.VISIBLE
                        } else {
                            removeStrikeThroughText(cartBinding.tvPrice)
                            cartBinding.llPriceWithDiscountContainer.visibility = View.GONE
                        }
                        cartBinding.tvCount.text = it.snapshot().sumBy { it.cartStuff.count }.toString()
                        binding?.tvCartCount?.text = it.snapshot().sumBy { it.cartStuff.count }.toString()
                    }
                })
            }
        }

    }

    override fun onBackPressed() {
        if (binding?.btnCart?.isSelected == true) {
            collapseCart()
        } else {
            super.onBackPressed()
        }
    }

    private fun observeInternetConnectionLiveData() {
        networkStateMonitor = NetworkStateMonitor(this)
        binding?.flProgressBar?.setOnClickListener {}
        networkStateMonitor?.apply {
            observeNetworkState()
                .to { LiveDataReactiveStreams.fromPublisher(it) }
                .observe(this@TabActivity, Observer {
                    it?.let {
                        if (it) {
                            noInternetSnackbar?.let {
                                binding?.flProgressBar?.visibility = View.VISIBLE
                                Handler().postDelayed({
                                    binding?.flProgressBar?.visibility = View.INVISIBLE
                                }, 3000)
                                it.dismiss()
                            }
                        } else {
                            binding?.let { binding ->
                                viewModel?.let { noInternetSnackbar = showNoInternetSnackbar(binding) }
                            }
                        }
                    }
                })
        }
    }

    private fun showNoInternetSnackbar(binding: ActivityTabBinding): Snackbar {
        val snackBar = Snackbar.make(binding.tlTabs, getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
        snackBar.show()
        Handler().postDelayed({ if (snackBar.isShown) snackBar.dismiss() }, 6000)
        return snackBar
    }

    fun onTouch(view: View, event: MotionEvent): Boolean {
        val x = event.rawX.toInt()
        val y = event.rawY.toInt()
        if (gestureDetector?.onTouchEvent(event) == true) {
            if (binding?.btnCart?.isSelected == true) {
                collapseCart()
            } else {
                expandCart()
            }
            return true
        } else {
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    val lParams = view.layoutParams as ConstraintLayout.LayoutParams
                    _xDelta = x - lParams.leftMargin
                    _yDelta = y - lParams.topMargin
                }
                MotionEvent.ACTION_UP -> {
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                }
                MotionEvent.ACTION_POINTER_UP -> {
                }
                MotionEvent.ACTION_MOVE -> {
                    val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
                    layoutParams.leftMargin = x - _xDelta
                    layoutParams.topMargin = y - _yDelta
                    layoutParams.rightMargin = _xDelta - x
                    layoutParams.bottomMargin = 0
                    view.layoutParams = layoutParams
                }
            }
        }
        binding?.btnCart?.invalidate()
        return false
    }

    private fun collapseCart() {
        binding?.btnCart?.let {
            val x = (it.right + it.left) / 2
            val y = (it.top + it.bottom) / 2

            val startRadius =
                Math.hypot(binding?.clRoot?.width?.toDouble() ?: 0.0, binding?.clRoot?.height?.toDouble() ?: 0.0)
                    .toInt()
            val endRadius = 0

            val anim = ViewAnimationUtils.createCircularReveal(
                binding?.clCart, x, y,
                startRadius.toFloat(),
                endRadius.toFloat()
            )
            anim.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {}
                override fun onAnimationStart(animation: Animator?) {}
                override fun onAnimationCancel(animation: Animator?) {}
                override fun onAnimationEnd(animation: Animator?) {
                    binding?.clCart?.visibility = View.INVISIBLE
                }
            })
            anim.start()
            binding?.btnCart?.isSelected = !(binding?.btnCart?.isSelected ?: false)
        }
    }

    private fun expandCart() {
        binding?.btnCart?.let {
            val x = (it.right + it.left) / 2
            val y = (it.top + it.bottom) / 2

            val startRadius = 0
            val endRadius =
                Math.hypot(binding?.clRoot?.width?.toDouble() ?: 0.0, binding?.clRoot?.height?.toDouble() ?: 0.0)
                    .toInt()

            val anim = ViewAnimationUtils.createCircularReveal(
                binding?.clCart, x, y,
                startRadius.toFloat(),
                endRadius.toFloat()
            )
            binding?.clCart?.visibility = View.VISIBLE
            anim.start()
            binding?.btnCart?.isSelected = !(binding?.btnCart?.isSelected ?: false)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initCartButton() {
        binding?.btnCart?.setOnTouchListener { v, event -> onTouch(v, event) }
        binding?.btnCart?.setOnClickListener {
            if (binding?.btnCart?.isSelected == true) {
                collapseCart()
            } else {
                expandCart()
            }
        }
    }

    private fun initViewPager() {
        pagerAdapter = TabPagerAdapter(supportFragmentManager, this)
        binding?.vpPages?.adapter = pagerAdapter
        binding?.vpPages?.offscreenPageLimit = pagerAdapter?.count ?: 0
        binding?.tlTabs?.setupWithViewPager(binding?.vpPages)
        binding?.tlTabs?.getTabAt(0)?.setIcon(R.drawable.ic_main_selector)
        binding?.tlTabs?.getTabAt(1)?.setIcon(R.drawable.ic_map_selector)
        binding?.tlTabs?.getTabAt(2)?.setIcon(R.drawable.ic_history_selector)
        binding?.tlTabs?.getTabAt(3)?.setIcon(R.drawable.ic_profile_selector)
        Handler().postDelayed({ (pagerAdapter?.getItem(0) as TabFragment).init() }, 300)
        binding?.vpPages?.addOnPageChangeListener(object : OnPageSelectedListener {
            override fun onPageSelected(position: Int) {
                Handler().postDelayed({
                    val inputMethodService = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    val view = currentFocus ?: View(this@TabActivity)
                    inputMethodService.hideSoftInputFromWindow(view.windowToken, 0)
                    if (binding?.btnCart?.isSelected == true) { collapseCart() }
                }, 100)
                Handler().postDelayed({ (pagerAdapter?.getItem(position) as TabFragment).init() }, 300)
            }
        })
    }

    companion object {

        fun start(currentActivity: AppCompatActivity, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            currentActivity.startActivity(Intent(currentActivity, TabActivity::class.java))
        }
    }
}

private class SingleTapConfirm : GestureDetector.SimpleOnGestureListener() {

    override fun onSingleTapUp(event: MotionEvent): Boolean {
        return true
    }
}

