package io.holub.flasher.tab

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.cart.DisplayedCartStuff
import io.holub.flasher.user.UserInteractor
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TabViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var userInteractor: UserInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerTabViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun incrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.incrementCartStuffCount(displayedCartStuff)
    }

    fun decrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.decrementCartStuffCount(displayedCartStuff)

    }

    fun removeCartStuff(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.removeCartStuff(displayedCartStuff)

    }

    fun observeDisplayedCartStuffs(): LiveData<PagedList<DisplayedCartStuff>> {
        return orderInteractor.observeDisplayedCartStuffs(0)
    }

    fun isUserAlreadyAuthorized(): Maybe<FirebaseUser> {
        return userInteractor.isUserAlreadyAuthorized()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun downsyncAllData(): Completable {
        return storeInteractor.downsyncAllData()
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(tabViewModel: TabViewModel)
    }
}


