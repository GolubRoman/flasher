package io.holub.flasher.order.payfororder

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.profile.Card
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.user.UserInteractor
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PayForOrderViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerPayForOrderViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun addCard(card: Card): Completable {
        return userInteractor.addCard(card)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun removeCard(card: Card) {
        userInteractor.removeCard(card)
    }

    fun setOrder(storeId: String, name: String, phone: String,
                 deliveryLatitude: Double, deliveryLongitude: Double, deliveryAddress: String): Completable {
        return orderInteractor.setOrder(storeId, name, phone, deliveryLatitude, deliveryLongitude, deliveryAddress)
            .subscribeOn(Schedulers.io())
    }

    fun observeOrderSum(storeId: String) = orderInteractor.observeDisplayedCartStuffsForStoreId(storeId)
        .map { displayedCartStuffs -> displayedCartStuffs.sumBy { it.cartStuff.count * ((1 - it.stuff.discountPercentage * 0.01) * it.stuff.price).toInt() } }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun observeAllCards(): LiveData<PagedList<Card>> {
        return userInteractor.observeCards(0)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(payForOrderViewModel: PayForOrderViewModel)
    }
}
