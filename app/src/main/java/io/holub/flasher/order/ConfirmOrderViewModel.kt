package io.holub.flasher.order

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.cart.DisplayedCartStuff
import io.holub.flasher.store.db.Store
import io.holub.flasher.user.UserInteractor
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ConfirmOrderViewModel : ViewModel() {

    @Inject
    lateinit var orderInteractor: OrderInteractor

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerConfirmOrderViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeStoreForStoreId(storeId: String): Maybe<Store> {
        return storeInteractor.observeStoreForStoreId(storeId)
            .subscribeOn(Schedulers.io())
    }

    fun observeCurrentUser(): Maybe<FirebaseUser> {
        return userInteractor.observeCurrentUser()
            .subscribeOn(Schedulers.io())
    }

    fun observeCartStuffs(storeId: String): LiveData<PagedList<DisplayedCartStuff>> {
        return orderInteractor.observeDisplayedCartStuffsForStoreIdLiveData(storeId, 0)
    }

    fun incrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.incrementCartStuffCount(displayedCartStuff)
    }

    fun decrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.decrementCartStuffCount(displayedCartStuff)

    }

    fun removeCartStuff(displayedCartStuff: DisplayedCartStuff?) {
        orderInteractor.removeCartStuff(displayedCartStuff)

    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(confirmOrderViewModel: ConfirmOrderViewModel)
    }
}


