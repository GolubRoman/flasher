package io.holub.flasher.order

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.GradientDrawable
import android.location.Geocoder
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityConfirmOrderBinding
import io.holub.flasher.login.credentials.CredentialsActivity
import io.holub.flasher.order.payfororder.PayForOrderActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class ConfirmOrderActivity : AppCompatActivity() {

    private var binding: ActivityConfirmOrderBinding? = null
    private var viewModel: ConfirmOrderViewModel? = null
    private var adapter: ConfirmOrderAdapter? = null
    private var googleMap: GoogleMap? = null
    private var onChangedMapPositionPublishSubject = PublishSubject.create<LatLng>()
    private val PERMISSIONS = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    private var deliveryLatitude = -1.0
    private var deliveryLongitude = -1.0
    private var deliveryAddress = ""

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWhiteStatusBar(window, this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_order)
        viewModel = ViewModelProviders.of(this).get(ConfirmOrderViewModel::class.java)
        viewModel?.observeCurrentUser()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ user ->
                intent?.getStringExtra(STORE_ID)?.let {
                    initViews(user, it)
                }
            }, {
                CredentialsActivity.start(this, true)
                ErrorHandler.logError(it)
            })
    }

    @SuppressLint("MissingPermission")
    private fun onMapReady(googleMap: GoogleMap, permissionGranted: Boolean) {
        this.googleMap = googleMap
        this.googleMap?.let {
            onChangedMapPositionPublishSubject
                .debounce(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ initAddressName(it) }, { showAlertDialog(this, R.string.address_not_found, R.string.address)})
            it.setOnCameraMoveListener {
                onChangedMapPositionPublishSubject.onNext(it.cameraPosition.target)
            }
            if (permissionGranted) {
                it.isMyLocationEnabled = true
            }
            it.uiSettings?.isMyLocationButtonEnabled = true
            it.uiSettings?.isCompassEnabled = true
            val kyiv = LatLng(50.4350207, 30.5281623)
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(kyiv, 11f))
            initAddressName(kyiv)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initAddressName(location: LatLng) {
        val geocoder = Geocoder(this)
        val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
        if(addresses.size > 0) {
            binding?.tvAddressValue?.text = addresses[0].getAddressLine(0)
        }
        this.deliveryAddress = addresses[0].getAddressLine(0)
        this.deliveryLatitude = location.latitude
        this.deliveryLongitude = location.longitude
    }

    @SuppressLint("CheckResult")
    private fun processVerticalTouchEventsOnMap() {
        PermissionUtils.requestEachPermissions(this, PERMISSIONS)
            .take(PERMISSIONS.size.toLong()).toList()
            .subscribe({ data ->
                val mapFragment = supportFragmentManager.findFragmentById(R.id.f_map) as OnScrollableContainerMapFragment
                mapFragment.getMapAsync{ onMapReady(it, data.none { !it.granted }) }
                mapFragment.setOnTouchListener(object : OnScrollableContainerMapFragment.OnTouchListener {
                    override fun onStartScrollingMap() {
                        binding?.collapsingToolbarLayout?.requestDisallowInterceptTouchEvent(true)
                    }
                    override fun onStopScrollingMap() {
                        binding?.collapsingToolbarLayout?.requestDisallowInterceptTouchEvent(false)
                    }
                })
            }, { ErrorHandler.logError(it) })
    }

    @SuppressLint("CheckResult")
    private fun initViews(currentUser: FirebaseUser, storeId: String) {
        showProgressBarFrame(binding?.flProgressBar)
        binding?.ivBack?.setOnClickListener { onBackPressed() }

        processVerticalTouchEventsOnMap()

        initOrderDishesList(storeId)

        initNameAndPhoneView(currentUser)
        initTitle(storeId)

        processPayButtonClicked(storeId)
    }

    private fun processPayButtonClicked(storeId: String) {
        binding?.let { binding ->
            binding.btnPay.setOnClickListener {
                when {
                    (binding.etPhone.text != null && binding.etPhone.text?.isEmpty()?: true) || binding.etName.text?.isEmpty()?: true ->
                        showAlertDialog(this, R.string.phone_number_and_name_are_empty, R.string.user_data)
                    !isPhoneNumberCorrect(binding.etPhone.rawText.toString()) ->
                        showAlertDialog(this, R.string.phone_number_incorrect, R.string.phone)
                    isInternetConnectionUnavailable() -> showAlertDialog(
                        this,
                        R.string.no_internet_connection,
                        R.string.no_internet_connection_title
                    )
                    else -> {
                        viewModel?.observeStoreForStoreId(storeId)
                            ?.observeOn(AndroidSchedulers.mainThread())
                            ?.subscribe({
                                PayForOrderActivity.start(this, it.merchantId,
                                    it.id, binding.etName.text.toString(), "380" + binding.etPhone.rawText,
                                    deliveryLatitude, deliveryLongitude, deliveryAddress, false)
                            }, {ErrorHandler.logError(it)})
                    }
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun initTitle(storeId: String) {
        viewModel?.observeStoreForStoreId(storeId)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                binding?.tvTitle?.text = it.name
            }, {ErrorHandler.logError(it)})
    }

    @SuppressLint("CheckResult", "SetTextI18n")
    private fun initOrderDishesList(storeId: String) {
        viewModel?.let{ adapter = ConfirmOrderAdapter(it) }
        binding?.rvCartStuffs?.layoutManager = LinearLayoutManager(this)
        binding?.rvCartStuffs?.itemAnimator = null
        binding?.rvCartStuffs?.adapter = adapter
        viewModel?.observeCartStuffs(storeId)?.observe(this, Observer {
            adapter?.submitList(it)
            if (it?.size ?: 0 == 0) {
                onBackPressed()
            }
            it?.let {
                binding?.btnPay?.text =
                    formatDoubleSumPrice(
                        it.snapshot()
                            .map { it.stuff.formatPriceWithDiscount().toDouble() * it.cartStuff.count }
                            .sum()) + "₴ " + resources.getString(R.string.pay)


            }
            hideProgressBarFrame(binding?.flProgressBar)
        })
    }

    private fun initNameAndPhoneView(currentUser: FirebaseUser) {
        initInputs()
        binding?.etName?.setText(currentUser.displayName)
    }

    private fun initInputs() {
        setEditTextFrameSelectable(binding?.etPhone, binding?.flPhoneContainer)
        setEditTextFrameSelectable(binding?.etName, binding?.flNameContainer)
        val drawable = binding?.flNameContainer?.background as GradientDrawable
        val enemyDrawable = binding?.flPhoneContainer?.background as GradientDrawable
        drawable.setStroke(4, resources.getColor(R.color.colorPrimary))
        enemyDrawable.setStroke(1, resources.getColor(R.color.activity_credentials_phone_container_unfocused_border))
    }

    companion object {

        val STORE_ID = "STORE_ID"

        fun start(currentActivity: AppCompatActivity, storeId: String, finishPrevious: Boolean) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }

            val intent = Intent(currentActivity, ConfirmOrderActivity::class.java)
            intent.putExtra(STORE_ID, storeId)
            currentActivity.startActivity(intent)
        }
    }
}