package io.holub.flasher.order.orders_list

import android.annotation.SuppressLint
import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.databinding.ItemOrderBinding
import io.holub.flasher.order.db.Order
import io.reactivex.android.schedulers.AndroidSchedulers

class OrderAdapter(val viewModel: OrdersViewModel?) : PagedListAdapter<Order, OrderAdapter.OrderViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val binding = DataBindingUtil.inflate<ItemOrderBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_order, parent, false
        )
        return OrderViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    inner class OrderViewHolder(val binding: ItemOrderBinding) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("CheckResult", "SetTextI18n")
        fun bindTo(order: Order?) {
            if (order != null) {
                binding.order = order
                binding.executePendingBindings()
                initCenterCropImageViewViaGlide(binding.ivPhoto, order.storePhotoUrl)

                val orderStuffsAdapter = OrderStuffsAdapter()
                binding.rvStuffs.layoutManager =
                    LinearLayoutManager(binding.ivPhoto.context, LinearLayoutManager.HORIZONTAL, false)
                binding.rvStuffs.adapter = orderStuffsAdapter
                viewModel?.observeOrderStuffsForOrderId(order.orderId)
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe({
                        orderStuffsAdapter.submitList(it)
                        binding.tvPrice.text =
                            (it.map { it.totalPriceWithDiscount }.sum().toFloat() / 100).toString() + " ₴"
                        binding.tvCount.text = (it.map { it.count }.sum()).toString()
                    }, { ErrorHandler.logError(it) })
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Order> = object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(
                item1: Order, item2: Order
            ): Boolean {
                return item1.orderId == item2.orderId
            }

            override fun areContentsTheSame(
                item1: Order, item2: Order
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
