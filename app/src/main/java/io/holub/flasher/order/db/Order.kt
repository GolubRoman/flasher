package io.holub.flasher.order.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import java.text.DecimalFormat
import java.util.Arrays.asList

@Entity
class Order {
    @PrimaryKey
    @NonNull
    @SerializedName("id")
    var orderId: String
    var timestamp: Long = -1L
    var storeId: String
    var storeName: String
    var storePhotoUrl: String
    var priceWithDiscount: Int = -1
    var price: Int = -1
    var stuffsCount: Int = -1
    var deliveryLatitude: Double = -1.0
    var deliveryLongitude: Double = -1.0
    var deliveryAddress: String = ""
    var userName: String? = ""
    var contactNumber: String? = ""

    @Ignore
    var orderItems: List<OrderStuff>

    constructor(
        orderId: String,
        timestamp: Long,
        storeId: String,
        storeName: String,
        storePhotoUrl: String,
        priceWithDiscount: Int,
        price: Int,
        stuffsCount: Int,
        deliveryLatitude: Double,
        deliveryLongitude: Double,
        deliveryAddress: String,
        userName: String,
        contactNumber: String
    ) {
        this.orderId = orderId
        this.timestamp = timestamp
        this.storeId = storeId
        this.storeName = storeName
        this.storePhotoUrl = storePhotoUrl
        this.priceWithDiscount = priceWithDiscount
        this.price = price
        this.stuffsCount = stuffsCount
        this.deliveryLatitude = deliveryLatitude
        this.deliveryLongitude = deliveryLongitude
        this.deliveryAddress = deliveryAddress
        this.userName = userName
        this.contactNumber = contactNumber
        this.orderItems = asList()
    }

    fun formatPrice() = DecimalFormat("###.##").format(price * 0.01).replace(",", ".")

    fun formatPriceWithDiscount() = DecimalFormat("###.##").format(priceWithDiscount * 0.01).replace(",", ".")
}
