package io.holub.flasher.order

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.holub.flasher.R
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.util.strikeThroughText
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemCartStuffForOrderBinding
import io.holub.flasher.store.cart.DisplayedCartStuff

class ConfirmOrderAdapter(val viewModel: ConfirmOrderViewModel) : PagedListAdapter<DisplayedCartStuff, ConfirmOrderAdapter.CartViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = DataBindingUtil.inflate<ItemCartStuffForOrderBinding>(LayoutInflater.from(parent.context),
            R.layout.item_cart_stuff_for_order, parent, false)
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    inner class CartViewHolder(val binding: ItemCartStuffForOrderBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(displayedCartStuff: DisplayedCartStuff?) {
            if (displayedCartStuff != null) {
                binding.displayedCartStuff = displayedCartStuff
                binding.executePendingBindings()
                if (binding.tvPrice.text != binding.tvPriceWithDiscount.text) {
                    strikeThroughText(binding.tvPrice)
                    binding.tvPriceWithDiscount.visibility = View.VISIBLE
                } else {
                    binding.tvPriceWithDiscount.visibility = View.GONE
                }
                initButtons(displayedCartStuff)
                initCenterCropImageViewViaGlide(binding.ivPhoto, displayedCartStuff.stuff.photoUrl)
            }
        }

        private fun initButtons(displayedCartStuff: DisplayedCartStuff?) {
            binding.ivPlus.setOnClickListener { viewModel.incrementCartStuffCount(displayedCartStuff) }
            binding.ivMinus.setOnClickListener {
                if (displayedCartStuff?.cartStuff?.count?: 0 > 1) {
                    viewModel.decrementCartStuffCount(displayedCartStuff)
                } else {
                    viewModel.removeCartStuff(displayedCartStuff)
                }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<DisplayedCartStuff> = object : DiffUtil.ItemCallback<DisplayedCartStuff>() {
            override fun areItemsTheSame(
                item1: DisplayedCartStuff, item2: DisplayedCartStuff): Boolean {
                return item1.cartStuff.cartStuffId == item2.cartStuff.cartStuffId
            }

            override fun areContentsTheSame(
                item1: DisplayedCartStuff, item2: DisplayedCartStuff): Boolean {
                return item1 == item2
            }
        }
    }
}
