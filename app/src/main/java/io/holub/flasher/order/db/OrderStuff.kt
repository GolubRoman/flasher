package io.holub.flasher.order.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.text.DecimalFormat

@Entity
data class OrderStuff(
    @PrimaryKey
    @NonNull
    var orderStuffId: String,
    var orderId: String,
    var stuffId: String,
    var stuffCategoryId: String,
    var name: String,
    var photoUrl: String,
    var description: String,
    var perStuffPrice: Int,
    var perStuffPriceWithDiscount: Int,
    var totalPrice: Int,
    var totalPriceWithDiscount: Int,
    var count: Int
) {
    fun formatPrice() = DecimalFormat("##.##").format((totalPrice * 0.01).toInt())

    fun formatPriceWithDiscount() = DecimalFormat("##.##").format((totalPriceWithDiscount * 0.01).toInt())
}