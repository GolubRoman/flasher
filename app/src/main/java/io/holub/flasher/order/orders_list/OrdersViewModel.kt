package io.holub.flasher.order.orders_list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.order.db.Order
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.order.db.OrderStuff
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OrdersViewModel : ViewModel() {

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerOrdersViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeAllOrders(): LiveData<PagedList<Order>> {
        return orderInteractor.observeAllOrders(0)
    }

    fun observeOrderStuffsForOrderId(orderId: String): Maybe<List<OrderStuff>> {
        return orderInteractor.observeOrderStuffsForOrderId(orderId)
            .subscribeOn(Schedulers.io())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(ordersViewModel: OrdersViewModel)
    }
}


