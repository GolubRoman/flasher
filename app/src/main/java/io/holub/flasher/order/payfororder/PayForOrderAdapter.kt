package io.holub.flasher.order.payfororder

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemCardBinding
import io.holub.flasher.profile.Card
import io.holub.flasher.profile.CardType
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class PayForOrderAdapter : PagedListAdapter<Card, PayForOrderAdapter.CardViewHolder>(DIFF_CALLBACK) {

    private val onRemoveCardClickedPublishSubject = PublishSubject.create<Card>()
    private val onCardClickPublishSubject = PublishSubject.create<Card>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val binding = DataBindingUtil.inflate<ItemCardBinding>(LayoutInflater.from(parent.context),
            R.layout.item_card, parent, false)
        return CardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        getItem(holder.adapterPosition)?.let { holder.bindTo(it) }
    }

    fun observeOnChooseCardClicked(): Observable<Card> = onCardClickPublishSubject

    fun observeOnRemoveCardClicked(): Observable<Card> = onRemoveCardClickedPublishSubject

    inner class CardViewHolder(val binding: ItemCardBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(card: Card) {
            binding.card = card
            binding.executePendingBindings()
            initImageView(card)
            initOnCardRemoveClickListener(card)
        }

        private fun initImageView(card: Card) {
            val photoResId = when (card.cardType()) {
                CardType.VISA -> R.drawable.ic_visa
                CardType.MASTERCARD -> R.drawable.ic_mastercard
                else -> R.drawable.ic_default_card
            }
            initCenterCropImageViewViaGlide(binding.ivPhoto, photoResId)
        }

        private fun initOnCardRemoveClickListener(card: Card) {
            binding.ivRemove.setOnClickListener {
                binding.card?.let { onRemoveCardClickedPublishSubject.onNext(card) }
            }
            binding.root.setOnClickListener {
                binding.card?.let { onCardClickPublishSubject.onNext(card) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Card> = object : DiffUtil.ItemCallback<Card>() {
            override fun areItemsTheSame(
                item1: Card, item2: Card): Boolean {
                return item1.cardNumber == item2.cardNumber
            }

            override fun areContentsTheSame(
                item1: Card, item2: Card): Boolean {
                return item1 == item2
            }
        }
    }
}
