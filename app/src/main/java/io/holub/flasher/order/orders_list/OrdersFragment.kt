package io.holub.flasher.order.orders_list

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import io.holub.flasher.R
import io.holub.flasher.common.util.*
import io.holub.flasher.common.view.TabFragment
import io.holub.flasher.databinding.FragmentOrdersBinding

class OrdersFragment : TabFragment() {

    private var binding: FragmentOrdersBinding? = null
    private var viewModel: OrdersViewModel? = null
    private var adapter: OrderAdapter? = null
    private var isInitted = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false)
        viewModel = ViewModelProviders.of(this).get(OrdersViewModel::class.java)
        return binding?.root
    }

    @SuppressLint("CheckResult")
    private fun initOrdersList() {
        initSwipeToRefreshView(binding?.srlSwipeToRefresh, binding?.flProgressBar)

        adapter = OrderAdapter(viewModel)
        binding?.rvOrders?.layoutManager = LinearLayoutManager(activity)
        binding?.rvOrders?.adapter = adapter
        viewModel?.observeAllOrders()?.observe(this, Observer {
            adapter?.submitList(it)
            if (it?.size ?: 0 == 0) {
                showEmptyListView(binding?.rvOrders, binding?.flEmptyList)
            } else {
                hideEmptyListView(binding?.rvOrders, binding?.flEmptyList)
            }
            hideProgressBarFrame(binding?.flProgressBar)
        })
    }

    override fun init() {
        if (!isInitted) {
            showProgressBarFrame(binding?.flProgressBar)
            initOrdersList()
            isInitted = true
        }
    }
}
