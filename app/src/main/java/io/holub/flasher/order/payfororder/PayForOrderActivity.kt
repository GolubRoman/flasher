package io.holub.flasher.order.payfororder

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import com.cloudipsp.android.CardFactory
import com.cloudipsp.android.Cloudipsp
import com.cloudipsp.android.Order
import com.cloudipsp.android.Receipt
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.databinding.ActivityPayForOrderBinding
import io.holub.flasher.profile.Card
import io.holub.flasher.tab.TabActivity
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import java.lang.System.currentTimeMillis
import java.util.*

const val MERCHANT_ID_KEY = "merchantId"

class PayForOrderActivity : AppCompatActivity() {

    private var binding: ActivityPayForOrderBinding? = null
    private var viewModel: PayForOrderViewModel? = null
    private var adapter: PayForOrderAdapter? = null
    private var fondyApi: Cloudipsp? = null
    private var currentRequestId: String? = null

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initStatusBar()
        processMerchantFromIntent(intent)
        initAddCardView()
        binding?.ivBack?.setOnClickListener { onBackPressed() }
    }

    private fun processMerchantFromIntent(intent: Intent?) {
        val merchantId = intent?.getIntExtra(MERCHANT_ID_KEY, 0) ?: 0
        if (merchantId != 0) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_pay_for_order)
            viewModel = ViewModelProviders.of(this).get(PayForOrderViewModel::class.java)
            fondyApi = Cloudipsp(merchantId, binding?.webViewConfirmPayment)
            initCardsList()
        } else {
            throw IllegalStateException("MerchantId is null while onCreate()")
        }
    }

    private fun initStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.activity_credentials_status_bar_color)
        }
    }

    override fun onResume() {
        super.onResume()
        revalidateButtonsAndCardList()
    }

    @SuppressLint("CheckResult")
    private fun initCardsList() {
        binding?.let { binding ->
            adapter = PayForOrderAdapter()
            binding.rvCards.layoutManager = LinearLayoutManager(this)
            binding.rvCards.adapter = adapter
            viewModel?.observeAllCards()?.observe(this, Observer {
                adapter?.submitList(it)
                if (it?.snapshot()?.size ?: 0 > 0) {
                    hideEmptyListView(binding.rvCards, binding.flEmptyList)
                } else {
                    showEmptyListView(binding.rvCards, binding.flEmptyList)
                }
            })
            adapter?.processSpecificCardChosen(binding)
            adapter?.processSpecificCardRemoveClicked(binding)
        }
    }

    private fun PayForOrderAdapter.processSpecificCardChosen(binding: ActivityPayForOrderBinding) =
        observeOnChooseCardClicked()
            .subscribe({
                if (!binding.rvCards.isLayoutFrozen) {
                    binding.rvCards.isLayoutFrozen = true
                    binding.fabAddCard.visibility = GONE
                    binding.executePendingBindings()
                    handleCardChosen(it)
                }
            }, {
                ErrorHandler.logError(it)
                showAlertDialog(this@PayForOrderActivity, R.string.cannot_find_the_card, R.string.card)
            })

    private fun PayForOrderAdapter.processSpecificCardRemoveClicked(binding: ActivityPayForOrderBinding) =
        observeOnRemoveCardClicked()
            .subscribe({
                viewModel?.removeCard(it)
            }, {
                ErrorHandler.logError(it)
                showAlertDialog(this@PayForOrderActivity, R.string.cannot_find_the_card, R.string.card)
            })

    private fun setOrder(): Completable? {
        if (intent != null && viewModel != null) {
            if (intent.getStringExtra(STORE_ID) != null && !intent.getStringExtra(STORE_ID).isEmpty()
                && intent.getStringExtra(NAME) != null && !intent.getStringExtra(NAME).isEmpty()
                && intent.getDoubleExtra(DELIVERY_LATITUDE, -1.0) != -1.0
                && intent.getDoubleExtra(DELIVERY_LONGITUDE, -1.0) != -1.0
                && intent.getStringExtra(DELIVERY_ADDRESS) != null && !intent.getStringExtra(DELIVERY_ADDRESS).isEmpty()
            ) {
                return viewModel?.setOrder(
                    intent.getStringExtra(STORE_ID),
                    intent.getStringExtra(NAME), intent.getStringExtra(PHONE),
                    intent.getDoubleExtra(DELIVERY_LATITUDE, -1.0),
                    intent.getDoubleExtra(DELIVERY_LONGITUDE, -1.0),
                    intent.getStringExtra(DELIVERY_ADDRESS)
                )
                    ?.observeOn(AndroidSchedulers.mainThread())
            } else {
                return Completable.error(NullPointerException(resources.getString(R.string.order_completion_failed)))
            }
        } else {
            return Completable.error(NullPointerException(resources.getString(R.string.order_completion_failed)))
        }
    }

    @SuppressLint("CheckResult")
    private fun handleCardChosen(card: Card) {
        binding?.let { binding ->
            currentRequestId = UUID.randomUUID().toString()
            binding.flProgressBar.visibility = VISIBLE
            intent?.getStringExtra(STORE_ID)?.let {
                viewModel?.observeOrderSum(it)
                    ?.flatMapSingleElement { orderSum ->
                        payWithFondy(card, orderSum, "UAH")
                    }
                    ?.flatMapCompletable { fondyOrderId -> setOrder() }
                    ?.subscribe({
                        finishThisActivity()
                    }, {
                        ErrorHandler.logError(it)
                        revalidateButtonsAndCardList()
                        hideProgressBarInMillis(0)
                    })
            }

            // hide progress bar after 20 seconds anyway (avoid forever spinner in case of errors)
            hideProgressBarInMillis(20_000)
        }
    }

    @SuppressLint("CheckResult")
    private fun addCard(card: Card) {
        viewModel?.addCard(card)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ showAlertDialog(this, R.string.card_added, R.string.card_added_title) },
                { showAlertDialog(this, R.string.card_adding_failed, R.string.card_added_title) })
    }

    private fun initAddCardView() {
        binding?.let { binding ->
            binding.fabAddCard.setOnClickListener {
                showAddCardDialog().subscribe({ addCard(it) }, { ErrorHandler.logError(it) })
            }
            setupFabScrollReaction(binding.fabAddCard, binding.rvCards)
        }
    }

    private fun hideProgressBarInMillis(millis: Long) {
        binding?.let { binding ->
            val cachedCurrentRequestId = currentRequestId
            binding.flProgressBar.postDelayed({
                if (cachedCurrentRequestId == currentRequestId) {
                    binding.flProgressBar.visibility = GONE
                    currentRequestId = null
                }
            }, millis)
        }
    }

    fun generateFondyOrderId() = "TIM${UUID.randomUUID()}"

    private fun payWithFondy(card: Card, amount: Int, currency: String): Single<String> {
//        Setting testing card number
        card.cardNumber = "4444555511116666"
        val fondyCard = CardFactory.makeCard(card.cardNumber, card.monthExpiring, card.yearExpiring, card.cvc)
        val fondyOrderId = generateFondyOrderId()
        val fondyOrder = Order(
            amount, currency, fondyOrderId,
            "Amount: $amount; " +
                "orderId: $fondyOrderId; " +
                "time: ${currentTimeMillis()}",
            "botanique250198@gmail.com"
        )
        fondyOrder.setPreauth(true)
        return Single.create {
            fondyApi?.pay(fondyCard, fondyOrder, object : Cloudipsp.PayCallback {
                override fun onPaidFailure(e: Cloudipsp.Exception?) {
                    handlePaymentFailed(it, e)
                }

                override fun onPaidProcessed(receipt: Receipt?) {
                    if (receipt?.status == Receipt.Status.approved) it.onSuccess(fondyOrderId)
                    else handlePaymentFailed(
                        it, IllegalStateException(
                            "Payment failed, receipt:" +
                                "status: ${receipt?.status}; " +
                                "amount: ${receipt?.amount}; " +
                                "verification status: ${receipt?.verificationStatus}; " +
                                "approval code: ${receipt?.approvalCode};" +
                                "response code: ${receipt?.responseCode}"
                        )
                    )
                }
            })
            binding?.webViewConfirmPayment?.setPageLoadedCallback {
                hideProgressBarInMillis(7_000)
            }
        }
    }

    private fun handlePaymentFailed(emitter: SingleEmitter<String>, error: Throwable?) {
        val errorToReport = error
            ?: IllegalStateException("Fondy payment failure, fondy error was null")
        emitter.onError(errorToReport)
        ErrorHandler.logError(errorToReport)
        showAlertDialog(this@PayForOrderActivity, R.string.payment_failed, R.string.payment_failed_title)
    }

    private fun finishThisActivity() {
        finishAffinity()
        TabActivity.start(this, true)
    }

    override fun onBackPressed() {
        binding?.let { binding ->
            if (binding.webViewConfirmPayment.waitingForConfirm()) {
                binding.webViewConfirmPayment.skipConfirm()
                revalidateButtonsAndCardList()
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun revalidateButtonsAndCardList() {
        binding?.let { binding ->
            binding.rvCards.isLayoutFrozen = false
            binding.fabAddCard.visibility = VISIBLE
        }
    }

    companion object {

        val STORE_ID = "STORE_ID"
        val NAME = "NAME"
        val PHONE = "PHONE"
        val DELIVERY_LATITUDE = "DELIVERY_LATITUDE"
        val DELIVERY_LONGITUDE = "DELIVERY_LONGITUDE"
        var DELIVERY_ADDRESS = "DELIVERY_ADDRESS"

        fun start(
            currentActivity: AppCompatActivity,
            merchantId: String,
            storeId: String,
            userName: String,
            phone: String,
            deliveryLatitude: Double,
            deliveryLongitude: Double,
            deliveryAddress: String,
            finishPrevious: Boolean
        ) {
            if (finishPrevious) {
                currentActivity.finishAffinity()
            }
            val intent = Intent(currentActivity, PayForOrderActivity::class.java)
            intent.putExtra(MERCHANT_ID_KEY, merchantId.toInt())
            intent.putExtra(STORE_ID, storeId)
            intent.putExtra(NAME, userName)
            intent.putExtra(PHONE, phone)
            intent.putExtra(DELIVERY_LATITUDE, deliveryLatitude)
            intent.putExtra(DELIVERY_LONGITUDE, deliveryLongitude)
            intent.putExtra(DELIVERY_ADDRESS, deliveryAddress)
            currentActivity.startActivity(intent)
        }
    }
}
