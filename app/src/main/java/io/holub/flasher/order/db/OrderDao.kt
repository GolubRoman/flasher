package io.holub.flasher.order.db

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import io.holub.flasher.store.cart.CartStuff
import io.holub.flasher.store.cart.DisplayedCartStuff
import io.reactivex.Maybe

@Dao
interface OrderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertOrder(order: Order): Long

    @Delete
    fun removeOrder(order: Order)

    @Query("DELETE FROM OrderStuff WHERE orderId = :orderId")
    fun removeOrderStuffsForOrderId(orderId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertOrders(orders: List<Order>): LongArray

    @Query("SELECT * FROM `Order` WHERE orderId = :orderId")
    fun observeOrderForId(orderId: String): Maybe<Order>

    @Query("SELECT * FROM `Order` ORDER BY `Order`.timestamp ASC")
    fun observeAllOrders(): DataSource.Factory<Int, Order>

    @Query("SELECT * FROM `Order` ORDER BY `Order`.timestamp ASC")
    fun getAllOrders(): Maybe<List<Order>>

    @Query("SELECT * FROM OrderStuff WHERE orderId = :orderId")
    fun observeOrderStuffsForOrderId(orderId: String): Maybe<List<OrderStuff>>

    @Query("SELECT * FROM CartStuff WHERE parentStuffId = :stuffId")
    fun observeCartStuffForStuffId(stuffId: String): Maybe<CartStuff>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCartStuff(cartStuff: CartStuff): Long

    @Query("SELECT * FROM CartStuff WHERE fullStuffId = :id")
    fun observeCartStuffForId(id: String): Maybe<CartStuff>

    @Query("SELECT * FROM CartStuff INNER JOIN Stuff ON CartStuff.parentStuffId = Stuff.stuffId INNER JOIN Store ON Stuff.parentStoreId = Store.id WHERE Stuff.parentStoreId = :storeId ORDER BY Stuff.parentStoreId, Stuff.stuffId ")
    fun getDisplayedCartStuffsForStoreId(storeId: String): List<DisplayedCartStuff>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertOrderStuffs(orderStuffs: List<OrderStuff>): LongArray

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertOrderStuff(orderStuff: OrderStuff): Long

    @Delete
    fun removeCartStuff(cartStuff: CartStuff)

    @Delete
    fun removeCartStuffs(cartStuffs: List<CartStuff>)

    @Query("SELECT * FROM CartStuff INNER JOIN Stuff ON CartStuff.parentStuffId = Stuff.stuffId INNER JOIN Store ON Stuff.parentStoreId = Store.id ORDER BY Stuff.parentStoreId, Stuff.stuffId ")
    fun observeDisplayedCartStuffs(): DataSource.Factory<Int, DisplayedCartStuff>

    @Query("SELECT * FROM CartStuff INNER JOIN Stuff ON CartStuff.parentStuffId = Stuff.stuffId INNER JOIN Store ON Stuff.parentStoreId = Store.id WHERE Stuff.parentStoreId = :storeId ORDER BY Stuff.parentStoreId, Stuff.stuffId ")
    fun observeDisplayedCartStuffsForStoreIdLiveData(storeId: String): DataSource.Factory<Int, DisplayedCartStuff>

    @Query("SELECT * FROM CartStuff INNER JOIN Stuff ON CartStuff.parentStuffId = Stuff.stuffId INNER JOIN Store ON Stuff.parentStoreId = Store.id WHERE Stuff.parentStoreId = :storeId ORDER BY Stuff.parentStoreId, Stuff.stuffId ")
    fun observeDisplayedCartStuffsForStoreId(storeId: String): Maybe<List<DisplayedCartStuff>>

    @Query("DELETE FROM CartStuff")
    fun deleteAllCartStuffs()

}
