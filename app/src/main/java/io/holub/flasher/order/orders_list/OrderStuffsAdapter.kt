package io.holub.flasher.order.orders_list

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.util.strikeThroughText
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemOrderStuffBinding
import io.holub.flasher.order.db.OrderStuff

class OrderStuffsAdapter : RecyclerView.Adapter<OrderStuffsAdapter.OrderStuffViewHolder>() {

    private var list = arrayListOf<OrderStuff>()

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderStuffViewHolder {
        val binding = DataBindingUtil.inflate<ItemOrderStuffBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_order_stuff, parent, false
        )
        return OrderStuffViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderStuffViewHolder, position: Int) {
        holder.bindTo(list[position])
    }

    fun submitList(list: List<OrderStuff>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    inner class OrderStuffViewHolder(val binding: ItemOrderStuffBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(orderStuff: OrderStuff?) {
            if (orderStuff != null) {
                binding.orderStuff = orderStuff
                binding.executePendingBindings()
                if (binding.tvPrice.text != binding.tvPriceWithDiscount.text) {
                    strikeThroughText(binding.tvPrice)
                    binding.tvPriceWithDiscount.visibility = View.VISIBLE
                } else {
                    binding.tvPriceWithDiscount.visibility = View.GONE
                }
                initCenterCropImageViewViaGlide(binding.ivPhoto, orderStuff.photoUrl)
            }
        }
    }
}
