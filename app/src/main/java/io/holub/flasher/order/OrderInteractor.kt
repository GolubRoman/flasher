package io.holub.flasher.order

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.io.db.Prefs
import io.holub.flasher.common.util.buildDbConfig
import io.holub.flasher.order.db.Order
import io.holub.flasher.order.db.OrderDao
import io.holub.flasher.order.db.OrderStuff
import io.holub.flasher.store.cart.CartStuff
import io.holub.flasher.store.cart.DisplayedCartStuff
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.db.StoreDao
import io.holub.flasher.store.db.Stuff
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import java.util.*

class OrderInteractor(
    private val prefs: Prefs,
    private val orderDao: OrderDao,
    private val storeDao: StoreDao
) {

    @SuppressLint("CheckResult")
    fun observeAllOrders(pageSize: Int): LiveData<PagedList<Order>> {
        return LivePagedListBuilder(orderDao.observeAllOrders(), buildDbConfig(pageSize)).build()
    }

    fun observeOrderStuffsForOrderId(orderId: String): Maybe<List<OrderStuff>> {
        return orderDao.observeOrderStuffsForOrderId(orderId)
    }

    @SuppressLint("CheckResult")
    fun addStuffToCart(stuff: Stuff): Completable {
        return orderDao.observeCartStuffForStuffId(stuff.stuffId)
            .defaultIfEmpty(CartStuff(UUID.randomUUID().toString(), stuff.stuffId, 0))
            .flatMap {
                Maybe.fromCallable {
                    it.count++
                    orderDao.upsertCartStuff(it)
                }
            }.ignoreElement()
            .subscribeOn(Schedulers.io())
    }

    @SuppressLint("CheckResult")
    fun incrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderDao.observeCartStuffForId(displayedCartStuff?.cartStuff?.cartStuffId ?: "")
            .flatMap {
                Maybe.fromCallable {
                    it.count++
                    orderDao.upsertCartStuff(it)
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe({}, { ErrorHandler.logError(it) })
    }

    @SuppressLint("CheckResult")
    fun decrementCartStuffCount(displayedCartStuff: DisplayedCartStuff?) {
        orderDao.observeCartStuffForId(displayedCartStuff?.cartStuff?.cartStuffId ?: "")
            .flatMap {
                Maybe.fromCallable {
                    it.count--
                    orderDao.upsertCartStuff(it)
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe({}, { ErrorHandler.logError(it) })

    }

    @SuppressLint("CheckResult")
    fun removeCartStuff(displayedCartStuff: DisplayedCartStuff?) {
        Maybe.fromCallable {
            displayedCartStuff?.cartStuff?.let { orderDao.removeCartStuff(it) }
        }
            .subscribeOn(Schedulers.io())
            .subscribe({}, { ErrorHandler.logError(it) })
    }

    fun observeDisplayedCartStuffsForStoreIdLiveData(
        storeId: String,
        pageSize: Int
    ): LiveData<PagedList<DisplayedCartStuff>> {
        return LivePagedListBuilder(
            orderDao.observeDisplayedCartStuffsForStoreIdLiveData(storeId),
            buildDbConfig(pageSize)
        ).build()
    }

    fun observeDisplayedCartStuffs(pageSize: Int): LiveData<PagedList<DisplayedCartStuff>> {
        return LivePagedListBuilder(orderDao.observeDisplayedCartStuffs(), buildDbConfig(pageSize)).build()
    }

    fun observeDisplayedCartStuffsForStoreId(storeId: String): Maybe<List<DisplayedCartStuff>> {
        return orderDao.observeDisplayedCartStuffsForStoreId(storeId)
    }

    fun setOrder(
        storeId: String, name: String, phone: String,
        deliveryLatitude: Double, deliveryLongitude: Double, deliveryAddress: String
    ): Completable {
        return storeDao.observeSpecificStoreForId(storeId)
            .flatMap {
                Maybe.fromCallable {
                    val order = createOrderWithStoreAndOtherData(
                        it,
                        deliveryLatitude,
                        deliveryLongitude,
                        deliveryAddress,
                        name,
                        phone
                    )
                    val cartStuffs = orderDao.getDisplayedCartStuffsForStoreId(order.storeId)
                    val orderStuffs = cartStuffs.map { mapDisplayedCartStuffToOrderStuff(it, order) }
                    order.stuffsCount = orderStuffs.size
                    order.priceWithDiscount = orderStuffs.sumBy { it.totalPriceWithDiscount }
                    order.price = orderStuffs.sumBy { it.totalPrice }
                    orderDao.upsertOrder(order)
                    orderDao.upsertOrderStuffs(orderStuffs)
                    cartStuffs.map { it.cartStuff }
                }
            }.flatMapCompletable {
                Completable.fromRunnable {
                    orderDao.removeCartStuffs(it)
                }
            }
    }

    private fun createOrderWithStoreAndOtherData(
        store: Store, deliveryLatitude: Double, deliveryLongitude: Double,
        deliveryAddress: String, name: String, phone: String
    ) = Order(
        UUID.randomUUID().toString(), Date().time,
        store.id, store.name, store.photoUrl, 0,
        0, 0, deliveryLatitude, deliveryLongitude, deliveryAddress, name, phone
    )

    private fun mapDisplayedCartStuffToOrderStuff(cartStuff: DisplayedCartStuff, order: Order) = OrderStuff(
        UUID.randomUUID().toString(),
        order.orderId,
        cartStuff.stuff.stuffId,
        cartStuff.stuff.stuffCategoryId,
        cartStuff.stuff.name,
        cartStuff.stuff.photoUrl,
        cartStuff.stuff.description,
        cartStuff.stuff.price,
        (cartStuff.stuff.price * (1 - cartStuff.stuff.discountPercentage * 0.01)).toInt(),
        cartStuff.cartStuff.count * cartStuff.stuff.price,
        cartStuff.cartStuff.count * (cartStuff.stuff.price * (1 - cartStuff.stuff.discountPercentage * 0.01)).toInt(),
        cartStuff.cartStuff.count
    )

}
