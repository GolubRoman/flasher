package io.holub.flasher.order

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.gms.maps.SupportMapFragment

class OnScrollableContainerMapFragment : SupportMapFragment() {

    private var mOnTouchListener: OnTouchListener? = null

    override fun onCreateView(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstance: Bundle?
    ): View? {
        val mapView = super.onCreateView(layoutInflater, viewGroup, savedInstance)

        mapView?.let { mapView ->
            activity?.let { activity ->
                (mapView as ViewGroup).addView(
                    TouchableWrapper(activity),
                    ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )
            }
        }

        return mapView
    }

    fun setOnTouchListener(onTouchListener: OnTouchListener) {
        mOnTouchListener = onTouchListener
    }

    interface OnTouchListener {

        fun onStartScrollingMap()

        fun onStopScrollingMap()
    }

    private inner class TouchableWrapper(context: Context) : FrameLayout(context) {

        init {
            setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
        }

        override fun dispatchTouchEvent(event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> mOnTouchListener?.onStartScrollingMap()
                MotionEvent.ACTION_UP -> mOnTouchListener?.onStopScrollingMap()
            }
            return super.dispatchTouchEvent(event)
        }
    }

}