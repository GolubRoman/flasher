package io.holub.flasher.profile

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.*
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.common.view.TabFragment
import io.holub.flasher.databinding.FragmentProfileBinding
import io.holub.flasher.login.credentials.CredentialsActivity
import io.reactivex.android.schedulers.AndroidSchedulers

class ProfileFragment : TabFragment() {

    private var binding: FragmentProfileBinding? = null
    private var viewModel: ProfileViewModel? = null
    private var adapter: CardAdapter? = null
    private var isInitted = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        return binding?.root
    }

    override fun init() {
        if (!isInitted) {
            showProgressBarFrame(binding?.flProgressBar)
            isInitted = true
            initViews()
        }
    }

    private fun initAddCardView() {
        binding?.let {
            it.btnAddCard.setOnClickListener {
                activity?.showAddCardDialog()?.subscribe({ addCard(it) }, { ErrorHandler.logError(it) })
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun addCard(card: Card) {
        viewModel?.addCard(card)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ showAlertDialog(activity, R.string.card_added, R.string.card_added_title) },
                { showAlertDialog(activity, R.string.card_adding_failed, R.string.card_added_title) })
    }

    private fun initInputs(user: FirebaseUser) {
        binding?.let { binding ->
            binding.etEmail.setText(user.email)
            binding.etName.setText(user.displayName)
            binding.etPhone.setText(user.phoneNumber)
            binding.etEmail.isEnabled = false
            binding.etName.isEnabled = false
            binding.etPhone.isEnabled = false
        }
    }

    @SuppressLint("CheckResult")
    private fun initViews() {
        viewModel?.observeCurrentUser()
            ?.subscribe({ user ->
                binding?.let { binding ->
                    initInputs(user)
                    initAddCardView()
                    collapseSpecificViewsIfDataIsEmpty(
                        user.phoneNumber,
                        binding.flPhoneContainer,
                        binding.tvInputPhone,
                        binding.ablAppBarLayout
                    )
                    collapseSpecificViewsIfDataIsEmpty(
                        user.email,
                        binding.flEmailContainer,
                        binding.tvInputEmail,
                        binding.ablAppBarLayout
                    )
                    initCenterCropImageViewViaGlide(binding.ivPhoto, user.photoUrl.toString())

                    initCardsList()
                    Handler().postDelayed({ hideProgressBarFrame(binding.flProgressBar) }, 1500)
                }
            }, {
                activity?.let { activity -> CredentialsActivity.start(activity as AppCompatActivity, true) }
                ErrorHandler.logError(it)
            })


    }

    private fun initCardsList() {
        activity?.let { activity ->
            adapter = CardAdapter()
            binding?.rvCards?.layoutManager = LinearLayoutManager(activity)
            binding?.rvCards?.adapter = adapter
            adapter?.processCardRemoveClicked()
            viewModel?.processCards()
        }
    }

    private fun CardAdapter.processCardRemoveClicked() = observeOnRemoveCardClicked()
        .subscribe({
            viewModel?.removeCard(it)
        }, { ErrorHandler.logError(it) })

    private fun ProfileViewModel.processCards() = observeCards().observe(this@ProfileFragment, Observer {
        it?.let {
            adapter?.submitList(it)
            if (it.size == 0) {
                showEmptyListView(binding?.rvCards, binding?.flEmptyList)
            } else {
                hideEmptyListView(binding?.rvCards, binding?.flEmptyList)
            }
        }
    })
}
