package io.holub.flasher.profile

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class Card(
    @PrimaryKey
    @NonNull
    var cardNumber: String,
    var cvc: String,
    var monthExpiring: String,
    var yearExpiring: String,
    var cardOwner: String
) {

    fun cutCardNumber(): String {
        return "•••• " + cardNumber.substring(cardNumber.length - 4, cardNumber.length)
    }

    fun cardType(): CardType {
        var unspacedCardNumber = cardNumber.replace(" ", "")
        return when {
            unspacedCardNumber.matches(Regex(CardType.VISA.pattern)) -> CardType.VISA
            unspacedCardNumber.matches(Regex(CardType.MASTERCARD.pattern)) -> CardType.MASTERCARD
            else -> CardType.UNKNOWN
        }
    }
}

enum class CardType(val pattern: String) {
    VISA("^4[0-9]{6,}\$"), MASTERCARD("^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}\$"), UNKNOWN(
        ""
    )
}