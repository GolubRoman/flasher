package io.holub.flasher.profile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.google.firebase.auth.FirebaseUser
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.user.UserInteractor
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel : ViewModel() {

    @Inject
    lateinit var userInteractor: UserInteractor

    init {
        DaggerProfileViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeCurrentUser(): Maybe<FirebaseUser> {
        return userInteractor.observeCurrentUser()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun removeCard(card: Card) {
        userInteractor.removeCard(card)
    }

    fun observeCards(): LiveData<PagedList<Card>> {
        return userInteractor.observeCards(0)
    }

    fun addCard(card: Card): Completable {
        return userInteractor.addCard(card)
            .subscribeOn(Schedulers.io())
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(profileViewModel: ProfileViewModel)
    }
}


