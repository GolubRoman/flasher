package io.holub.flasher.profile

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initCenterCropImageViewViaGlide
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemCardBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class CardAdapter : PagedListAdapter<Card, CardAdapter.CardViewHolder>(DIFF_CALLBACK) {

    private val onRemoveCardClickedPublishSubject = PublishSubject.create<Card>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val binding = DataBindingUtil.inflate<ItemCardBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_card,
            parent,
            false
        )
        return CardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        getItem(holder.adapterPosition)?.let { holder.bindTo(it) }
    }

    fun observeOnRemoveCardClicked(): Observable<Card> = onRemoveCardClickedPublishSubject

    inner class CardViewHolder(val binding: ItemCardBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(card: Card) {
            binding.card = card
            binding.executePendingBindings()
            initCenterCropImageViewViaGlide(binding.ivPhoto, getImageResForCard(card))
            initOnCardRemoveClickListener(card)
        }

        private fun getImageResForCard(card: Card)  = when (card.cardType()) {
                CardType.VISA -> R.drawable.ic_visa
                CardType.MASTERCARD -> R.drawable.ic_mastercard
                else -> R.drawable.ic_default_card
            }

        private fun initOnCardRemoveClickListener(card: Card) {
            binding.ivRemove.setOnClickListener {
                binding.card?.let { onRemoveCardClickedPublishSubject.onNext(card) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Card> = object : DiffUtil.ItemCallback<Card>() {
            override fun areItemsTheSame(
                item1: Card, item2: Card
            ): Boolean {
                return item1.cardNumber == item2.cardNumber
            }

            override fun areContentsTheSame(
                item1: Card, item2: Card
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
