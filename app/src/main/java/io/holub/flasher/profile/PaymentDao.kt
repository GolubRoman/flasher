package io.holub.flasher.profile

import android.arch.paging.DataSource
import android.arch.persistence.room.*

@Dao
interface PaymentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCard(card: Card): Long

    @Delete
    fun removeCard(card: Card)

    @Query("SELECT * FROM Card ORDER BY cardNumber")
    fun observeCardsLiveData(): DataSource.Factory<Int, Card>

}
