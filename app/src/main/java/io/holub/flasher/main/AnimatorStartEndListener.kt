package io.holub.flasher.main

import android.animation.Animator

interface AnimatorStartEndListener : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) {}
    
    override fun onAnimationCancel(animation: Animator?) {}
}