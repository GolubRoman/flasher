package io.holub.flasher.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.flasher.common.App
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppScope
import io.holub.flasher.order.OrderInteractor
import io.holub.flasher.store.db.Category
import io.holub.flasher.store.db.Store
import io.holub.flasher.store.StoreInteractor
import io.holub.flasher.store.db.Stuff
import io.reactivex.Completable
import javax.inject.Inject

class MainViewModel : ViewModel() {

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var orderInteractor: OrderInteractor

    init {
        DaggerMainViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun observeDiscountOffers(): LiveData<List<DiscountOffer>> {
        return storeInteractor.observeDiscountOffers()
    }

    fun observeCategoriesLiveData(): LiveData<PagedList<Category>> {
        return storeInteractor.observeCategoriesLiveData(0)
    }

    fun observeTopSalesLiveData(): LiveData<PagedList<Stuff>> {
        return storeInteractor.observeTopSalesLiveData(0)
    }

    fun observeStoresLiveData(): LiveData<PagedList<Store>> {
        return storeInteractor.observeAllStoresLiveData(0)
    }

    fun addStuffToCart(stuff: Stuff): Completable {
        return orderInteractor.addStuffToCart(stuff)
    }

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(mainViewModel: MainViewModel)
    }
}


