package io.holub.flasher.main

import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.error.ErrorHandler
import io.holub.flasher.common.util.hideProgressBarFrame
import io.holub.flasher.common.util.setCardFlipViewPagerTransformer
import io.holub.flasher.common.util.showOnCompletedView
import io.holub.flasher.common.util.showProgressBarFrame
import io.holub.flasher.common.view.TabFragment
import io.holub.flasher.databinding.FragmentMainBinding
import io.holub.flasher.store.StoreActivity
import io.holub.flasher.store.categories_list.CategoryListActivity
import io.holub.flasher.store.category.CategoryActivity
import io.holub.flasher.store.store_list.StoreListActivity
import io.holub.flasher.store.stuff.StuffActivity
import io.holub.flasher.store.top_sales_list.TopSalesListActivity
import io.reactivex.android.schedulers.AndroidSchedulers

class MainFragment : TabFragment() {

    private var binding: FragmentMainBinding? = null
    private var viewModel: MainViewModel? = null
    private var discountOffersPagerAdapter: MainDiscountOffersPagerAdapter? = null
    private var storesAdapter: StorePagedAdapter? = null
    private var categoriesAdapter: MainCategoriesPagedAdapter? = null
    private var salesTopAdapter: MainTopSalesPagedAdapter? = null
    private var isInitted = false
    private var currentAnimator: AnimatorSet? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return binding?.root
    }

    private fun initDiscountOffers() {
        activity?.let {
            discountOffersPagerAdapter = MainDiscountOffersPagerAdapter(it as AppCompatActivity)
            binding?.vpDiscountOffers?.adapter = discountOffersPagerAdapter
            binding?.tlTabs?.setViewPager(binding?.vpDiscountOffers)
            binding?.vpDiscountOffers?.addOnPageChangeListener(object : OnPageSelectedListener {
                override fun onPageSelected(position: Int) {
                    binding?.tlTabs?.selection = position
                }
            })
            setCardFlipViewPagerTransformer(binding?.vpDiscountOffers)
            processDiscountOffers()
        }
    }

    private fun processDiscountOffers() = viewModel?.observeDiscountOffers()?.observe(this, Observer {
        it?.let {
            discountOffersPagerAdapter?.submitList(it)
            binding?.tlTabs?.count = it.size
            binding?.tlTabs?.selection = 0
        }
    })

    private fun processCategories() = viewModel?.observeCategoriesLiveData()?.observe(this, Observer {
        it?.let { categoriesAdapter?.submitList(it) }
    })

    private fun processSpecificCategoryClicked() = categoriesAdapter?.observeOnCategoryClicked()?.subscribe({
        CategoryActivity.start(activity as AppCompatActivity, it.id, false)
    }, { ErrorHandler.logError(it) })

    @SuppressLint("CheckResult")
    private fun initCategoriesList() {
        categoriesAdapter = MainCategoriesPagedAdapter()
        binding?.rvCategories?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding?.rvCategories?.itemAnimator = null
        binding?.rvCategories?.adapter = categoriesAdapter

        processSpecificCategoryClicked()
        processCategories()
        binding?.btnCategories?.setOnClickListener {
            CategoryListActivity.start(
                activity as AppCompatActivity,
                false
            )
        }
    }

    private fun initTopSalesList() {
        activity?.let { activity ->
            salesTopAdapter = MainTopSalesPagedAdapter()
            binding?.rvSalesTop?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            binding?.rvSalesTop?.itemAnimator = null
            binding?.rvSalesTop?.adapter = salesTopAdapter
            processSpecificTopSalesClicked()
            processTopSalesStuffs()
            processSpecificTopSalesBuyClicked()
            binding?.btnSalesTop?.setOnClickListener {
                TopSalesListActivity.start(
                    activity as AppCompatActivity,
                    false
                )
            }
        }
    }

    private fun processTopSalesStuffs() = viewModel?.observeTopSalesLiveData()?.observe(this, Observer {
        it?.let { salesTopAdapter?.submitList(it) }
    })

    private fun processSpecificTopSalesClicked() = salesTopAdapter?.observeOnStuffClicked()?.subscribe({
        StuffActivity.start(activity as AppCompatActivity, it.stuffId, false)
    }, { ErrorHandler.logError(it) })

    private fun processSpecificTopSalesBuyClicked() = salesTopAdapter?.observeOnBuyStuffClicked()
        ?.subscribe({
            viewModel?.addStuffToCart(it)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                    { currentAnimator = showOnCompletedView(binding?.ivCompleted, currentAnimator) },
                    { ErrorHandler.logError(it) })
        }, { ErrorHandler.logError(it) })

    private fun initStoreList() {
        activity?.let { activity ->
            storesAdapter = StorePagedAdapter()
            binding?.rvStores?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            binding?.rvSalesTop?.itemAnimator = null
            binding?.rvStores?.adapter = storesAdapter
            processSpecificStoreClicked()
            processStores()
            binding?.btnStores?.setOnClickListener { StoreListActivity.start(activity as AppCompatActivity, false) }
        }
    }

    private fun processStores() = viewModel?.observeStoresLiveData()?.observe(this, Observer {
        it?.let { storesAdapter?.submitList(it) }
    })

    private fun processSpecificStoreClicked() = storesAdapter?.observeOnStoreClicked()?.subscribe({
        StoreActivity.start(activity as AppCompatActivity, it.id, false)
    }, { ErrorHandler.logError(it) })

    @SuppressLint("ClickableViewAccessibility")
    override fun init() {
        if (!isInitted) {
            showProgressBarFrame(binding?.flProgressBar)
            Handler().postDelayed({ hideProgressBarFrame(binding?.flProgressBar) }, 2000)
            isInitted = true
            bindViews()
        }
    }

    @SuppressLint("CheckResult")
    private fun bindViews() {
        initDiscountOffers()
        initCategoriesList()
        initTopSalesList()
        initStoreList()
    }

}
