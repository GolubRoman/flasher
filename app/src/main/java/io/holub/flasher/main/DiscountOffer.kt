package io.holub.flasher.main

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class DiscountOffer(
    @PrimaryKey
    val id: String,
    val photoUrl: String,
    val stuffId: String
)