package io.holub.flasher.main

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.flasher.R
import io.holub.flasher.common.util.initImageViewViaGlide
import io.holub.flasher.databinding.ItemCategoryBinding
import io.holub.flasher.store.db.Category
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class MainCategoriesPagedAdapter : PagedListAdapter<Category, MainCategoriesPagedAdapter.CategoryViewHolder>(
    DIFF_CALLBACK
) {

    private val onCategoryClickedPublishSubject = PublishSubject.create<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding = DataBindingUtil.inflate<ItemCategoryBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_category,
            parent,
            false
        )
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    fun observeOnCategoryClicked(): Observable<Category> {
        return onCategoryClickedPublishSubject
    }

    inner class CategoryViewHolder(val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(category: Category?) {
            if (category != null) {
                binding.category = category
                binding.executePendingBindings()
                initImageViewViaGlide(binding.ivPhoto, category.photoUrl)
                initOnCategoryClickListener()
            }
        }

        private fun initOnCategoryClickListener() {
            binding.category?.let { category ->
                binding.root.setOnClickListener { onCategoryClickedPublishSubject.onNext(category) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Category> = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(
                item1: Category, item2: Category
            ): Boolean {
                return item1.id == item2.id
            }

            override fun areContentsTheSame(
                item1: Category, item2: Category
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
