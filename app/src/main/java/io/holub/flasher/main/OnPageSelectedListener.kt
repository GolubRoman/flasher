package io.holub.flasher.main

import android.support.v4.view.ViewPager

interface OnPageSelectedListener : ViewPager.OnPageChangeListener {

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageScrollStateChanged(state: Int) {}
}