package io.holub.flasher.main

import android.databinding.DataBindingUtil
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.holub.flasher.R
import io.holub.flasher.common.util.initImageViewViaGlide
import io.holub.flasher.common.view.GlideApp
import io.holub.flasher.databinding.ItemDiscountOfferBinding
import io.holub.flasher.store.stuff.StuffActivity

class MainDiscountOffersPagerAdapter(val activity: AppCompatActivity) : PagerAdapter() {

    private var inflater = LayoutInflater.from(activity)
    private var discountOffers = mutableListOf<DiscountOffer>()

    fun submitList(discountOffers: List<DiscountOffer>) {
        this.discountOffers.clear()
        this.discountOffers.addAll(discountOffers)
        notifyDataSetChanged()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return discountOffers.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val binding =
            DataBindingUtil.inflate<ItemDiscountOfferBinding>(inflater, R.layout.item_discount_offer, view, false)

        initImageViewViaGlide(binding.ivPhoto, discountOffers[position].photoUrl)
        view.addView(binding.root)
        binding.ivPhoto.setOnClickListener { StuffActivity.start(activity, discountOffers[position].stuffId, false) }

        return binding.root
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}