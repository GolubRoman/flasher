package io.holub.flasher

import android.app.Activity
import android.os.Build
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage
import android.support.test.uiautomator.UiDevice
import android.support.test.uiautomator.UiObjectNotFoundException
import android.support.test.uiautomator.UiSelector
import android.support.v7.widget.RecyclerView
import android.view.View
import io.holub.flasher.common.di.AppComponent
import org.hamcrest.Matcher
import android.widget.TextView
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom


fun waitForViewWithId(viewInteraction: ViewInteraction) {
    do {
        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    } while (!doesViewExist(viewInteraction))
}

private fun doesViewExist(viewInteraction: ViewInteraction) = try {
    viewInteraction.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    true
} catch (e: NoMatchingViewException) {
    false
}

fun getCurrentActivity(): Activity? {
    var currentActivity: Activity? = null
    getInstrumentation().runOnMainSync {
        val resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
        for (activity in resumedActivities) {
            currentActivity = activity
            break
        }
    }

    return currentActivity
}

fun grantPermissionsIfNecessary() {
    if (Build.VERSION.SDK_INT >= 23) {
        val allowPermissions = UiDevice.getInstance(getInstrumentation()).findObject(UiSelector().text("ALLOW"))
        if (allowPermissions.exists()) {
            try {
                allowPermissions.click()
            } catch (e: UiObjectNotFoundException) {}
        }
    }
}

fun getText(matcher: Matcher<View>): String {
    val stringHolder = arrayOf<String>()
    onView(matcher).perform(object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(TextView::class.java)
        }

        override fun getDescription(): String {
            return "getting text from a TextView"
        }

        override fun perform(uiController: UiController, view: View) {
            val tv = view as TextView
            stringHolder[0] = tv.text.toString()
        }
    })
    return stringHolder[0]
}

fun clickOnSpecificButtonInItemViewAtRow(position: Int, rvId: Int, viewId: Int) {
    onView(withId(rvId)).perform(
        RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
            position,
            ClickOnSpecificButtonInItemView(viewId)
        )
    )
}

fun dropUserAuthentication(appComponent: AppComponent) {
    appComponent.firebaseAuth().signOut()
}

class ClickOnSpecificButtonInItemView(val viewId: Int) : ViewAction {

    var clickAction = click()

    override fun getConstraints(): Matcher<View> {
        return clickAction.constraints
    }

    override fun getDescription(): String {
        return "click on custom view"
    }

    override fun perform(uiController: UiController, view: View) {
        clickAction.perform(uiController, view.findViewById<View>(viewId))
    }
}