package io.holub.flasher

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeUp
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppModule
import io.holub.flasher.common.di.DaggerAppComponent
import io.holub.flasher.tab.TabActivity
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import org.hamcrest.Matchers.containsString
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.RuntimeException
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class TabActivityInstrumentedTest {

    @get:Rule
    var mActivityRule = ActivityTestRule(TabActivity::class.java)

    var appComponent: AppComponent? = null

    @Before
    fun init() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(InstrumentationRegistry.getTargetContext().applicationContext))
            .build()
    }

    @Test
    fun checkCartStuffsAddedToCart() {
        appComponent?.let {
            Completable.fromRunnable { it.orderDao().deleteAllCartStuffs() }
                .subscribeOn(Schedulers.io())
                .subscribe()

            sleep(2000)

            waitForViewWithId(onView(withId(R.id.sv_scroll_view)))
            onView(withId(R.id.sv_scroll_view)).perform(swipeUp())

            clickOnSpecificButtonInItemViewAtRow(0, R.id.rv_sales_top, R.id.btn_buy)
            clickOnSpecificButtonInItemViewAtRow(1, R.id.rv_sales_top, R.id.btn_buy)

            waitForViewWithId(onView(withId(R.id.btn_cart)))
            onView(withId(R.id.btn_cart)).perform(click())

            waitForViewWithId(onView(withId(R.id.tv_cart_count)))
            if (getText(withId(R.id.tv_cart_count)) != "3") {
                throw RuntimeException("Test not passed")
            }
            sleep(4000)
        }
    }
}
