package io.holub.flasher

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.UiDevice
import android.util.Log
import io.holub.flasher.common.di.AppComponent
import io.holub.flasher.common.di.AppModule
import io.holub.flasher.common.di.DaggerAppComponent
import io.holub.flasher.login.credentials.CredentialsActivity
import io.holub.flasher.tab.TabActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.RuntimeException
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class CredentialsInstrumentedTest {

    @get:Rule
    var mActivityRule = ActivityTestRule(CredentialsActivity::class.java, true, false)

    var appComponent: AppComponent? = null

    @Before
    fun init() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(InstrumentationRegistry.getTargetContext().applicationContext))
            .build()
    }

    @Test
    fun signInWithGoogleTest() {
        appComponent?.let {
            dropUserAuthentication(it)

            mActivityRule.launchActivity(Intent())

            waitForViewWithId(onView(withId(R.id.btn_google_sign_in)))
            onView(withId(R.id.btn_google_sign_in)).perform(click())
            sleep(2000)

            val device = UiDevice.getInstance(getInstrumentation())
            device.click(540, 960)

            sleep(4000)

            if (it.firebaseAuth().currentUser == null) {
                throw RuntimeException("Test not passed")
            }
        }
    }

    @Test
    fun signInWithFacebookTest() {
        appComponent?.let {
            dropUserAuthentication(it)
            mActivityRule.launchActivity(Intent())

            waitForViewWithId(onView(withId(R.id.btn_facebook_sign_in)))
            onView(withId(R.id.btn_facebook_sign_in)).perform(click())
            sleep(2000)

            val device = UiDevice.getInstance(getInstrumentation())
            device.click(540, 1240)

            sleep(4000)

            if (it.firebaseAuth().currentUser == null) {
                throw RuntimeException("Test not passed")
            }
        }
    }
}
